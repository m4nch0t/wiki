# Wordpress

## Changer en base de donnée l'url du site:

Prenons un cas simple, je souhaiterais ajouter **www** et le passer en HTTPS, voici un script SQL:

```sql
UPDATE vmanchot_options
SET option_value = replace(option_value, 'http://vmanchot.net', 'https://www.vmanchot.net')
WHERE option_name = 'home'
OR option_name = 'siteurl';

UPDATE vmanchot_posts
SET guid = REPLACE (guid, 'http://vmanchot.net', 'https://www.vmanchot.net');

UPDATE vmanchot_posts
SET post_content = REPLACE (post_content, 'http://vmanchot.net', 'https://www.vmanchot.net');

UPDATE vmanchot_postmeta
SET meta_value = REPLACE (meta_value, 'http://vmanchot.net','https://www.vmanchot.net');
```
