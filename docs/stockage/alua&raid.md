# Présentation du RAID et ALUA

## Rappels sur le stockage

### RAID

RAID signifie Rudondant Array of inexpensives disks ou grappe de disques redondants peu coûteux. Voici les types les plus couramment utilisés :

* **RAID 0** = Aggregation de disques sans redondance mais offrant une haute performance. La donnée est répartie équitablement entre tous les disque.
* **RAID 1** = Mirroir de disques pour les écritures et les lectures. La donnée est dupliquée sur tous les disques.
* **RAID 5** = Aggrégation de disques avec paritée répartie sur l'ensemble des disques. Le bons compromis financier entre redondance et sécurité.
* **RAID 6** = Aggrégation de disques avec paritée répartie sur un ou deux autres disques de la grappe. Le RAID 6 s'apparente à un mélange de RAID 1 et de RAID 5 et supporte la perte de plusieurs disques.
* **RAID DP** = inspiré du RAID 6 à ceci prés que les données de paritée sont écrites sur deux disques fixes avec deux algorithmes différents. Ce type de RAID est utilisé sur les NAS Network Appliance.
* **RAID 0+1** = Mirroir de grappes RAID 0, très rapide, mais couteux en nombre de disques.

### Controleur

C'est une carte électronique qui pilote un ensemble de disques physiques et qui les associe ensemble pour former une grappe RAID. Cette grappe RAID contient elle un ou plusieurs volumes logiques appelés ARRAYs (litéralement un étalage). On en déduit donc qu'un même tirroir de disque peut contenir plusieurs morceaux d'ARRAY avec des niveaux de RAID différents : RAID 5 pour le premier ARRAY, RAID 6 pour le second ...

La baie de stockage dépendra ensuite d'un savant équillibre entre le nombre de disques que l'on appelle alors AXES, le nombre de cartes controleurs; la performance, la sécurité et le temps de reconstruction attendus : plus il y a d'axes, plus les performances seront importantes puisque chaque disque manipulera moins d'informations pour chaque opération de lecture ou d'écriture (I/O).

A l'inverse, l'ensemble des disques composant une gpappe partageront l'ensemble de la bande passante du controleur qui les pilote Vous me suivez ? Par exemple un controleur Fiber Channel 4 Gb/s disposera d'une bande passante de 500 Mo/s soit l'équivalent de 7 disques SATA II à 65 Mo/s accédés simultanément. L'accés est dit non blocant ou à vitesse nominale.

Depuis une quinzaine d'années, les controleurs sont traditionnellement installés dans une tête SAN ou NAS, un simple serveur faisant tourner un systéme d'exploitation. Chez EMC cet OS a pour doux nom FLARE (un lointain cousin de Windows Storage Server) tandis que chez NetAPP il s'appelle Data ONTAP et s'apparente plutôt à un unix BSD de type Solaris. Le systême d'exploitation qui pilote donc l'ensemble de la baie est responsable de son fonctionnement et inclut des fonctions annexes matérielles et logicielles telles que le cache mémoire ou la réplication. On parle alors de stockage monolithique.

Certains ne faisant pas comme tout le monde, il existe des exceptions appelés stockages modulaires comme l'Axiom Pillar Data l'IBM XIV ou l'EMC vMax dont les fonctions sont disséminées dans les différents éléments de la baie. par exemple, les contrôleurs sont inclus dans les tiroirs de disques, les faisant fonctionner de manière totalement non bloquante.

### LUN

Un array est partagés sur le réseau de stockage sous un numéro unique appelé LUN (Logical Unit Number).

### Initiateur

C'est le port d'une machine qui cherche d'accéder à un LUN au travers du protocole ISCSI, Fiber Channel ou encore FCoE (Fiber Channel Over Ethernet).

### Target/Port Group Target

C'est la combinaison d'un ou plusieurs ports physiques et du descripteur (nom logique) sur lesquels on peut se connecter pour avoir accés é un LUN.
En iSCSI, le descripteur aura un format iSCSI qualified name ou iqn. par exemple : iqn.1998-01.com.vmware:ESX4-1db07213 ; alors qu'en FC il aura la forme d'un World Wide Name par exemple : 22:00:00:0b:08:45:18.

## L'ALUA

La redondance des contrôleurs avec ALUA et la vie est plus simple ...

Aujourd'hui la majorité des baies de stockage intègrent des contrôleurs redondants pour piloter les LUNs : EMC Clariion, Network Appliance, IBM DS & NS, HP EVA, Pillar Data Axiom, Dell Equalogic ... Cependant, bien que redondants et similaires en terme d'administration, les caractéristiques d'accès aux LUNs peuvent varier pour les ports de chaque contrôleur ; elles différent notamment en terme de performances. A un instant T, l'un des contrleurs disposera d'un accès plus rapide sur le LUN que l'autre, on parle de chemin optimisé. C'est ce mode d'accès asymétrique au LUN que l'on nomme ALUA (Asymmetric logical Unit Access).

Jusqu'à la version 3.5, de VMware ESX (oui, de vieux souvenirs pour les plus anciens), le mode ALUA n'était pas supporté nativement dans l'hyperviseur. On ne pouvait paramétrer que le mode Chemin Fixe, le plus récent (Most Recently Used) ou l'alternance avec le Round Robin. les deux derniers modes étaient susceptibles de provoquer des accès non optimisés aux LUNs. Il y avait intérêt à mettre tout le monde d'accord sous peine de se voir infliger des messages de type 'LUN Non Optimal Access Path" pouvant aller jusqu'au reboot du contrôleur concerné, infligeant quelques menus désagréments aux VMs du comme un écran noir, machine figée, vmdk HS.... l'avalanche des utilisateurs concernés, dont vous venez visiblement de couper son SI, dans votre bureau étant un plus.

Seule la présence de plug-in annexe tel que EMC powerPath permettait de s'affranchir de ces soucis en accordant host ESX et stockage sur le chemin optimal à utiliser. La version VMware vSphere implémente nativement deux APIs appelées Storage Array Type Plugins ou SATP chargée du Failover et Path Selection Plug-in (PSP) accès à l'équilibre de charge entre les chemins qui permettent de s'affranchir de la gestion des chemins optimisés.

## Un peu de CLI

Vous pouvez accéder à ces APIs au travers du mode console de vSPhere :

Commande pour lister les règles SATP connues du serveur vSphere

```bash
[root@XXXX ~]# esxcli nmp satp listrules

Name Vendor Model Claim Options Description

VMW_SATP_ALUA_CX DGC tpgs_on CLARiiON array in ALUA mode
VMW_SATP_EVA HSV111 tpgs_off active/active EVA 5000 GL
VMW_SATP_EVA HSV200 tpgs_off active/active EVA 4000/6000 XL
VMW_SATP_EVA HSV210 tpgs_off active/active EVA 8000/8100 XL
VMW_SATP_ALUA NETAPP tpgs_on NetApp with ALUA
VMW_SATP_ALUA HP ^MSA2* tpgs_on HP MSA A/A with ALUA
VMW_SATP_ALUA Pillar tpgs_on active/active Ctl Pillar Axiom ALUA Support
```

*tpgs_on* signifie que SATP ne sera activé que si le failover est activé pour l'ensemble des Ports

Group Target du stockage.

Commande pour lister les PSP :

```bash
[root@XXXX ~]# esxcli nmp psp list

Name Description
VMW_PSP_MRU Most Recently Used Path Selection
VMW_PSP_RR Round Robin Path Selection
VMW_PSP_FIXED Fixed Path Selection
```

Par exemple, pour Utiliser par défaut les chemins en Round Robin, il vous faudra taper la commande : esxcli nmp satp setdefaultpsp --psp="VMW_PSP_RR" --satp="VMW_SATP_ALUA"

On peut méme mettre sa propre une règle pour un stockage non listé par défaut : esxcli nmp satp addrule --vendor="Overland" --claim-option="tpgs_on" --satp="VMW_SATP_ALUA" --description="Overland Snap Server 620"
