# Créer une clé usb d’install Windows

Marre de vous baladez avec votre cd d’install pour windows? Marre de la lenteur du lecteur cd/dvd si il y en a un ?! Voici comment créer une clé usb bootable en cli sous windows:

Lancer l’utilitaire diskpart (Win+R , cmd , diskpart)
Lancer les commandes suivantes :

```Batchfile
list disk
```

affiche les lecteurs connectés à votre pc, et leur attribue un chiffre

```Batchfile
select disk x
```

sélectionne le disque voulu (le x est le chiffre attribué à votre clé usb)

```Batchfile
clean
create partition primary
active
format fs=fat32 quick
assign
```

Votre clé usb est maintenant bootable, en fat32. Il ne vous reste plus qu’à coller l’iso d’install de Windows à la racine de la clé usb, et booter dessus.
S'il vous reste de l’espace, vous pouvez y coller les mises à jour récupérer avec wsus offline, et vous aurez la clé usb d’install Windows parfaite.
