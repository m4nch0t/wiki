# Windows

Vous trouverez ici un ensemble de documentation pour gérer votre serveur Windows.

## Transférer sa licence Windows

### Désinstaller la licence sur la source

Ouvrez une invite de commandes en tant qu’administrateur et récupérer l’ID d’activation liée à cette clé produit:

```powershell
slmgr /dlv
```

Désinstallez la clé de produit en saisissant la commande suivante en remplaçant l'ID par celui noté précédemment :

```powershell
slmgr /upk 928a-4016-cd32-28ad-265e
```

Effacez la clé de produit du registre :

```powershell
slmgr /cpky
```

Enfin, réinitialisez l’état de la licence de l’ordinateur :

```powershell
slmgr /rearm
```

### Installer la licence sur la destination

Ouvrez une invite de commandes en tant qu’administrateur et installer la clé produit:

```powershell
slmgr /ipk 928a-4016-cd32-28ad-265e
```

Lancer l’activation de Windows :

```powershell
slmgr /ato
```

### Tips

Lancez l’activation de Windows par téléphone avec la commande :

```powershell
slui 4
```

### Lien symbolique

Voici comment créer un lien symbolique sous Windows:

```Batchfile
mklink /d source  destination
```

Cela fonctionne aussi pour des partages réseau.

### Suppression du reliquat des anciennes installations et mises à jour

Si comme moi après l’installation d'un Service Pack ou mise à jour majeure vous vous êtes rendu compte que le répertoire **c:\windows\winsxs** a grossit de manière exponentielle, il vous faut faire un petit nettoyage comme suit:
Lancez une invite de commande en tant qu’administrateur et à l’intérieur de celle-ci, tapez la commande :

```Batchfile
dism /online /cleanup-image /spsuperseded
```

Et pour info ***winsxs*** (Windows Side By Side) est un dossier qui stocke des DLL en vue d’accès concurrentiels sur celles-ci par de multiples applications. Ce ne sont donc que des clones . Mais ne pas supprimer pour autant! Qui sait ce qu’il y a d’autre dedans que Steve Balmer nous a caché? ^^ Je n’ai pas plus d’infos sûres sur ce que contient ce dossier, donc si vous pouvez éclairer ma lanterne, je suis preneur!
