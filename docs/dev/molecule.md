# Molecule

## Présentation

Molecule est un logiciel d'aide à l'écriture des rôles Ansible. Il permet d'automatiser des tests sur plusieurs distributions, en container ou vm.

## Création du rôle

Tout d'abord, nous commençons par la création d'un rôle avec molecule:

```shell
molecule init role --role-name mon-premier-role
```

Ceci va nous créer une nouvelle arborescence:

```shel
├── README.md
├── defaults
│   └── main.yml
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── molecule
│   └── default
│       ├── Dockerfile.j2
│       ├── INSTALL.rst
│       ├── molecule.yml
│       ├── playbook.yml
│       └── tests
│           ├── __pycache__
│           │   └── test_default.cpython-37.pyc
│           └── test_default.py
├── tasks
│   └── main.yml
└── vars
    └── main.yml
```

Ce qui va nous intéresser ici est le dossier molecule. Vous avez un dossier *default* qui sera le nom de votre *scenario*.
A l'intérieur vous retrouverez:

* un **Dockerfile** qui va être utilisé par molecule pour créer le container. C'est un template, donc à ne pas modifier.
* un fichier **molecule.yml** qui va contenir le contexte d'exécution de votre test.
* un fichier **playbook.yml** qui va permettre l'exécution de votre rôle dans votre contexte molecule.

## Le fichier *molecule.yml*

Il permet de configurer molecule :

* **dependency**: par défaut, molecule utilise *Ansible Galaxy* pour résoudre les dépendances
* **driver**: par défaut molecule utilise Docker, mais vous pouvez en utiliser d'autres. Ce sont des modules Ansible qui sont utilisés.
* **lint**: pour vérifier si les bonnes pratiques d'écriture sont respectées, molecule utilise *Yamllint* par défaut.
* **platforms**: ici vous allez spécifier le type d'instance à créer, leurs noms, et leur appartenance à un groupe Ansible.
* **provisioner**: seul Ansible est supporté ici.
* **verifier**: par défaut, molecule utilise *TestInfra* pour vous permettre d'écrire des tests de déploiement sur vos instances.

Par défaut notre molecule utilise le **provider** docker avec une image centos.

Pour créer un scenario dans votre rôle, rien de compliqué, la commande est similaire à la création d'un rôle.

```shell
molecule init scenario -s debian
```

## Utilisation

### Converge

A partir de maintenant vous avez un rôle vide qui ne fait rien, mais que l'on peut d'ores et déjà tester.
Il vous suffit de vous positionner dans votre rôle et de lancer la commande :

```shell
molecule converge
```

Ceci aura pour action de lancer votre scénario dans votre contexte d'exécution default (créé lors de l'initialisation).
Elle correspond à l'exécution d'une de ces commandes :

```shell
molecule converge
molecule converge --scenario-name default
```

Un scénario est un contexte d'éxécution pour tester votre rôle.
Il peut contenir des variables différentes dans le playbook (ex: role apache pour déployer un site en prod et préprod).  

```shell
molecule converge --scenario-name prod
```

Comme vous n'avez pas encore de tâche dans votre rôle, l'exécution se limitera à :

```shell
Validation completed successfully.
--> Test matrix

└── default
    ├── dependency
    ├── create
    ├── prepare
    └── converge

--> Scenario: 'default'
--> Action: 'dependency'
Skipping, missing the requirements file.
--> Scenario: 'default'
--> Action: 'create'
[DEPRECATION WARNING]: docker_image_facts is kept for backwards compatibility
but usage is discouraged. The module documentation details page may explain
more about this rationale.. This feature will be removed in a future release.
Deprecation warnings can be disabled by setting deprecation_warnings=False in
ansible.cfg.

    PLAY [Create] ******************************************************************

    TASK [Log into a Docker registry] **********************************************
    skipping: [localhost] => (item=None)

    TASK [Create Dockerfiles from image names] *************************************
    changed: [localhost] => (item=None)
    changed: [localhost]

    TASK [Discover local Docker images] ********************************************
    ok: [localhost] => (item=None)
    ok: [localhost]

    TASK [Build an Ansible compatible image] ***************************************
    changed: [localhost] => (item=None)
    changed: [localhost]

    TASK [Create docker network(s)] ************************************************

    TASK [Determine the CMD directives] ********************************************
    ok: [localhost] => (item=None)
    ok: [localhost]

    TASK [Create molecule instance(s)] *********************************************
    changed: [localhost] => (item=None)
    changed: [localhost]

    TASK [Wait for instance(s) creation to complete] *******************************
    FAILED - RETRYING: Wait for instance(s) creation to complete (300 retries left).
    changed: [localhost] => (item=None)
    changed: [localhost]

    PLAY RECAP *********************************************************************
    localhost                  : ok=6    changed=4    unreachable=0    failed=0    skipped=2    rescued=0    ignored=0


--> Scenario: 'default'
--> Action: 'prepare'
Skipping, prepare playbook not configured.
--> Scenario: 'default'
--> Action: 'converge'

    PLAY [Converge] ****************************************************************

    TASK [Gathering Facts] *********************************************************
    ok: [instance]

    PLAY RECAP *********************************************************************
    instance                   : ok=1    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0
```

[Ici](https://molecule.readthedocs.io/en/stable/configuration.html#root-scenario) le lien pour la documentation du scénario

### Les autres commandes importantes

* **check**: permet de faire un *dry-run* afin de valider la syntaxe de votre molecule
* **converge**: permet de lancer l'exécution de votre rôle
* **create**: permet de démarrer votre instance Docker, vagrant, etc
* **dependency**: permet de gérer les dépendances de votre rôle
* **destroy**: permet de détruire votre instance
* **init**: initialise votre rôle
* **lint**: vérifie la syntaxe yaml, ansible de votre rôle
* **list**: permet de lister le statut de votre instance
* **login**: permet de se connecter à une instance sans passer par les commandes docker par exemple
* **syntax**: permet de vérifier la syntaxe de votre rôle
* **test**: permet de faire la totalité des commandes molecule afin de valider définitivement votre rôle et son idempotence
