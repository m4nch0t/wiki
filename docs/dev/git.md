# GIT

## Présentation et vocabulaire

Git est un gestionnaire de révision.

* *révision* : série de modification d'un projet, que l'on nomme **commit**. Possède un identifiant unique.
* *branche* : historique de développement. La branche nominale se nomme **master**, considérée comme *stable*, elle peut se voir attribuer parallèlement de nouvelles branches. Ceci permet de travailler sur l'ajout d'une nouvelle fonctionnalité sans perturber le travail de ses collaborateurs et sans qu'elle n'affecte la branche master.

```ascii
+----------+
| révision <------------+
+----+-----+            |
     ^                  |
+----+-----+            |
| révision |      +-----+----+
+----+-----+      | révision |
     ^            +-----+----+
+----+-----+            ^
| révision |      +-----+----+
+----+-----+      | révision |
     ^            +-----+----+
+----+-----+            ^
| révision +------------+
+----------+
```

Dès que nous décidons de faire suivre un fichier par Git, ce dernier va en conserver toutes les révisions.

Une bonne pratique de Git et de créer des révisions les plus *atomiques* possible. Elles doivent correspondre à des modifications précises et définies. Exemple l'ajout d'une fonctionnalité. Ceci nous permet d'identifier rapidement la modification et de l'annuler en cas de régression. Cette révision doit avoir un commentaire explicite.

## Création du dépôt local

### Configurer l'utilisateur globalement

Pour travailler avec Git, vous devez être identifié, c'est à dire que Git doit mettre un nom devant les modifications. Pour renseigner ces informations de manière globale :

```bash
git config --global user.name "M4nch0t"
git config --global user.email "manchot@vmanchot.net"
```

Ceci sera positionné au niveau global, vous trouverez donc ces informations dans votre *home directory*:

```bash
cat ~/.gitconfig
# This is Git's per-user configuration file.
[user]
        name = m4nch0t
        email = manchot@vmanchot.net
```

### Configurer l'utilisateur localement

Ou simplement par projet :

```bash
git config user.name "Yoan Moscatelli"
git config user.email "yoan@moscatelli.fr"
```

Vous trouverez donc ces informations dans votre dépôt localement :

```bash
cat .git/config
[user]
        name = m4nch0t
        email = manchot@vmanchot.net
```

### Initialiser un projet Git

```bash
cd /opt/git/monprojet/
git init --bare --shared ./monprojet.git
```

Les options :

* **bare** correspond à l'utilisation des répertoires comme arborescence
* **shared** correspond au partage ou non du dépôt, la valeur par défaut est le group correspondant au umask

## Utilisation

### Dépot distant

Récupération du dépôt localement

```bash
git clone ssh://manchot@GITSERVER/opt/git/monprojet/monprojet.git
```

Pour vérifier d'où provient le code vous pouvez utiliser la commande suivante:

```bash
git remote -v
origin  https://gitlab.com/m4nch0t/wiki.git (fetch)
origin  https://gitlab.com/m4nch0t/wiki.git (push)
```

Vous pouvez commencer à coder sur votre poste. Pour connaître le delta entre votre clone et le dépôt:

```bash
git status
```

### Ajouts et commits

Il va nous montrer les fichiers qui ne sont pas présents et nous proposer de les ajouter à notre projet via le choix d'une des commandes suivantes :

* ajout de l'ensemble des fichiers modifiés ou nouveaux, sans tenir compte des supprimés :

```bash
git add .
```

* ajout de l'ensemble des fichiers modifiés ou supprimés, sans tenir compte des nouveaux :

```bash
git add -u
```

* ajout de l'ensemble des fichiers modifiés, supprimés, nouveaux :

```bash
git add -A
```

* ajout d'un fichier ou dossier :

```bash
git add <fichier>
git add <dossier>
```

Une fois ajoutés, nous pouvons faire notre **commit**

```bash
git commit -a -m "Rajout de la feature droïde"
```

Les options :

* **-a** sélectionne tous les fichiers précédemment sélectionnés via la commande **git add**
* **-m** rajoute un message qui permettra de décrire votre commit, utile en cas de projet partagé ou pour revenir sur son code ultérieurement

Il peut être intéressant aussi de créer un **tag** à votre **commit**. Pour identifier par exemple le statut de votre projet ou la version :

```bash
git tag c11a9316ff v2

git tags
    v2
```

Supprimer un tag :

```bash
git tag -d v2
```

On peut ajouter un commentaire à notre tag, comme sur une révision :

```bash
git tag c11a9316ff -a v2 -m "version 2"
```

Pour que notre tag  **v2** soit pris en compte sur un serveur distant :

```bash
git push origin v2
```

### Git Ignore

Il est intéressant de ne pas inclure des fichiers spécifiques à l'OS ou à notre environnement local par exemple.

Pour les exclure il faut les rajouter au fichier **.gitignore** à la racine de notre projet :

```txt
# OS
.DS_Store
Thumbs.db

# vscode
.vscode

# Vagrant
.vagrant
.env

# Python cache
__pycache__

# ansible galaxy
.galaxy_install_info
```

Ou alors de manière globale :

```bash
git config --global core.excludesfile ~/.gitignore
```

Ce fichier accepte des *patterns* :

* *.txt : interdit tous les fichiers avec cette extension
* !readme.txt : ne pas ignorer ce fichier même s'il est identifié par un autre pattern
* test.sh : interdit un fichier en particulier
* dev/ : interdit un répertoire
* **/dev : interdit un répertoire peut importe sa localisation dans l'arborescence

Si vous souhaitez activer le fichier gitignore après avoir déjà commité :

```bash
git rm -r --cached
git add .
git commit -m "add .gitignore"
```

### Publication dans un dépôt distant

La dernière étape consiste à envoyer votre code sur le dépôt :

```bash
git remote add origin youruser@yourserver.com:/path/to/my_project.git
git push origin master
```

Ceci va pousser votre code dans la branche master, si vous n'en êtes pas à votre premier commit, il suffit simplement d'utiliser la commande **git push**.
Nous aurions pu lancer cette commande, qui n'est normalement obligatoire qu'au premier commit, lorsque le repository est vide :

```bash
git push --set-upstream origin master
```

Le paramètre **--set-upstream** permet de spécifier la branche de travail vers laquelle sera poussé notre code. Ici master, mais vous pouvez remplacer par n'importe quelle branche à utiliser. La branche master étant normalement la branche stable.

**Push** permet aussi d'effectuer des modifications plus poussées:

* Publier la branche dev dans master :

```bash
git push origin dev:master
```

* Effacer la branche dev distante :

```bash
git push origin :dev
```

* Créer une **pull-request**, une demande de changement. Par exemple avec une nouvelle banche nommée *feature-au-top* :

```bash
git push origin master:feature-au-top
```

Si vous souhaitez modifier l'url de votre dépôt distant :

```bash
git remote set-url origin youruser@yourserver.com:/path/to/my_project.git
```

### Historique

Pour voir l'historique des révisions:

```bash
git log
commit c11a9316ff8536960d957bad69163e4dff914fcf
Author: m4nch0t <manchot@vmanchot.net>
Date:   Mon Jun 10 16:48:38 2019 +0200

    Rajout de la feature droïde
```

On voit ici notre commentaire du commit précédent.

Pour avoir une visualisation graphique:

```ascii
*   5f6d2ab Merge remote-tracking branch 'origin/master'
|\  
| * 5605126 typo
| * 1f7952a reorder nav
| * f5fc61b add git stash, reset
| * 66ddf70 reorder nav
| *   b9c5007 Merge remote-tracking branch 'origin/master'
| |\  
| * | e7ea062 clean history
* | | be018a3 typo bash
| |/  
|/|
* | aaa4f99 add exec, molecule and ansible specification
|/  
* 087eef1 add os specific
* 0eec3ba ansible and molecule
```

Nous continuons à travailler sur notre code, et nous souhaiterions savoir ce que nous avons modifié. Il existe une commande pour cela :

```bash
git diff
diff --git a/docs/dev/git.md b/docs/dev/git.md
index 6113c83..5caa268 100644
--- a/docs/dev/git.md
+++ b/docs/dev/git.md
@@ -12,18 +12,38 @@ Les options :

 ## Utilisation sur votre post de dev

-Récupération du dépôt localement
+
 bash
 git clone ssh://manchot@GITSERVER/opt/git/monprojet/monprojet.git

+Pour vérifier d où provient le code vous pouvez utiliser la commande suivante:
+bash
+git remote -v
+origin  https://gitlab.com/m4nch0t/wiki.git (fetch)
+origin  https://gitlab.com/m4nch0t/wiki.git (push)
+
+Pour travailler avec Git, vous devez être identifié, c est à dire que Git doit mettre un nom devant les modifications. Pour renseigner ces informations :
```

 Voici un extrait de mon **diff** lors de mon écriture de ce tuto. Les lignes avec un symbole plus **+** sont des ajouts et celle avec un symbole **-** sont des suppressions.

Pour voir les modifications sur un fichier :

```bash
git log -p docs/dev/git.md

commit 5f6d2ab25ce8969bc24739d9ebbcdc0176be01f9
Merge: be018a3 5605126
Author: manchot <yoan@moscatelli.fr>
Date:   Tue Nov 12 10:10:53 2019 +0100

    Merge remote-tracking branch 'origin/master'
```

Pour voir qui a modifier un fichier :

```bash
git blame docs/dev/git.md

61e9dd91 (m4nch0t           2019-06-23 20:41:21 +0200   1) # GIT
```

## Retour dans l'historique

Nous souhaitons revenir dans un état antérieur, car notre dernier commit a mis la pagaille dans notre pipeline CI/CD, heureusement il y a une commande pour cela, **git checkout**

```bash
git checkout HEAD~1
```

Nous allons récupérer  le code dans l'état où il était avant le dernier commit. Ce dernier est toujours présent sur le serveur et n'est pas modifié.
S'il y a eu trop de commit et/ou que vous ne souhaitiez pas compter, vous pouvez spécifier un numéro de commit en particulier:

```bash
git checkout c11a9316ff8536960d957bad69163e4dff914fcf
```

A partir de là, nous avons plusieurs choix:

* recoder et remplacer le commit le plus réçent par une version moins disruptive
* créer une nouvelle branche

## Création d'une nouvelle branche

Une nouvelle branche permet de travailler en collaboration sans perturber le travail des autres, par exemple sur une nouvelle feature ou simplement re-factoriser le code sans détruire ce qui est en prod. Créons une branche **dev**.

```bash
git checkout -b dev
```

Renommer une branche :

```bash
git branch -m dev new_dev
```

Pour changer de branche, vous utilisez la même commande:

```bash
git checkout master
```

Pour fusionner des branches avec un nom de commit, vous devez vous positionner dans la branche de *destination*, celle dans laquelle vous souhaitez rapatrier vos modifications.

```bash
git checkout master
git merge dev -m "Merge branch 'dev'"
```

Cette action va garder l'historique des branches fusionnées.

Si par contre vous souhaiter simplement ajouter séquentiellement vos modifications à partir de la dernière révision, sans faire apparaître votre branche d'origine, vous pouvez utiliser la fonction de rebase.

```bash
git checkout master
git rebase dev -m "Rebase branch 'dev'"
```

## Résoudre des conflits lors de fusion d'une branche

### mergetool

Pour commencer il est préférable de configurer un outil afin de visualiser les conflits, ici *vimdiff* :

```bash
git config merge.tool vimdiff
git config merge.conflictstyle diff3
```

Pour résoudre les conflits :

```bash
git mergetool
```

Vous obtiendrez un affichage dans lequel les informations seront représentées sous forme de fenêtre :

* **LOCAL** : le fichier de la branche courante
* **BASE** : contient le fichier avant changement de local ou remote
* **REMOTE** : contient le fichier que l'on souhaite fusionner
* **MERGED** : contient le résultat de la fusion qui sera sauvegardé

Pour naviguer entre les fenêtres : **ctrl + w**

Les raccourcis **vim** :

* **:diffg LO** : pour accepter les modifications de **LOCAL** (transférées par la suite dans **MERGED**)
* **:diffg BA** : pour accepter les modifications de **BASE**
* **:diffg RE** : pour accepter les modifications de **REMOTE**

### diff

Vous pouvez aussi simplement utiliser **git diff** pour voir vos conflits.

Pour voir les conflits, la sortie de la commande **git status** indique les fichiers qui n'ont pas été mergés en raison d'un conflit.

Il vous suffit simplement d'ouvrir votre fichier incriminé ou de l'afficher. Nous pouvons voir certains ajouts :

```txt
<<<<<< HEAD
======
>>>>>> branch
```

Considérez ces nouvelles lignes comme des *séparateurs de conflit* :

* La ligne **=======** est le *centre* du conflit.
* Tout le contenu entre le *centre* et la ligne **<<<<<<< HEAD** dans la branche principale actuelle vers laquelle pointe la réf HEAD (distante donc).
* Tout le contenu entre le *centre* et **>>>>>>> branch** est présent dans notre branche de dev.

Voir les conflits sur un fichier de **BASE** :

```bash
git diff --base README.md
```

Voir les conflits en acceptant les modifications distantes :

```bash
git diff --theirs README.md
```

Voir les conflits en acceptant les modifications locales:

```bash
git diff --ours README.md
```

Générer un journal contenant une liste de commits en conflit entre les branches de merge.

```bash
git log --merge
```

### Envoi

Une fois la gestion des conflits effectuées, quittez simplement vim de manière classique : **:wqa**

Une bonne pratique est de créer le commit et de nettoyer le dépôt des fichiers non suivis :

```bash
git commit
git clean
```

## Annuler ses changements

Enlever un fichier suivi par git:

```bash
git rm --cached wrong_file
```

Supprimer les modifications effectuées sur un fichier depuis le dernier commit:

```bash
git reset HEAD staged_file
```

Supprimer les modifications effectuées depuis le N dernier commit:

```bash
git reset --soft HEAD~N
```

Retirer une révision de l'historique:

```bash
git revert c11a9316ff8536960d957bad69163e4dff914fcf
```

Le **revert** va supprimer la révision en créant une nouvelle révision qui inverse les changements et permet ainsi de documenter cette suppression.

## Mettre en pause son travail

Si vous souhaiter mettre en pause vos modifications pour travailler sur une autre feature sans perdre vos modifications, vous pouvez vous tourner vers **stash**.
Il va enregistrer les modifications ne faisant pas parties d'un commit et vous permettre d'y revenir plus tard.

```bash
git stash
Saved working directory and index state WIP on master: 66ddf70 reorder nav
```

Pour voir les stash en cours:

```bash
git stash list
```

Vous pourrez ainsi voir les différents stash et leur id entre accolades.
Pour revenir dans un stash:

```bash
git stash apply stash@{0}
```

Pour le(s) supprimer:

```bash
git stash drop stash@{0}
git stash clear
```

## Rapatrier les modifications distantes

* **git fetch** : va récupérer toutes les données des commits effectués sur la branche courante qui n'existent pas encore dans votre version en local. Ces données seront stockées dans le répertoire de travail local mais ne seront pas fusionnées avec votre branche locale.

Pour réinitialiser votre dépôt local à la version présente sur le dépôt distant :

```bash
git fetch origin
```

* **git pull** : regrouper les commandes git fetch suivie de git merge.

Pour éviter de créer un **commit** de **merge**, vous pouvez indiquer à git d'effectuer un **rebase**

```bash
git pull -rebase origin master
```

Une fonctionnalité avancée aussi intéressante est le *picorage*, ou **cherry-pick**. Vous pouvez récupérer une révision de n'importe quelle branche et l'incorporer dans votre travail en cours.

```bash
git cherry-pick c11a9316ff8536960d957bad69163e4dff914fcf
git cherry-pick -x c11a9316ff8536960d957bad69163e4dff914fcf
```

L'option **-x** nous permet de rajouter dans l'historique l'information du commit *picoré*.

En cas de conflit, il suffit de spécifier la version que l'on souhaite garder :

* **ours**, garder notre version locale
* **theirs**, remplacer notre version par celle picorée.

```bash
git cherry-pick --strategy=theirs c11a9316ff8536960d957bad69163e4dff914fcf
```

## Astuces

### Script pour changer les informations sur l'auteur

```shell
#!/bin/sh

git filter-branch --env-filter '

OLD_EMAIL="wrong.email@test.lab"
NEW_EMAIL="manchot@vmanchot.net"
NEW_NAME="M4nch0t"

if [ "${GIT_COMMITTER_EMAIL}" = "${OLD_EMAIL}" ]
then
    export GIT_COMMITTER_NAME="${NEW_NAME}"
    export GIT_COMMITTER_EMAIL="${NEW_EMAIL}"
fi
if [ "${GIT_AUTHOR_EMAIL}" = "${OLD_EMAIL}" ]
then
    export GIT_AUTHOR_NAME="${NEW_NAME}"
    export GIT_AUTHOR_EMAIL="${NEW_EMAIL}"
fi
' --tag-name-filter cat -- --branches --tags
```

Il vous suffit de lancer ce script :

```bash
sh clean_author.sh
git push --force --tags origin 'refs/heads/*'
```

### Convertir un export bare vers non-bare

```shell
mkdir .git
mv branches/ .git/
mv config  .git/
mv FETCH_HEAD .git/
mv HEAD .git/
mv hooks/ .git/
mv logs/ .git/
mv objects/ .git/
mv packed-refs .git/
mv refs/ .git/
git config --local --bool core.bare false
git reset --hard
```