# Guide pour l'utilisation du Markdown

## Présentation

C'est un *language* de formatage de texte, c'est une syntaxe particulière et un format de fichier. Il est léger, lisible et facilement convertible.  
Principalement utilisé pour de la documentation (ex: README.md dans les projets Git).  

## Syntaxe

### Titres

Les niveaux de titre sont simplement gérés par les caractères #, au plus ce caractère est répété, au plus vous descendez dans l'arborescence.

    # titre 1
    ## titre 2
    ### titre 3

### Paragraphes

Pour créer un paragraphe, il vous suffit de faire 2 retours chariot.

### Saut de ligne

Pour faire un retour chariot (balise HTML **br**), saut de ligne, vous devez mettre 2 espaces à la fin de votre ligne.

### Emphase

Pour passer votre texte en _italique_ (balise **em**), vous pouvez utiliser au choix une astérisque `*` ou un underscore `_`

    *italique*
    _italique_

Pour passer votre texte en **gras** (balise **strong**), vous pouvez utiliser au choix deux astérisques `*` ou deux underscore `_`

    **gras**
    __gras__

Pour passer votre texte en ~~barre~~ (balise **del**), vous devez utiliser deux tilde `~`

    ~~barré~~

### Citations

Pour afficher un bloc de citation (balise **blockquote**), commencez le paragraphe par un chevron fermant `>`

    > citation

> citation

### Listes

Dans les deux cas, il suffit d'une indentation pour aller au niveau inférieur.

#### liste non ordonnée

Pour afficher une liste (balise **li**), commencez la ligne au choix par une astérisque `*`, un moins `-` ou un plus `+`

    * liste 1
        * sous liste 1
    * liste 2

* liste 1
  * sous liste 1
* liste 2

#### liste ordonnée

Pour afficher une liste ordonnée, commencez la ligne par un nombre suivi d’un point.

    1. liste 1
        1. sous liste 1
    2. liste 2

1. liste 1
    1. sous liste 1
2. liste 2

### Liste de tâches

Pour créer une liste de tâches, il vous suffit de commencer par un tiret `-` ou une étoile `*` et de positionner entre crochets `[]` un `x`ou non suivant l'état de réalisation.

    * [x] ne pas mouiller Guizmo
    * [x] installer le blog à mamie
    * [ ] apporter la démocratie en Corée du Nord

* [x] ne pas mouiller Guizmo
* [x] installer le blog à mamie
* [ ] apporter la démocratie en Corée du Nord

### Code

#### Bloc de code

Pour créer un bloc de code, il vous suffit de sauter deux fois de ligne et d'indenter avec 4 espaces ou une tabulation. Cela va créer un bloc de code dans votre affichage. Ce dernier s'arrête à la première ligne non indentée.

Ou suivant votre outils de génération de page HTML, il est possible d'utiliser le triple  guillemets simples \` \` \` en début et fin de votre bloc.
Voici la liste des [languages reconnus] ( <https://github.com/github/linguist/blob/master/lib/linguist/languages.yml> "github languages").

#### Code sur une ligne

Là aussi nous allons utiliser les guillemets simples, mais cette fois nous allons entourer notre code d'un seul guillemet simple.

### Séparateur

Si vous voulez mettre un filet de séparation (balise **hr**), vous avez le choix:

* 3 astérisques `*`
* 3 moins `-`

### Liens

#### Lien simple

Pour créer un simple lien cliquable en automatique, il vous suffit d'entourer votre url de chevrons ouvrant et fermant, `<http://url>`

#### Lien avancé

Vous pouvez spécifier des paramètres pour votre lien.

* entre crochets `[]` vous allez mettre le texte de votre lien
* mettre votre lien entre parentheses `()`
* à la suite de votre lien, toujours dans les parentheses, le texte entouré de double guillemets `"`, à afficher lorsque l'on passera le curseur dessus

`[texte] (http://url "lien vers mon super site")`

### Tableaux

Pour créer un tableau vous devez:

* écrire une ligne d'entête
* dessous cette dernière, placer une ligne de tirets `-`
* séparer les colonnes avec des pipe `|`
* spécifier l'alignement avec les deux points `:`
* les pipes `|`sont optionnels en début ou fin de ligne

```bash
    | Aligné à gauche  | Centré          |  Aligné à droite |  
    | :--------------- |:---------------:| ----------------:|  
    | Aligné à gauche  | ce texte        |  Aligné à droite |  
    | Aligné à gauche  | est             |  Aligné à droite |  
    | Aligné à gauche  | centré          |  Aligné à droite |  
```

Aligné à gauche  | Centré          |  Aligné à droite
:--------------- |:---------------:| ---------------:
Aligné à gauche  | ce texte        |  Aligné à droite
Aligné à gauche  | est             |  Aligné à droite
Aligné à gauche  | centré          |  Aligné à droite

### Échappement

Pour échapper les caractères markdown qui seront interprétés, il vous suffit comme souvent de les précéder d'un anti slash \\

   \ * ` - _ [] () {} # + . !
