# Installation et utilisation de podman sous macOS

## Introduction

Podman permet de gérer des containers sur une instance Linux, il se base sur un runtime OCI (runc, containerd, cri-o) pour instancier des containers.
Comme le fait Docker sous macOS, il faut donc avoir accès à un Linux, le plus souvent en machine virtuelle.
Donc pour reprendre les fonctionnalités de Docker Desktop, nous devons pouvoir démarrer rapidement une machine virtuelle Linux, j'utilise Multipass (de Canonical) dans notre exemple, plus rapide et agréable que virtualbox ou vmware workstation.

## Multipass

### Installation et première vm

Il se base sur Hyperkit, l'hyperviseur opensource pour macOS.

```
$ brew install multipass podman
```

Il permet de démarrer des machines virtuelles Ubuntu qui peuvent très bien convenir à un envirronement de dev.

```
$ multipass find

Image                       Aliases           Version          Description
snapcraft:core18                              20201111         Snapcraft builder for Core 18
snapcraft:core20                              20201111         Snapcraft builder for Core 20
snapcraft:core                                20210430         Snapcraft builder for Core 16
18.04                       bionic            20210512         Ubuntu 18.04 LTS
20.04                       focal,lts         20210510         Ubuntu 20.04 LTS
20.10                       groovy            20210511.1       Ubuntu 20.10
```

Pour rajouter votre clé ssh à la création de la machine virtuelle:

```
ssh-keygen -t rsa -b 2048 -f ~/.ssh/id_multipass
```

Puis édition du fichier de configuration disponible ici [cloud-init.yaml] (https://raw.githubusercontent.com/hutger/podman-on-mac/main/ubuntu-20.10/cloud-init.yaml) pour y rajouter votre clé publique.

```
$ cat ~/cloud-config.yaml

---
users:
  - name: ubuntu
    ssh-authorized-keys:
      - ssh-rsa /BzKN9n... manchot@my-mac

apt_sources:
  - source: "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/xUbuntu_20.10/ /"
    key: |
      -----BEGIN PGP PUBLIC KEY BLOCK-----
      Version: GnuPG v1.4.5 (GNU/Linux)

      mQENBFtkV0cBCADStSTCG5qgYtzmWfymHZqxxhfwfS6fdHJcbGUeXsI5dxjeCWhs
      XarZm6rWZOd5WfSmpXhbKOyM6Ll+6bpSl5ICHLa6fcpizYWEPa8fpg9EGl0cF12G
      GgVLnnOZ6NIbsoW0LHt2YN0jn8xKVwyPp7KLHB2paZh+KuURERG406GXY/DgCxUx
      Ffgdelym/gfmt3DSq6GAQRRGHyucMvPYm53r+jVcKsf2Bp6E1XAfqBrD5r0maaCU
      Wvd7bi0B2Q0hIX0rfDCBpl4rFqvyaMPgn+Bkl6IW37zCkWIXqf1E5eDm/XzP881s
      +yAvi+JfDwt7AE+Hd2dSf273o3WUdYJGRwyZABEBAAG0OGRldmVsOmt1YmljIE9C
      UyBQcm9qZWN0IDxkZXZlbDprdWJpY0BidWlsZC5vcGVuc3VzZS5vcmc+iQE+BBMB
      CAAoBQJfcJJOAhsDBQkIKusHBgsJCAcDAgYVCAIJCgsEFgIDAQIeAQIXgAAKCRBN
      ZDkDdQYKpB0xCACmtCT6ruPiQa4l0DEptZ+u3NNbZfSVGH4fE4hyTjLbzrCxqcoh
      xJvDKxspuJ85wWFWMtl57+lFFE1KP0AX2XTT+/v2vN1PIfwgOSw3yp2sgWuIXFAi
      89YSjSh8G0SGAH90A9YFMnTbllzGoGURjSX03iasW3A408ljbDehA6rpS3t3FD7P
      PnUF6204orYu00Qvc54an/xVJzxupb69MKW5EeK7x8MJnIToT8hIdOdGVD6axsis
      x+1U71oMK1gBke7p4QPUdhJFpSUd6kT8bcO+7rYouoljFNYkUfwnqtUn7525fkfg
      uDqqXvOJMpJ/sK1ajHOeehp5T4Q45L/qUCb3iEYEExECAAYFAltkV0cACgkQOzAR
      t2udZSOoswCdF44NTN09DwhPFbNYhEMb9juP5ykAn0bcELvuKmgDwEwZMrPQkG8t
      Pu9n
      =42uC
      -----END PGP PUBLIC KEY BLOCK-----

package_update: true
packages:
  - podman
  - containerd

runcmd:
  - sudo apt upgrade -y
  - systemctl --user enable podman.socket
  - sudo systemctl enable -s HUP ssh
  - sudo loginctl enable-linger $USER
```

Nous pouvons maintenant démarrer votre machine virtuelle :

```
multipass launch -c 2 -m 4G -d 10G -n podman-ftw 20.10 --cloud-init ~/cloud-config.yaml
```

* **-c** : nombre de coeurs alloués
* **-m** : quanité de mémoire allouée
* **-d** : quantité d'espace disque alloué
* **-n** : nom de votre machine virtuelle, les underscore \_ ne sont pas autorisés.
* **--cloud-init** : chemin vers votre fichier de configuration cloud-init

Pour vous connecter à la machine virtuelle et passer à la suite du tuto

```
multipass shell podman-ftw
```

### Commandes utiles

* multipass find : chercher des images
* multipass launch ubuntu : démarrer une nouvelle VM ubuntu vierge
* multipass list : lister les VMs
* multipass info ubuntu : afficher des informations sur la VM ubuntu
* multipass exec podman-ftw -- hostname -f : éxécuter la commande *hostname -f* à l'intérieur de la vm et vous ressortir le résultat
* multipass stop podman-ftw : stopper la vm podman-ftw
* multipass delete podman-ftw : supprimer l'instance podman-ftw
* multipass purge : supprimer toutes les instances (stopped + deleted)

## Podman

Il vous reste quelques modifications à effectuer pour que podman puisse fonctionner

Il vous faut activer le mode rootless afin de pouvoir vous y connecter à distance avec l'utilisateur ubuntu.

```
systemctl --user enable podman.socket
```

Puis pour que le socket soit ouvert sans que l'utilisateur soit connecté :

```
sudo loginctl enable-linger $USER
```

Et redémarrer votre vm

```
sudo systemctl reboot
```

Ces étapes étranges car les commandes sont présentes dans le cloud-config. #TODO

Pour vérifier que tout s'est bien passé et que votre vm est fonctionnelle :

```
multipass shell podman-ftw
Welcome to Ubuntu 20.10 (GNU/Linux 5.8.0-53-generic x86_64)

 * Documentation:  https://help.ubuntu.com
....

ubuntu@podman-ftw:~$ podman --remote info
host:
  arch: amd64
  buildahVersion: 1.20.1
  cgroupManager: cgroupfs
  cgroupVersion: v1
  conmon:
    package: 'conmon: /usr/libexec/podman/conmon'
    path: /usr/libexec/podman/conmon
    version: 'conmon version 2.0.27, commit: '
  cpus: 2
  distribution:
    distribution: ubuntu
    version: "20.10"
  ...
```

Récupérer l'adresse ip de la vm :

```
multipass info podman-ftw |grep IP | awk -F ":" '{ print $2 }'

172.16.113.10
```

Une fois ces étapes terminées vous n'avez plus qu'à spécifier à votre client podman sur votre mac de contacter votre vm :

```
podman system connection add multipass --identity ~/.ssh/id_multipass ssh://ubuntu@172.16.113.10

podman info
host:
  arch: amd64
  buildahVersion: 1.20.1
  cgroupManager: cgroupfs
  cgroupVersion: v1
  conmon:
    package: 'conmon: /usr/libexec/podman/conmon'
    path: /usr/libexec/podman/conmon
    version: 'conmon version 2.0.27, commit: '
  cpus: 2
  distribution:
    distribution: ubuntu
...
```

Et commencer à travailler :)

```
$ podman run -it centos:8 /bin/sh

sh-4.4# cat /etc/centos-release
CentOS Linux release 8.3.2011
```

## OCI

Podman supporte plusieurs registries. Quand vous spécifiez un nom d'image qui ne contient pas le nom d'un registry (exemple docker.io/library/centos:7), podman va consulter le fichier de configuration des registries **/etc/containers/registries.conf**.
Si vous souhaitez rajouter ou envlever des registries vous n'avez qu'à éditer ce fichier et modifier la partie suivante :

```ini
unqualified-search-registries = ["registry.access.redhat.com", "docker.io", "quay.io"]
```

Dans cet exemple j'ai rajouté le registry Redhat en début de liste pour qu'il soit la première source testé. Mais vous pouvez positionner uniquement le votre.

Ce fichier de configuration est assez riche et bien docummenté, je ne m'attarderai donc pas dessus.

## Multipass ne démarre plus après une purge

Si vous rencontrez une erreur de ce type

```
multipass list
list failed: cannot connect to the multipass socket
Please ensure multipassd is running and '/var/run/multipass_socket' is accessible
```

Voici comment repartir d'une installation propre :

```
sudo launchctl unload /Library/LaunchDaemons/com.canonical.multipassd.plist
sudo echo "{\n}" > /var/root/Library/Application\ Support/multipassd/multipassd-vm-instances.json
sudo launchctl load /Library/LaunchDaemons/com.canonical.multipassd.plist
```

Vous pouvez vérifier que vous n'avez pas de reliqua de vm :

```
sudo ls /var/root/Library/Application\ Support/multipassd/vault/instances
```

## Podman et zsh

```
mkdir -p $ZSH_CUSTOM/plugins/podman/
podman completion zsh -f $ZSH_CUSTOM/plugins/podman/_podman
```