# Ansible Tower

## Composants

* HTTP Services: Tower et ses webservices
* Callback Receiver: Reçois les événements des jobs Ansible en cours
* Dispatcher: La file de travail de tous les jobs
* RabbitMQ: Le message broker, qui fait le lien avec le dispatcher
* Memcached: Service de cache

Tower est configuré de telle sorte que l'échec d'un de ces services redémarre la totalité des services soit redémarrée. Si plusieurs échecs interviennent dans un laps de temps court, l'instance est mise hors ligne.

## Arrêt et redémarrage

Ansible Tower possède une commande qui permet de démarrer, relancer tous les services concourant à Ansible Tower. Le binaire est positionné ici : */usr/bin/ansible-tower-service*

```bash
ansible-tower-service restart
```

Pour vérifier que tous les services du supervisor sont démarrés :

```bash
supervisorctl status
```

## Logs

Dans le répertoire */var/log/tower/* vous pouvez retrouver les différents logs:

* callback_receiver.log
* fact_receiver.log
* setup-XX-XX-XX-XX.log
* socketio_service.log
* task_system.log
* tower.log

Dans le répertoire */var/log/supervisor/* vous pouvez retrouver les différents logs:

* awx-dispatcher.log
* awx-daphne.log
* awx-channels-worker.log
* awx-uwsgi.log
* supervisord.log
* stdout (contient la sortie de tous les services)

## Nettoyage

Il est important de s'assurer de ne pas garder un historique trop important en base de donnée. Tout dépend de l'utilisation, du nombre de *job templates* lancés par jour et du nombre d'utilisateurs humains ou non qui s'y connectent.

Plusieurs jobs existent dans l'interface, il vous suffit de leur donner une périodicité ou de les lancer au besoin :

* Cleanup Activity Stream: Supprime le flux d'activités plus anciens qu'un certain nombre de jours.
* Cleanup Expired OAuth 2 Tokens: Supprime les jetons OAuth 2 expirés et les rafraîchis.
* Cleanup Expired Sessions: Supprime les sessions expirées.
* Cleanup Job Details: Supprime l'historique des jobs plus anciens qu'un certain nombre de jours.

Vous les retrouverez dans la partie *Management Job*.

## Statut et monitoring

Pour vérifier que votre instance Tower fonctionne, vous pouvez faire un simple appel api

```bash
curl https://ansible-tower.lan/api/v2/ping
```

Vous retrouvez des informations sur :

* L'instance vers laquelle vous avez fait la requête
* La date du dernier heartbeat de toutes les instances du cluster
* Les groupes d'instances et leur appartenance

## Supprimer une instance du cluster

1. Éteindre l'instance ou stopper le service.

2. Lancer la commande de dé-provisionnement sur un des autres serveurs du cluster :

```bash
awx-manage deprovision_instance --hostname=hostame-instance
```

## Sauvegarde et restauration

Pour sauvegarder et restaurer votre instance, vous devez avoir le répertoire contenant les sources de l'installation et lancer le script d'installation soit avec l'argument *-r* pour la restauration ou *-b* pour la sauvegarde.

```bash
./setup.sh -r
./setup.sh -b
```

Attention, la restauration n'est viable qu'entre des instances de même version!

## Changer le mot de passe admin

```bash
awx-manage changepassword admin
```

## Python virtualenv

Par défaut des *virtualenv* sont créer à l'installation en fonction de votre distribution et des versions de python disponibles. Des environnement python isolés sont créés afin de se prémunir de problèmes liés aux conflit de dépendances.
Exemple de la mise à jour d'un package python dans l'environnement ansible:

```bash
source /var/lib/awx/venv/ansible/bin/activate
umask 0022
pip install --upgrade pywinrm
deactivate
```

Voici les deux *venv* nécessaire :

* /var/lib/awx/venv/ansible
* /var/lib/awx/venv/awx

Pour créer un environnement custom avec une version d'Ansible différente :

```bash
mkdir /opt/venvs
chmod 755 /opt/venvs
python3 -m venv /opt/venvs/custom_venv
/opt/venvs/custom_venv/bin/pip install --upgrade pip
/opt/venvs/custom_venv/bin/pip install psutil
/opt/custom_venv/bin/pip install -U "ansible == 2.9.5"
```

On peut spécifier à tower dans quel répertoire rechercher les *venv* custom.

```bash
curl -X PATCH 'https://admin:password@ansible-tower.lan/api/v2/settings/system/' -d '{"CUSTOM_VENV_PATHS": ["/opt/venvs/"]}'  -H 'Content-Type:application/json'
```

Pour le faire prendre en compte pour un projet/organisation/inventaire/job de manière spécifique via un appel API:

```bash
curl -X PATCH 'https://admin:password@ansible-tower.lan/api/v2/organizations/1/' -d '{"custom_virtualenv": "/opt/venvs/custom_venv/"}'  -H 'Content-Type:application/json'
```

Les différents endroits où appliquer un venv custom:

* PATCH <https://ansible-tower.lan/api/v2/organizations/N/>
* PATCH <https://ansible-tower.lan/api/v2/projects/N/>
* PATCH <https://ansible-tower.lan/api/v2/job_templates/N/>
* PATCH <https://ansible-tower.lan/api/v2/inventory_sources/N/>

Et là vous trouverez lors de la prochaine exécution en débug la sortie suivante:

```bash
ansible-playbook 2.9.5
  config file = /etc/ansible/ansible.cfg
  configured module search path = ['/var/lib/awx/.ansible/plugins/modules', '/usr/share/ansible/plugins/modules']
  ansible python module location = /opt/venvs/custom_venv/lib/python3.6/site-packages/ansible
  executable location = /opt/venvs/custom_venv/bin/ansible-playbook
  python version = 3.6.8 (default, Aug  7 2019, 17:28:10) [GCC 4.8.5 20150623 (Red Hat 4.8.5-39)]
```

Au lieu de :

```bash
ansible-playbook 2.9.1
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/var/lib/awx/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/site-packages/ansible
  executable location = /usr/bin/ansible-playbook
  python version = 2.7.5 (default, Aug  7 2019, 00:51:29) [GCC 4.8.5 20150623 (Red Hat 4.8.5-39)]
```
