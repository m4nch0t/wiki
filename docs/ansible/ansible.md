# Ansible

## Installation

Le moteur Ansible est installable via pip :

```bash
python3 -m pip install ansible
```

Vous pouvez installer AWX qui est le pendant communautaire d'Ansible Tower sur Debian par exemple :

```bash
apt install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common git python-pip
echo "deb http://ftp.debian.org/debian stretch-backports main contrib non-free" > /etc/apt/sources.list.d/stretch-backports.list
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
curl -fsSL https://download.docker.com/linux/debian/gpg | sudo apt-key add -
apt update
apt install -y -t stretch-backports docker-ce docker-compose
cd ~
git clone https://github.com/ansible/awx
cd ~/awx/installer
sed -i "s|/tmp/pgdocker|/opt/pgdocker|g" inventory
sed -i "s|# use_docker_compose=false|use_docker_compose=true|g" inventory
sed -i "s|#project_data_dir=/var/lib/awx/projects|project_data_dir=/opt/awx_projects|g" inventory
pip install docker
pip install docker-compose
ansible-playbook install.yml -i inventory
```

## Ajouter l'utilisateur ansible via root

```bash
ansible all -a "useradd ansible -s /bin/bash"
cat ~/.ssh/id_rsa.pub | ssh root@198.168.1.100 "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >>  ~/.ssh/authorized_keys && chown -R ansible. /home/ansible/"
```

## Utilisation des rôles

Les roles sont l'équivalent des **Cookbooks** pour **Chef** ou **modules** pour **Puppet**. Ce sont des containers d'actions réutilisables.
Pour créer un role, il suffit de nous positionner dans l'arborescence par défaut d'Ansible et d'initialiser sa création. Prenons l'exemple d'un role syslog-ng :

```bash
cd /etc/ansible/roles
ansible-galaxy init syslog-ng --offline
```

Voici ce que l'on trouvera à l'issue de la commande :

```bash
ansible@ansible:/etc/ansible/roles$ tree syslog-ng/
syslog-ng/
├── defaults
│   └── main.yml
├── files
├── handlers
│   └── main.yml
├── meta
│   └── main.yml
├── README.md
├── tasks
│   └── main.yml
├── templates
├── tests
│   ├── inventory
│   └── test.yml
└── vars
    └── main.yml
```

Un aperçu des dossiers :

* **defaults**, va contenir les données de votre rôle, ses variables par défaut
* **files**, va contenir les fichiers statiques qui vont être copiés vers le serveur client
* **handlers**, ce sont des listes de tâches, qui sont référencées par un nom unique et déclenchées par **notify**. Peu importe le nombre de tâches qui notifient le handler, il ne sera lancé qu'une seule fois, à la fin du playbook.
* **meta**, va décrire les informations sur votre rôle, mainteneur, OS supportés, dépendances, etc...
* **tasks**, va contenir les actions
* **templates**, va contenir les fichiers dynamiques
* **vars**, va contenir les variables

## Cheat Sheet

### Ad-hoc, users et ssh

Tester la connectivité à tous les hosts de votre inventaire ou simplement un groupe ou un seul host:

```bash
ansible all -m ping
ansible test-server -m ping
```

Tester la connectivité à tous les hosts d'un autre fichier d'inventaire ou simplement un groupe ou un seul host:

```bash
ansible -i /etc/ansible/hosts -m ping
ansible -i /etc/ansible/hosts test-server -m ping
```

Lister les facts d'un host:

```bash
ansible -i /etc/ansible/hosts test-server -m setup
```

Lancer des modules ou un playbook avec un utilisateur spécifique:

```bash
ansible all -m ping -u manchot
ansible-playbook my_awesome_playbook.yml -u manchot
```

Utiliser une clé ssh alternative:

```bash
ansible all -m ping --private-key=~/.ssh/backup_id
ansible-playbook my_awesome_playbook.yml --private-key=~/.ssh/backup_id
```

Utiliser une connexion avec mot de passe:

```bash
ansible all -m ping --ask-pass
ansible-playbook my_awesome_playbook.yml --ask-pass
```

Utiliser une connexion avec mot de passe sudo:

```bash
ansible all -m ping --ask-become-pass
ansible-playbook my_awesome_playbook.yml --ask-become-pass
```

Lancer des commandes ad-hoc avec l'argument `-a`:

```bash
ansible all -a "echo ansible FTW"
```

Lancer des commandes ad-hoc en utilisant un module:

```bash
ansible all -m yum -a "name=htop"
ansible srv-apache -m shell -a "systemctl restart httpd"
```

Si vous avez besoin d'avoir des informations sur l'utilisation d'un module et que vous n'avez pas internet sous la main, utiliser la commande `ansible-doc` suivie du nom de votre module:

```bash
ansible-doc ping
```

### Modification de l'éxécution d'un playbook

Surcharger la variable host présente dans le playbook avec l'argument `-l`:

```bash
ansible-playbook -l test-server my_awesome_playbook.yml
```

Vérification des hosts affectés par l'éxécution d'un playbook sans l'exécuter:

```bash
ansible-playbook my_awesome_playbook.yml --list-hosts
```

Lister les tâches d'un playbook sans l'exécuter:

```bash
ansible-playbook my_awesome_playbook.yml --list-tasks
```

Lister les tags d'un playbook sans l'exécuter:

```bash
ansible-playbook my_awesome_playbook.yml --list-tags
```

On peut exécuter seulement un ou plusieurs *tags* d'un playbook avec l'argument `--tags=` :

```bash
ansible-playbook my_awesome_playbook.yml --tags=configrepos,installhtop
```

On peut aussi démarrer l'exécution d'un playbook à partir d'une tâche en particulier:

```bash
ansible-playbook my_awesome_playbook.yml --start-at-task="Config repos"
```

Ou encore ne pas exécuter une certaine tâche, qui par exemple n'est pas idempotente:

```bash
ansible-playbook my_awesome_playbook.yml --skip-tags="Secure OS"
```

### Vault

Créer un fichier chiffré:

```bash
ansible-vault create secret-vars.yml
```

Chiffrer un fichier déjà existant:

```bash
ansible-vault encrypt secret-vars.yml
```

Visualiser un fichier chiffré:

```bash
ansible-vault view secret-vars.yml
```

Éditer un fichier chiffré:

```bash
ansible-vault edit secret-vars.yml
```

Déchiffrer un fichier chiffré:

```bash
ansible-vault decrypt secret-vars.yml
```

Une fonctionnalité intéressante du vault est l'utilisation des **vault-id**. En effet, vous pouvez partager les variables de différents environnements (dev/prod/int) possédant chacun leur propre mot de passe. Pour cela vous devez utiliser l'argument `--vault-id=` et spécifier une méthode pour fournir le mot de passe, un fichier de mot de passe ou le taper directement ( `@prompt` ):

```bash
ansible-vault create --vault-id prod@very_secret_password_file
ansible-vault create --vault-id prod@prompt secret-vars-dev.yml
```

Différents cas d'éxécution de playbook avec des vaults:

```bash
ansible-playbook my_awesome_playbook.yml --ask-vault-pass
ansible-playbook my_awesome_playbook.yml --vault-password-file very_secret_password_file
ansible-playbook my_awesome_playbook.yml --vault-id prod@prompt
```

### Troubleshooting

Si vous voulez augmenter la verbosité de l'éxécution de vos playbook, il suffit d'utiliser l'argument `-v`. Pour encore plus de verbosité il suffit d'ajouter des **v** :  `-vvvv`

### Playbook

#### Exemple d'installation d'apache

```yaml
    ---

    - hosts: apache
      become: yes
      tasks:
        - name: Install apache
          yum:
            name: httpd
            state: present
```

#### Les boucles

Depuis Ansible 2.5 les boucles ***with_*** sont remplacées par **loop**

Exemple, installation d'apache et php avec une liste simple:

```yaml
    ---

    - hosts: apache
      become: yes
      tasks:
        - name: Install apache
          yum:
            name: "{{ item }}"
            state: present
          loop:
            - httpd
            - php
```

#### Listes et dictionnaires

Exemple d'utilisation d'une liste complexe:

```yaml
    ---

    - hosts: apache
      become: yes
      tasks:
        - name: Install apache
          yum:
              name: "{{ item.name }}"
              state: "{{ item.state }}"
          loop:
            - { name: 'httpd', state: 'present' }
            - { name: 'php', state: 'present' }
```

Exemple d'utilisation d'un dictionnaire:

```yaml
    ---

    - hosts: apache
      become: yes
      vars:
        packages:
          - name: httpd
            state: present
          - name: php
            state: present
      tasks:
        - name: Install apache
          yum:
              name: "{{ item.name }}"
              state: "{{ item.state }}"
          loop: "{{ packages }}"
```

#### Inclure un fichier de tâches en fonction de la distribution

```yaml
    - name: include OS family/distribution specific task file
      include: "{{ item }}"
      with_first_found:
        - "{{ ansible_os_family | lower }}/{{ ansible_distribution | lower }}_{{ ansible_distribution_version | lower }}.yml"
        - "{{ ansible_os_family | lower }}/{{ ansible_distribution | lower }}.yml"
        - "{{ ansible_os_family | lower }}.yml"
```

Un exemple d'arborescence de votre dossier **tasks**:

```yaml
    ├── tasks
    │   ├── centos.yml
    │   ├── debian.yml
    │   ├── main.yml
    │   └── redhat.yml
```

### Récupération de variables

Si nous sommes dans un role, donc sans possibilité d'utilisation des group_vars et que
vous souhaitez récupérer une hostvars générée pendant l'éxécution d'une task vous avez plusieurs solutions :

En jouant avec les filtres, voici un extrait d'un role d'installation chrony :

```yaml
- name: generate chrony key
  command: chronyc keygen 10 SHA512 2048
  register: __generated_chrony_key
  when: chrony_generate_keys
  # Ici la commande est exécutée sur un seul host et le register positionne la variable dans ses hostvars, non utilisable par les autres hosts

- debug:
    var: __generated_chrony_key.stdout
    verbosity: 2
  when: chrony_generate_keys
  # On vérifie que la donnée enregistrée est correcte dans les hostvars du host

- name: get the generated key
  set_fact:
    __chrony_key: "{{ groups['all'] | map('extract', hostvars) | selectattr('__generated_chrony_key', 'search', 'stdout') | map(attribute='__generated_chrony_key') | list }}"
    # On extrait toutes les hostvars de tous les hosts ciblés et on recherche la variable générée précedement et on fait le tri des données inutiles
    # Si vous n'êtes pas sûr du type de donnée vous pouvez remplacer le filtre list par type_debug

- debug:
    var: __chrony_key
    verbosity: 2
  # Ici je vois une liste contenant un dictionnaire

- debug:
    var: __chrony_key.0.stdout
    verbosity: 2
  # Je sors donc la première entrée (0) contenant la donnée souhaitée
```

Une autre méthode, lancer une loop sur tous les hosts et ne garde que le résultat correct :

```yaml
- name: generate and get chrony key
  block:

  - name: generate chrony key
    command: chronyc keygen 10 SHA512 2048
    register: __generated_chrony_key
    loop: "{{ ansible_play_hosts }}"
    delegate_to: "{{ item }}"
    when: hostvars[item].chrony_generate_keys is defined

  - set_fact:
      __chrony_key: "{{ item.stdout }}"
    loop: "{{ __generated_chrony_key.results }}"
    when: item.stdout is defined
    no_log: true
  run_once: true

- name: debug chrony key var
  debug:
    var: __chrony_key
    verbosity: 2
```

### Exclure le host de son groupe

Lorsque l'on déploie des clusters on ne souhaite pas que le serveur soit dans la liste de ses pairs :

```yaml
chrony_ntp_peers: "{{ groups['servers'] | reject('equalto', inventory_hostname) | list }}"
```
