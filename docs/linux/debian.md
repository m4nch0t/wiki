# Debian

Vous trouverez ici un ensemble de documentation pour gérer votre serveur Debian.

## Gagner de l’espace disque avec APT

Si votre serveur sous Debian (ou dérivée) commence à manquer d’espace, voici comment gagner quelques centaines de Mo voire Go rien qu’en utilisant APT.
Un paquet qui permet de supprimer les fichiers de langues (locales) autres que la votre :

```bash
root@vps:~# apt-get install localepurge
root@vps:~# localepurge
```

Ensuite vous avez le couple clean/autoclean.

La commande clean nettoie le référentiel local des paquets récupérés. Elle supprime tout ce qui se trouve dans le cache APT (/var/cache/apt), excepté le fichier de verrou situé dans **/var/cache/apt/archives/** et **/var/cache/apt/archives/partial/**.

```bash
root@vps:~# apt-get clean
```

Tout comme clean, autoclean nettoie le référentiel local des paquets récupérés. La différence est qu'il supprime uniquement les paquets qui ne peuvent plus être téléchargés et qui sont inutiles.

```bash
root@vps:~# apt-get autoclean
```

Pour supprimer les dépendances qui sont devenues inutiles :

```bash
root@vps:~# apt-get autoremove
```

## Maîtriser ses mises à jours sous Debian

Que vous aillez besoin d’avoir des paquets en testing alors que vous souhaitez restez en stable, et/ou que vous le voulez pas mettre à jour votre noyau, il vous faut pourvoir maitriser ce que fait apt.

### Pré-requis

ous Debian, nous avons trois versions:

* stable, version figée, ou il n’y a que les mises à jour de sécurité.
* testing future stable, dernière ligne droite des paquets avant la validation finale.
* unstable, (appelé  sid), mise à jour incessante, bac à sable.

APT gère des priorités de mises à jour:

* 1001 : le paquet ne sera jamais remplacé par APT.
* 1000 : idem, mais APT refusera d’installer le paquet si une autre version est déjà présente.
* 990 : Le paquet ne pourra être remplacé que si une version supérieure est disponible dans la distribution utilisée.
* 500 : Toute version du paquet supérieure à celle présente sera installée.
* 100 : Toute version du paquet, supérieure ou inférieure, remplacera la version en place.
* -1 : On empêche un paquet (ou une version spécifique) d’être installé.

### Modification

Pour cela, vous devrez modifier deux fichiers :

```bash
/etc/apt/preferences
/etc/apt/sources.list
```

Dans le **preferences**, la syntaxe doit être de ce type:

```bash
Package:NomDuPaquet
Pin: release a=VersionDeDebian(stable,unstable,testing)
Pin-Priority: NuméroDePriorité
```

Exemples:

Je met à jour les paquets si ils sont plus récent sur testing.

```bash
Package: *
Pin: release a=testing
Pin-Priority: 990
```

J’installe toutes les nouvelles sources du noyau.

```bash
Package: linux-source-2.6.*
Pin: release a=unstable
Pin-Priority: 550
```

Le sources.list doit être plus fournis, car apt doit savoir où trouver les paquets qui vont bien. Voici l’exemple d’un source.list:

```bash
# Stable
deb http://security.debian.org stable/updates main contrib non-free
deb-src http://security.debian.org/ stable/updates main contrib non-free
deb http://ftp.fr.debian.org/debian stable main contrib non-free
deb-src http://ftp.fr.debian.org/debian stable main contrib non-free
# Testing
deb http://security.debian.org buster/updates main contrib non-free
deb-src http://security.debian.org/ buster/updates main contrib non-free
deb http://ftp.fr.debian.org/debian buster main contrib non-free
deb-src http://ftp.fr.debian.org/debian buster main contrib non-free
# Unstable
deb http://ftp.fr.debian.org/debian unstable main contrib non-free
deb-src http://ftp.fr.debian.org/debian unstable main contrib non-free
```

### Mises à jour automatiques avec Unattended Upgrades

Nous pouvons par exemple mettre à jour notre système avec les dernières mise à jour de sécurité.

```bash
apt-get install unattended-upgrades
```

Maintenant configurons en éditant le fichier **/etc/apt/apt.conf.d/50unattended-upgrades**. Pour les mises à jour dé sécurité, il vous suffit de rajouter ces lignes dans le bloc **Unattended-Upgrade::Origins-Pattern {**

```bash
"o=Debian,a=stable";
"o=Debian,a=stable-updates";
"origin=Debian,archive=stable,label=Debian-Security";
```

Et modifier les lignes suivantes:

```bash
Unattended-Upgrade::AutoFixInterruptedDpkg "true";
Unattended-Upgrade::MinimalSteps "true";
Unattended-Upgrade::InstallOnShutdown "false";
Unattended-Upgrade::Mail "root";
Unattended-Upgrade::MailOnlyOnError "true";
Unattended-Upgrade::Remove-Unused-Dependencies "true";
Unattended-Upgrade::Automatic-Reboot "false";
```

Il nous reste plus qu'à créer le fichier **/etc/apt/apt.conf.d/02periodic** afin de planifier quotidiennement nos mises à jour:

```shell
// Enable the update/upgrade script (0=disable)
APT::Periodic::Enable "1";

// Do "apt-get update" automatically every n-days (0=disable)
APT::Periodic::Update-Package-Lists "1";

// Do "apt-get upgrade --download-only" every n-days (0=disable)
APT::Periodic::Download-Upgradeable-Packages "1";

// Run the "unattended-upgrade" security upgrade script
// every n-days (0=disabled)
// Requires the package "unattended-upgrades" and will write
// a log in /var/log/unattended-upgrades
APT::Periodic::Unattended-Upgrade "1";

// Do "apt-get autoclean" every n-days (0=disable)
APT::Periodic::AutocleanInterval "7";
```

Les logs sont présent dans **/var/log/unattended-upgrades/unattended-upgrades.log**

Effectuer la mise à jour est une bonne chose, mais sans le redémarrage du service concerné par le patch, la vulnérabilité restera présente.
A partir de Jessie (Debian 8) vous pouvez installer le paquet needrestart qui va redémarrer les services concernés par le patch de sécurité.

```bash
apt install needrestart
```

## Commandes utiles

Si vous souhaitez télécharger un paquet sans l'installer :

```shell
apt-get download nom-du-paquet
```

## Sécurisation

### Audit par lynis

Il parcourt la configuration du système et crée un résumé des informations système et des problèmes de sécurité, utilisable par des auditeurs professionnels. Il peut aider à des audits automatisés.

```bash
apt install lynis
```

Pour lancer une vérification du système avec seulement l'affichage des warnings :

```bash
lynis --check-all --quick
```

Vous trouverez le rapport dans **/var/log/lynis-report.dat** et sa documentation pour aller plus loin sur le site officiel.

### debsums

Debsums permet de vérifier l'intégrité des fichiers des paquets installés avec les sommes de contrôle MD5 installées par le paquet ou générées à partir d'une archive ".deb".

Pour l'installer :

```bash
apt install debsums
```

Pour le lancer (vérifie les fichiers de configurations --all et n'affiche que les erreurs : --silent) :

```bash
debsums --all --silent
```

Vous avez bien entendu différentes options que vous pouvez voir dans les pages man (man debsums). Pour générer les sommes de contrôle MD5 à partir des fichiers .deb pour les paquets qui n'en fournissent pas :

```bash
debsums --generate=missing
```

Cette commande est bien entendu à utiliser sur un système neuf.

### deborphan

Deborphan recherche les paquets orphelins sur votre système en déterminant quels paquets n'ont aucune dépendance sur eux et vous affiche la liste de ces paquets.

```bash
apt install deborphan
```

Je vous conseille grandement de vérifier ce qu'il propose d'enlever avant de supprimer les paquets :

```bash
deborphan
```

Et si ça vous semble correct, vous pouvez supprimer les paquets avec la commande **dpkg** :

```bash
deborphan| xargs dpkg -P
```

Il peut être nécessaire de le relancer plusieurs fois (en vérifiant ou non ce qu'il veut effacer à chaque fois)

### Check setuid

Le paquet **checksecurity** permet de scanner quotidiennement votre système afin d'y détecter un changement au niveau setuid. Il se configure pour l'envoi de mail par exemple, ici : **/etc/checksecurity.conf**

### Linux Malware detector (maldet)

Installer maldet :

```bash
wget http://www.rfxn.com/downloads/maldetect-current.tar.gz
tar xfz maldetect-current.tar.gz
cd maldetect-*
./install.sh
```

Pour mettre la base de signatures à jour :

```bash
maldet -u
```

Pour affiner vos réglages, avoir des informations par courriel, configurer la mise en quarantaine, le fichier de configuration se trouve dans **/usr/local/maldetect/conf.maldet**

Il est possible de rajouter la base de signatures de maldet à clamav, ce qui permet de scanner une seule fois avec les deux bases de signatures. Après avoir installé maldet, vous pouvez faire la manipulation suivante :

```bash
ln -s /usr/local/maldetect/sigs/rfxn.hdb /var/lib/clamav/rfxn.hdb
ln -s  /usr/local/maldetect/sigs/rfxn.ndb /var/lib/clamav/rfxn.ndb
/etc/init.d/clamd restart
```

Vous pouvez utiliser la commande suivante pour lancer une analyse, **--recursive** permet de descendre dans l'arborescence, **--infected** permet de n'afficher que les résultats positifs lors de l'analyse pour éviter une sortie particulièrement verbeuse, et --log permet de définir un fichier ou le résultat de l'analyse sera écrit :

clamscan --recursive --infected --log="/var/log/clamscan"

### Empêcher les utilisateurs de créer un cron

Si l'on veut qu'aucun utilisateur ne puisse créer une tache planifié, on le spécifie comme ceci.

```bash
echo ALL >>/etc/cron.deny
```

Si l'on veut autoriser un utilisateur, ce sera comme ça :

```bash
echo manchot >>/etc/cron.allow
```

## Combinaison ctrl-alt-suppr

La combinaison de touches ctrl-alt-suppr sur un linux a pour effet de redémarrer le système. Pour éviter toute attaque de ce type, le mieux étant de désactiver l'action de cette combinaison.

```bash
ln -s /dev/null /etc/systemd/system/ctrl-alt-del.target
```

## Magic SysRq key

Les Magic SysRq key sont des combinaisons de touches qui permettent aux utilisateurs connectés sur une console d'effectuer des actions *bas-niveau*. On commence toujours par la même combinaison, on maintient les 2 touches suivantes alt+Impr Ecran/Syst puis une autre touche d'action.
Exemple:

* **alt+Impr Ecran/Syst+o** arrête le système.
* **alt+Impr Ecran/Syst+reisub** redémarre proprement le système. (Reboot Even If System Utterly Broken)

Le mieux étant de désactiver cette possibilité, certes fort pratique, mais dangereuse, peut causer un remontage en read only, un reboot, un kernel panic.
Pour cela, il faut éditer /etc/sysctl.conf et ajouter la ligne suivante :

```bash
# Disables the magic SysRq key
kernel.sysrq = 0
````
