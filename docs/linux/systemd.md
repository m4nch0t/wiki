# Présentation de Systemd

Selon **Red Hat**:

* Contrôle des *units* et non plus des daemons
* Gère les dépendances entre les *units*
* Suis les processus en fonction des informations de service
* Les services appartiennent à un **cgroup**
* On peut simplement configurer des *SLAs* pour les CPU, RAM et IO
* Kill proprement les daemons
* Réduit le temps de chargement

## Targets

### Présentation

Les *targets* sont simplement des groupes d'*units*  
Les *runlevels* sont représentés comme des *targets units*  
On se retrouve maintenant avec ces équivalences:

* multi-user.target vs. runlevel3
* graphical.target vs. runlevel5

### Utilisation

Déterminer la *target* par défaut:

```shell
systemctl get-default
```

Définir la *target* par défaut:

```shell
systemctl set-default {target}
```

Changer la target en cours:

```shell
systemctl isolate {target}
```

## Units

### Modification

Tout d'abord voyons à quoi ressemble le fichier de configuration :

```shell
systemctl cat chronyd

# /usr/lib/systemd/system/chronyd.service
[Unit]
Description=NTP client/server
Documentation=man:chronyd(8) man:chrony.conf(5)
After=ntpdate.service sntp.service ntpd.service
Conflicts=ntpd.service systemd-timesyncd.service
ConditionCapability=CAP_SYS_TIME

[Service]
Type=forking
PIDFile=/var/run/chronyd.pid
EnvironmentFile=-/etc/sysconfig/chronyd
ExecStart=/usr/sbin/chronyd $OPTIONS
ExecStartPost=/usr/libexec/chrony-helper update-daemon
PrivateTmp=yes
ProtectHome=yes
ProtectSystem=full

[Install]
WantedBy=multi-user.target
```

Maintenant que nous en savons un peu plus sur sa configuration actuelle, nous pouvons voir toutes les options disponibles:

```shell
systemctl show --all chronyd

Type=forking
Restart=no
PIDFile=/var/run/chronyd.pid
NotifyAccess=none
RestartUSec=100ms
TimeoutStartUSec=1min 30s
TimeoutStopUSec=1min 30s
WatchdogUSec=0
WatchdogTimestamp=Mon 2019-07-29 08:17:45 CEST
WatchdogTimestampMonotonic=6178810
StartLimitInterval=10000000
StartLimitBurst=5
StartLimitAction=none
RebootArgument=
FailureAction=none
PermissionsStartOnly=no
RootDirectoryStartOnly=no
RemainAfterExit=no
GuessMainPID=yes
MainPID=813
ControlPID=0
BusName=
FileDescriptorStoreMax=0
StatusText=
StatusErrno=0
Result=success
ExecMainStartTimestamp=Mon 2019-07-29 08:17:45 CEST
ExecMainStartTimestampMonotonic=6178787
ExecMainExitTimestamp=
ExecMainExitTimestampMonotonic=0
ExecMainPID=813
ExecMainCode=0
ExecMainStatus=0
ExecStart={ path=/usr/sbin/chronyd ; argv[]=/usr/sbin/chronyd $OPTIONS ; ignore_errors=no ; start_time=[Mon 2019-07-29 08:17:45 CEST] ; stop_time=[Mon 2019-07-29 08:17:45 CEST] ; pid=799 ; code=exited ; status=0 }
ExecStartPost={ path=/usr/libexec/chrony-helper ; argv[]=/usr/libexec/chrony-helper update-daemon ; ignore_errors=no ; start_time=[Mon 2019-07-29 08:17:45 CEST] ; stop_time=[Mon 2019-07-29 08:17:45 CEST] ; pid=817 ; code=exited
Slice=system.slice
ControlGroup=/system.slice/chronyd.service
MemoryCurrent=18446744073709551615
TasksCurrent=18446744073709551615
Delegate=no
CPUAccounting=no
CPUShares=18446744073709551615
StartupCPUShares=18446744073709551615
CPUQuotaPerSecUSec=infinity
....
```

Comme vous le voyez énormément d'options sont disponibles, la sortie est tronquée par soucis de lisibilité.  
Si vous commencer à être à l'aise avec ces options, vous pouvez vérifier l'état d'une option pour un service :

```shell
systemctl show -p CPUShares chrony

CPUShares=18446744073709551615
```

Maintenant que nous y voyons un peu plus clair, leur modification ne doit pas passer par l'édition du fichier du service directement (ex: /usr/lib/systemd/system/chronyd.service).
En effet, nous devons utiliser la fonctionnalité des *Drop-In*, ici deux solutions:

* Manuellement :
  * Créer un répertoire de configuration alternative, (ex: /etc/systemd/system/chronyd.service.d)
  * Créer un fichier de configuration, (ex: /etc/systemd/system/httpd.service.d/50-chronyd.conf
  [Service]  
  Restart=always CPUAffinity=0 1 2 3)
  * Recharger l'*units* avec la nouvelle configuration
  systemctl daemon-reload
* Par l'outils de **systemctl**:
  * systemctl edit
  [Service]  
  Restart=always CPUAffinity=0 1 2 3)
  * La modification est pris en compte dès l'écriture du *Drop-in*

Maintenant nous pouvons voir que notre *units* a été modifiée et qu'un *Drop-in* est mainenant visible:

```shell
systemctl status chronyd

● chronyd.service - NTP client/server
   Loaded: loaded (/usr/lib/systemd/system/chronyd.service; enabled; vendor preset: enabled)
  Drop-In: /etc/systemd/system/chronyd.service.d
           └─override.conf
   Active: active (running) since Mon 2019-07-29 08:17:45 CEST; 22min ago
     Docs: man:chronyd(8)
           man:chrony.conf(5)
 Main PID: 813 (chronyd)
   CGroup: /system.slice/chronyd.service
           └─813 /usr/sbin/chronyd
```

### La configuration des options liées à la sécurité

La liste ci-dessous provient de Red Hat, nous ne nous attarderons pas sur leur utilisation car cela sera trop spécifique à votre environnement.

* **PrivateTmp=**  
  File system namespace with /tmp & /var/tmp  
  (Files are under /tmp/systemd-private-*-[unit]-*/tmp)
* **PrivateNetwork=**  
  Creates a network namespace with a single loopback device
* **JoinsNamespaceOf=**  
  Enables multiple units to share PrivateTmp=
* **SELinuxContext=**  
  Specify an SELinux security context for the process/service
* **ProtectSystem=**  
  If enabled, /usr & /boot directories are mounted read- only  
  If “full”, /etc is also read-only
* **ProtectHome=**  
  If enabled, /home, /root, /run/user will appear empty  
  Alternatively can set to “read-only”
* **PrivateDevices=**  
  If enabled, creates a private /dev namespace.  
  Includes pseudo devices like /dev/null, /dev/zero, etc
* **ReadWriteDirectories=, ReadOnlyDirectories=, InaccessibleDirectories=**  
  Configure file system namespaces
* **NoNewPrivileges=**  
  Ensure a process & children cannot elevate privileges

## Usage basique

### Localisation

Plusieurs localisations en fonction de notre besoin:

* Maintenance: **/usr/lib/systemd/system**
* Administrateurs: **/etc/systemd/system**, les fichiers présents dans ce path surchargeront les autres
* Non-persistent, runtime: **/run/systemd/system**

Commande sympathique pour identifier et comparer le fichiers d'override

```bash
systemd-delta
```

### Utilisation basique

Vous pouvez arrêter/démarrer/redémarrer les services comme ceci:

```bash
systemctl {start,stop,restart,reload} httpd.service
```

Vous pouvez spécifier plusieurs services à la fois:

```bash
systemctl restart httpd mariadb
```

Si le type d'units n'est pas spécifié, systemd va utiliser par défaut *.service*, **systemctl restart httpd** sera équivalent à **systemctl restart httpd.service**.

Activer ou désactiver un service:

```bash
systemctl {enable, disable} httpd
```

Activer et démarrer un service en une seule commande:

```bash
systemctl enable --now httpd mariadb
```

Voir l'état de son service:

```bash
systemctl status httpd
### ou pour être plus verbeux
systemctl status httpd -l
```

Lister les services démarrés

```bash
systemctl -t service
```

Lister les services installés

```bash
systemctl list-unit-files -t service
```

Lister les services dans l'état failed

```bash
systemctl --failed
```

## Usage plus avancé

### Vous pouvez redémarrer des services à distance

```bash
systemctl -H [hostname] restart httpd
```

ToDo: vérifier les prérequis

### Analyser le lancement de votre système

Voir quel service est à *blamer*, c'est à dire celui qui prend le plus de temps:

```bash
[root@rhel7 ~]# systemd-analyze blame

          3.014s kdump.service
          2.306s tuned.service
          2.261s NetworkManager-wait-online.service
          1.220s postfix.service
          1.196s dev-mapper-rhel\x2droot.device
          1.066s lvm2-monitor.service
           583ms firewalld.service
           350ms network.service
           330ms polkit.service
           301ms httpd.service
           249ms chronyd.service
           223ms boot.mount
           212ms lvm2-pvscan@8:2.service
           207ms systemd-user-sessions.service
           198ms systemd-vconsole-setup.service
           197ms rhel-dmesg.service
           189ms rhnsd.service
           184ms auditd.service
           176ms sshd.service
           145ms rhel-import-state.service
           126ms systemd-udev-trigger.service
           113ms systemd-logind.service
           100ms rsyslog.service
            86ms rhel-readonly.service
            84ms systemd-tmpfiles-setup-dev.service
            81ms NetworkManager.service
            69ms systemd-journald.service
            57ms rhel-domainname.service
            57ms systemd-udevd.service
            55ms rhsmcertd.service
            50ms plymouth-read-write.service
            46ms dev-mqueue.mount
            45ms dev-hugepages.mount
            44ms kmod-static-nodes.service
            44ms dev-mapper-rhel\x2dswap.swap
            41ms plymouth-start.service
            36ms plymouth-quit.service
            35ms systemd-tmpfiles-clean.service
            32ms systemd-sysctl.service
            31ms plymouth-quit-wait.service
            30ms systemd-fsck-root.service
            29ms systemd-remount-fs.service
            27ms systemd-journal-flush.service
            25ms systemd-update-utmp-runlevel.service
            24ms systemd-tmpfiles-setup.service
            23ms sys-kernel-debug.mount
            18ms systemd-rfkill@rfkill0.service
            13ms systemd-random-seed.service
            10ms systemd-update-utmp.service
             7ms sys-kernel-config.mount
```

On peut voir toute la chaîne de démarrage, l'enchainment entre les services et les temps.

```bash
[root@rhel7 ~]# systemd-analyze  critical-chain

The time after the unit is active or started is printed after the "@" character.
The time the unit takes to start is printed after the "+" character.

multi-user.target @7.933s
└─tuned.service @5.626s +2.306s
  └─network.target @5.615s
    └─network.service @5.264s +350ms
      └─NetworkManager-wait-online.service @2.997s +2.261s
        └─NetworkManager.service @2.915s +81ms
          └─network-pre.target @2.913s
            └─firewalld.service @2.328s +583ms
              └─polkit.service @1.988s +330ms
                └─basic.target @1.979s
                  └─sockets.target @1.978s
                    └─dbus.socket @1.978s
                      └─sysinit.target @1.973s
                        └─systemd-update-utmp.service @1.961s +10ms
                          └─auditd.service @1.774s +184ms
                            └─systemd-tmpfiles-setup.service @1.748s +24ms
                              └─rhel-import-state.service @1.601s +145ms
                                └─local-fs.target @1.598s
                                  └─boot.mount @1.373s +223ms
                                    └─local-fs-pre.target @1.372s
                                      └─lvm2-monitor.service @305ms +1.066s
                                        └─lvm2-lvmetad.service @370ms
                                          └─lvm2-lvmetad.socket @292ms
                                            └─-.slice
```

Vous pouvez aussi générer un fichier SVG à ouvrir avec votre navigateur par exemple ou envoyer ver un dashboard.

```bash
systemd-analyze plot > test.svg
```

Voici un extrait. ![systemdanalyze](img/systemd-analyze-plop.jpg)

### Control Groups

Depuis la renaissance des containers, une fonctionnalité du noyau Linux revient sur le devant de la scène, les *cgroups*. Ici nous allons les aborder dans le cadre de la gestion des ressources. En effet nous allons pouvoir mettre des quotas, limitations et priorités.

#### Slices, Scopes, Services

* Slice: Type d'*unit* pour créer la hiérarchie du *cgroup*
* Scope: Un groupe de processus enfants
* Service: Un processus ou un group de processus contrôlés par systemd

Par défaut, les slices, services et scopes possèdent la même priorité (share), la valeur est de 1024.

NB: on va retrouver différents types de slice:

* **user slice**, qui va contenir bash, sshd user, etc
* **system slice**, qui va contenir les services tels que sshd, apache, etc
* **machine slice**, qui va contenir systemd, qemu, etc

Tous avec la même valeur de *shares*.

On peut voir tous ce petit monde avec la commande suivante :

```shell
systemd-cgls

├─1 /usr/lib/systemd/systemd --switched-root --system --deserialize 22
├─user.slice
│ └─user-0.slice
│   ├─session-2.scope
│   │ ├─1839 sshd: root@pts/0
│   │ ├─1843 -bash
│   │ ├─2397 systemd-cgls
│   │ └─2398 systemd-cgls
│   └─session-1.scope
│     ├─ 814 login -- root
│     └─1817 -bash
└─system.slice
  ├─rhsmcertd.service
  │ └─1295 /usr/bin/rhsmcertd
  ├─rhnsd.service
  │ └─1311 rhnsd
  ├─rsyslog.service
  │ └─1290 /usr/sbin/rsyslogd -n
  ├─sshd.service
  │ └─1289 /usr/sbin/sshd -D
  ├─postfix.service
  │ ├─1591 /usr/libexec/postfix/master -w
  │ ├─1597 qmgr -l -t unix -u
  │ └─2393 pickup -l -t unix -u
  ```

Et un équivalent de la commande **top** pour ces *cgroups*:

```shell
systemd-cgtop
Path                                Tasks   %CPU  Memory  Input/s   Output/s

/                                     164   0.8     204.5M     -        -
/system.slice/NetworkManager.service  2      -        -        -        -
/system.slice/auditd.service          1      -        -        -        -
/system.slice/chronyd.service         1      -        -        -        -
```

C'est bien mignon, mais si je veux influer sur ces process?!

Comme vu précédement, on peut jouer sur les *Drop-in*, avec commande vous allez changer les *shares* du processus, puis créer automagiquement un fichier de *Drop-in*:

```shell
systemctl set-property chronyd CPUShares=2048
```

Pour changer à la volé sans nécessairement créer un *Drop-in*:

```shell
systemctl set-property --runtime httpd CPUShares=2048
```

Mais vous pouvez aussi jouer sur bien d'autres paramètres :

* **CPUAccounting=1** pour activer la gestion des quotas
* **StartupCPUShares=** s'applique uniquement au démarrage du système, pour prioriser un processus
* **CPUQuota=** pourcentage maximum d'un CPU, une valeur du type *CPUQuota=200%* permettra d'utiliser l'équivalent de la totalité des cycles processeurs de 2 CPU.
* **MemoryAccounting=1** pour activer la gestion des quotas
* **MemoryLimit=1G** le processus ne pourra par exemple pas utiliser plus d'1 Go de mémoire vive.
* **BlockIOAccounting=1** pour activer la gestion des quotas
* **BlockIOWeight=**
  * permet d'assigner un poid aux IO d'un service (comme les CPU shares)
  * la valeur par défaut: 1000
  * doit être compris entre 10 et 1000
* **BlockIODeviceWeight=** peut etre définit par *device* (/dev) ou point de montage
* **BlockIOReadBandwidth=** spécifie la bande passante maximale qui peut être allouée en lecture
* **BlockIOWriteBandwidth=** spécifie la bande passante maximale qui peut être allouée en écriture
  * **BlockIOWriteBandwith=/var/log 5M** en exemple ici, on limite la bande passante à 5M du service sur /var/log
