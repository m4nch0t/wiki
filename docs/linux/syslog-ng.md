# SYSLOG-NG

## Envoie des logs chiffrés en TLS

Le cheminement est assez simple, générer vos certificats et modifier la configuration syslog-ng pour la prise en compte.  
Je vous conseille avant tout de tester un envoi de log sans chiffrement.

```shell
mkdir /etc/syslog-ng/cert.d/

openssl x509 -noout -hash -in /etc/syslog-ng/cert.d/ca.crt
aa19aea1

ln -s /etc/syslog-ng/cert.d/ca.crt /etc/syslog-ng/cert.d/aa19aea1.0

cd /etc/syslog-ng/cert.d/
vi /etc/syslog-ng/syslog-ng.conf

    destination d_net { tcp("log.vmanchot.net" port(514) tls(cert_file("/etc/syslog-ng/cert.d/ca.crt") ca_dir("/etc/syslog-ng/cert.d")) log_fifo_size(1000)); };
    # All messages send to a remote site
    #
    log { source(s_src); destination(d_net); };
```

Maintenant il faut redémarrer le service, avec au une commande au choix:

```bash
killall -HUP syslog-ng
/etc/init.d/syslog-ng restart
```

### Troubleshooting

Si vous avez cette erreur, c'est que vous êtes sûrement dans un container openvz

```bash
syslog-ng: Error setting capabilities, capability management disabled; error='Operation not permitted'
```

rajouter la ligne suivant dans /etc/default/syslog-ng:

```bash
SYSLOGNG_OPTS="--no-caps"
```
