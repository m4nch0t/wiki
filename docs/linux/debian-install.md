# Installation Debian 9 aka Stretch

Le but de cette documentation est de vous montrer les bases d'une installation sécurisée d'un serveur Debian et de vous donner les pistes de réflexions pour faire mieux.

ous allons parler de l'installation. Ce qui suit est à adapter en fonction de la situation du serveur : physique, virtuel, cloud public, etc...

Je ne dis pas que c'est comme cela que ça doit être fait, mais c'est comme cela que je vois les choses et comme toute information de sécurité, elle doit être adaptée au contexte.
Premier choix important, si vous voulez avoir les mises à jour de sécurité, restez sur la branche stable de Debian.

## L'installation du système

### La table de partition

Il en existe deux : MBR et GPT :

* **Intel (Master_boot_record)** : c'est le mode de partitionnement historique des ordinateurs, celui qui est le plus répandu. Dans ce modèle, une zone de 512 octets appelée le Master Boot Record (MBR) est réservée en début de disque pour contenir l'information relative à un maximum de quatre partitions. Une table de partitions de type MBR ne peut pas prendre en charge un disque d'une taille supérieure à 2.2 To
* **GPT (GUID Partition Table)** : ce mode de partitionnement est utilisé dans les ordinateurs récents (>2010) et les Mac (>2006). Ce modèle permet de créer jusqu'à 128 partitions par disque. De plus, il prend en charge les disques d'une taille supérieure à 2.2 To. Dans un ordinateur ayant un micrologiciel UEFI, ce schéma de table de partitions est requis pour charger un système d'exploitation.

### Le partitionnement

Il doit être adapté à besoin d'hébergement. Dogmatiquement je préfère suivre au maximum le standard qui définit l'arborescence des systèmes linux (Filesystem Hierarchy Standard).
Petit rappel :

* **/** la racine. Il contient des fichiers de configuration spécifiques au système. Il n'a pas besoins de beaucoup d'espace.
* **/bin** Il contient les binaires nécessaires au commandes utilisables par root, les utilisateurs ou des scripts.
* **/boot** Il contient les fichiers nécessaires au démarrage du système, le boot loader.
* **/dev** Il contient les fichiers des composants (devices)
* **/etc** Il contient des fichiers de configuration spécifiques au système. Pas de binaires à l'intérieur!
* **/lib** Il contient les librairies partagées ainsi que les modules du noyau.
* **/media** et **/mnt** Ce sont deux point de montage temporaire. Le premier est pour les médias amovibles et l'autre pour des systèmes de fichiers temporaires, ex d'un partage nfs.
* **/opt** Il doit contenir les logiciels additionnels, ex /opt/nagios.
* **/srv** Il doit contenir les données des services fournies par le système.
* **/tmp** Il contient les fichiers temporaires.
* **/usr** Il contient les données spécifiques aux utilisateurs (binaires, librairies, documentation, headers etc...)
* **/var** Il contient les données variables.

Des points de montage spécifiques doivent à mon avis être pris en compte :

* **/home** Il contient les données des utilisateurs du système.
* **/root** Il contient les données du super utilisateur du système.

Le but est ici donc d'augmenter la sécurité de l'ensemble de notre système via le partitionnement.
Dans un premier temps je veux réduire le risque de déni de service par le simple remplissage d'une partition, le plus gros risque serait la racine (/). Donc ce qui est spécifique aux utilisateurs, aux logiciels supplémentaires ou les données statiques je vais le séparer:

* **/home**
* **/opt**
* **/tmp**
* **/var**
* **/var/log**
* **/var/tmp**

C'est déjà le minimum. Ensuite une partition qui peut se remplir extrêmement vite est le /var et surtout /var/log. Il est dommage de voir son serveur hors d'usage à cause de la place occupé parle cache apt (/var/cache/apt/archives), ou les logs d'accès de son serveur web (/var/log/apache2/access.log) Je vais donc les séparer de la racine.

Je préfère séparer **/opt** et **/usr/local** et dans le cas d'un serveur mail, il est bon de séparer aussi **/var/spool** et/ou **/var/mail/spool**.

Bien sûr il est contraignant de gérer un partitionnement aussi drastique, surtout lorsque l'on y affecte trop peu d'espace, mais pour se simplifier la vie il ne faut pas hésiter à utiliser le lvm et/ou zfs.

### Le choix du système de fichiers

Alors là tout dépend du matériel, du besoin etc. Le mieux est d'effectuer des benchmarks qui correspondent à votre besoin (avec iometer par exemple). Mais dans tous les cas, un système de fichier journalisé est pour moi obligatoire. Il permet de pouvoir rejouer les modifications effectuer sur vos données et donc de limiter la perte de ces dernière lors d'une coupure brutale du serveur.
Les seuls cas où l'utilisation d'un système de fichier journalisé n'est pas absolument nécessaire sont :

* /tmp qui va être de toutes façons vidé au démarrage.
* /boot qui est monté en read-only par défaut.

Quoi qu'il en soit, j'étais partisans du ReiserFS avant qu'il ne soit abandonné par la force des choses. Après plusieurs jours de benchmark j'ai préféré XFS.
Libre à vous de prendre ext4,xfs,jfs etc. Les nouvelles star du moment semblent être btrfs et zfs pour leur implémentation du raid logiciels, snapshot et autres. Testez les, ça vaut le détour.

### Choix des logiciels

Un des prérequis à une bonne installation sécurisée est pour moi la simplicité. J'ai pris l'habitude de n'installer mes système uniquement via le cd netinstall avec le minimum de croix dans le choix des paquets (screenshot).
Ce qui ne nous sert pas ne dois être installé, car il pourrait être un vecteur d'attaque supplémentaire. Donc on choisira une image noyau ciblé.

### Mots de passes masqués

Lors de la questions "devons nous activer les mots de passes masqués" répondez oui pour que les mots de passes soient stockés dans le fichier /etc/shadow. Par défaut en SHA-512 sous Wheezy (/etc/login.defs : ENCRYPT_METHOD SHA512). Pour le vérifier :

```bash
awk -F$ '($2 == "6"){print $1 "SHA-512"}' /etc/shadow
```

### Exemple d'installation

Je vais prendre l'exemple ici d'un VPS Cloud Public loué chez OVH. Je n'ai rien à redire quant à la qualité de leurs infrasctures, mais une Debian installé par quelqu'un d'autre ne m'inspire pas confiance. Donc il faut redémarrer en mode rescue et réinstaller avec la méthode du bootstrap.

Premièrement on sauvegarde la configuration de notre VPS qui vous semble nécessaire (Réseau, DNS, etc...)

On spécifie que l'on veut utiliser le dépot Debian Officiel :

```bash
echo "deb http://ftp.fr.debian.org/debian/ stretch main" >/etc/apt/sources.list
```

On met à jour et on installe les paquets nécessaires :"

```bash
apt-get update
apt-get install gdisk vim binutils xfsprogs dosfstools lvm2 parted
```

La liste des paquet installé :

* gisk : permet le partitionnement en GPT
* vim : parce que je préfère :)
* binutils : contient la commande ar nécessaire à debootstrap
* xfsprogs : pour le formatage en XFS
* dosfstools : pour le formatage en FAT
* lvm2 : pour gérer nos volumes LVM
* parted : nécessaire pour avoir la commande partprobe

J'ai choisi d'utiliser une table de partition GPT, mais vous pouvez très bien rester en MBR.

En mode rescue, je me retrouve avec un disque /dev/sda qui contient le système de secours et un disque /dev/sdb qui est le disque de mon vps. Je démonte donc les points de montage qui ont un rapport avec ce disque.

```bash
umount /mnt/sdb1
```

Un exemple de partitionnement, avec  :
une partition de démarrage
une partition pour le bios efi
une partition pour
une partition pour lvm qui contiendra

/
/home
/opt
/tmp
/usr
/var
/var/log
swap

Maintenant que l'on a choisi, on va créer la table de partition:

```bash
gdisk /dev/sdb


Partition table scan:
  MBR: MBR only
  BSD: not present
  APM: not present
  GPT: not present


***************************************************************
Found invalid GPT and valid MBR; converting MBR to GPT format
in memory. THIS OPERATION IS POTENTIALLY DESTRUCTIVE! Exit by
typing q if you don t want to convert your MBR partitions
to GPT format!
***************************************************************
Command (? for help): o
This option deletes all partitions and creates a new protective MBR.
Proceed? (Y/N): y

Command (? for help): n
Partition number (1-128, default 1):
First sector (34-41943006, default = 2048) or {+-}size{KMGTP}:
Last sector (2048-41943006, default = 41943006) or {+-}size{KMGTP}: +1M
Current type is Linux filesystem
Hex code or GUID (L to show codes, Enter = 8300): ef02
Changed type of partition to BIOS boot partition

Command (? for help): n
Partition number (2-128, default 2):
First sector (34-41943006, default = 4096) or {+-}size{KMGTP}:
Last sector (4096-41943006, default = 41943006) or {+-}size{KMGTP}: +1M
Current type is Linux filesystem
Hex code or GUID (L to show codes, Enter = 8300): ef00
Changed type of partition to EFI System

Command (? for help): n
Partition number (3-128, default 3):
First sector (34-41943006, default = 6144) or {+-}size{KMGTP}:
Last sector (6144-41943006, default = 41943006) or {+-}size{KMGTP}: +100M
Current type is Linux filesystem
Hex code or GUID (L to show codes, Enter = 8300): 8300
Changed type of partition to Linux filesystem

Command (? for help): n
Partition number (4-128, default 4):
First sector (34-41943006, default = 210944) or {+-}size{KMGTP}:
Last sector (210944-41943006, default = 41943006) or {+-}size{KMGTP}:
Current type is Linux filesystem
Hex code or GUID (L to show codes, Enter = 8300): 8e00
Changed type of partition to Linux LVM

Command (? for help): p
Disk /dev/sdb: 41943040 sectors, 20.0 GiB
Logical sector size: 512 bytes
Disk identifier (GUID): 030CA1DD-6B1B-47B7-8DA7-60DA8878DAAD
Partition table holds up to 128 entries
First usable sector is 34, last usable sector is 41943006
Partitions will be aligned on 2048-sector boundaries
Total free space is 2014 sectors (1007.0 KiB)

Number  Start (sector)    End (sector)  Size       Code  Name
   1            2048            4095   1024.0 KiB  EF02  BIOS boot partition
   2            4096            6143   1024.0 KiB  EF00  EFI System
   3            6144          210943   100.0 MiB   8300  Linux filesystem
   4          210944        41943006   19.9 GiB    8E00  Linux LVM

Command (? for help): w

Final checks complete. About to write GPT data. THIS WILL OVERWRITE EXISTING
PARTITIONS!!

Do you want to proceed? (Y/N): y
OK; writing new GUID partition table (GPT) to /dev/sdb.
Warning: The kernel is still using the old partition table.
The new table will be used at the next reboot or after you
run partprobe(8) or kpartx(8)
The operation has completed successfully.
```

Il arrive que le système ne voit pas les nouvelles partitions, pour cela il suffit de lancer la commande suivante :

```bash
partprobe
```

Maintenant on formate en fat pour la partition EFI à monter sur /boot/efi

```bash
mkdosfs /dev/sdb2
mkfs.fat 4.1 (2017-01-24)
```

On formate le /boot en ext3 (ou ext2 si l'on ne veut pas de système de fichier journalisé)

```bash
mkfs.ext3 /dev/sdb3
```

Création du physical volume qui est la base de notre LVM

```bash
pvcreate  /dev/sdb4
```

Le Volume Group nomé arbitrairement vg0, contenu dans le Physical Volume précédemment créé

```bash
vgcreate vg0 /dev/sdb4
```

Et il contiendra les Logicals Volumes suivant :

```bash
lvcreate -L 512M -n home vg0
lvcreate -L 256M -n opt vg0
lvcreate -L 1024M -n root vg0
lvcreate -L 512M -n tmp vg0
lvcreate -L 1536M -n usr vg0
lvcreate -L 512M -n var vg0
lvcreate -L 1024M -n log vg0
lvcreate -L 2048M -n swap vg0
```

On formate tout ce petit monde en XFS

```bash
mkfs.xfs -f /dev/mapper/vg0-home
mkfs.xfs -f /dev/mapper/vg0-log
mkfs.xfs -f /dev/mapper/vg0-opt
mkfs.xfs -f /dev/mapper/vg0-root
mkfs.xfs -f /dev/mapper/vg0-tmp
mkfs.xfs -f /dev/mapper/vg0-usr
mkfs.xfs -f /dev/mapper/vg0-var
```

OU si l'on est fainéant :

```bash
for i in `ls /dev/mapper/vg0-* |grep -v swap`; do mkfs.xfs -f $i; done
```

On créer et monte la partition swap

```bash
mkswap /dev/mapper/vg0-swap
sync
swapon /dev/mapper/vg0-swap
```

Maintenant on va créer notre arborescence et y monter nos partitions précedements créées.

```bash
mkdir /newroot/
mount /dev/mapper/vg0-root /newroot/
mkdir /newroot/var /newroot/opt /newroot/usr  /newroot/home /newroot/tmp /newroot/boot
cd /newroot/
mount /dev/sdb3 boot/
mkdir boot/efi/
mount /dev/sdb2 boot/efi/
mount /dev/mapper/vg0-var var/
mkdir /newroot/var/log
mount /dev/mapper/vg0-log var/log
mount /dev/mapper/vg0-home home
mount /dev/mapper/vg0-opt opt
mount /dev/mapper/vg0-tmp tmp
mount /dev/mapper/vg0-usr usr
```

Maintenant que tout le monde est correctement monté au bon endroit, il va nous falloir y installer notre Debian. On va tout d'abord récupérer la dernière version de debootstrap

```bash
cd /tmp/
wget http://ftp.debian.org/debian/pool/main/d/debootstrap/debootstrap_1.0.111_all.deb
```

On extrait le paquet et son contenu :

```bash
ar -x debootstrap_1.0.111_all.deb
cd /
zcat /tmp/data.tar.gz |tar xv
```

On lance la commande debootstrap fraîchement installée pour récupérer les fichiers de notre système de base à partir du miroir officiel:

```bash
/usr/sbin/debootstrap --arch amd64 stretch /newroot/ http://deb.debian.org/debian
```

Maintenant on prépare le chroot :

```bash
mount -o bind /dev /newroot/dev
mount -o bind /sys /newroot/sys
mount -t proc /proc /newroot/proc
```

Petite astuce pour savoir si l'on est dans un environnement chrooté (oui je sais ce verbe n'existe pas), l'inode de / sera toujours 2 si il est effectivement la racine d'un système de fichiers.

```bash
ls -di /
2 /
```

On effectue notre chroot et on revérifie

```bash
chroot /newroot /bin/bash
ls -di /
96 /
```

Première étape, on change son mot de passe root, ce serait dommage de se retrouver bloqué à la porte après tous ces efforts

```bash
passwd
```

On installe les paquets qui nous seront directement nécessaire au redémarrage.

```bash
apt-get install gdisk vim binutils xfsprogs dosfstools lvm2 ssh
```

Ensuite on va devoir créer notre fichier fstab. Pour cela, deux méthodes, soit à l'ancienne avec le chemin des devices, soit via les UUIDs. J'ai préféré cette dernière.

Tout d'abord, on identifie :

```bash
blkid
/dev/sdb1: UUID="838bb60b-f760-4fe8-a3b7-b2e214f16e83" TYPE="ext3" PARTLABEL="BIOS boot partition" PARTUUID="b8e281ac-74f4-48c8-9f90-bbdfc63747c1"
/dev/sdb2: SEC_TYPE="msdos" UUID="706D-9012" TYPE="vfat" PARTLABEL="EFI System" PARTUUID="2dda4f53-9d5f-41a5-86e2-db71bc9bf76a"
/dev/sdb3: UUID="2db5a7cd-5617-4a12-80b6-a155a1c29473" TYPE="ext3" PARTLABEL="Linux filesystem" PARTUUID="a4df25ed-07dc-40f6-aac9-855480b545b7"
/dev/sdb4: UUID="MuAbjB-WV9M-Gj5L-A3yH-dg7M-mC6m-UFQRVt" TYPE="LVM2_member" PARTLABEL="Linux LVM" PARTUUID="d6ce5ecf-5fa2-4cef-bb2d-a490429cfb02"
/dev/sda1: UUID="fb39de4d-eabd-4f0f-a9df-e5ed35d6528b" TYPE="ext4" PARTUUID="683d8876-01"
/dev/mapper/vg0-home: UUID="716e67ae-8121-4f03-a1ed-ad3fb24f64f7" TYPE="xfs"
/dev/mapper/vg0-opt: UUID="031280f9-bb83-4a40-aa78-8cfd108650c9" TYPE="xfs"
/dev/mapper/vg0-root: UUID="4c448bb2-9754-41a7-9ada-80e7db0636e2" TYPE="xfs"
/dev/mapper/vg0-tmp: UUID="eab79503-c8fa-4e41-8cb0-189097bdc9c0" TYPE="xfs"
/dev/mapper/vg0-usr: UUID="ba592437-caa9-46e9-9080-8401b9417d9e" TYPE="xfs"
/dev/mapper/vg0-var: UUID="c81978b9-784c-4be3-a0dd-38c86294d614" TYPE="xfs"
/dev/mapper/vg0-log: UUID="d8095ea6-38c4-40f6-8400-d5e5ebb342a9" TYPE="xfs"
/dev/mapper/vg0-swap: UUID="31d8978f-42bf-48a4-be41-c698a19d3a9c" TYPE="swap"
```

On modifier notre fichier **/etc/fstab** en fonction :

```bash
# /etc/fstab: static file system information.
#
# file system    mount point   type    options                  dump pass
#/dev/vdb3            /boot           ext3    defaults            0    2
UUID="2db5a7cd-5617-4a12-80b6-a155a1c29473" /boot/          ext3    defaults        0    2
#/dev/vdb2            /boot/efi       vfat    defaults     0    2
UUID="706D-9012"      /boot/efi       vfat    defaults     0    2
#/dev/mapper/vg0-root /               xfs     defaults        0       1
UUID="4c448bb2-9754-41a7-9ada-80e7db0636e2" /               xfs     defaults        0       1
#/dev/mapper/vg0-home /home           xfs     defaults        0       2
UUID="716e67ae-8121-4f03-a1ed-ad3fb24f64f7" /home           xfs     defaults        0       2
#/dev/mapper/vg0-opt /opt            xfs     defaults         0       2
UUID="031280f9-bb83-4a40-aa78-8cfd108650c9" /opt            xfs     defaults        0       2
#/dev/mapper/vg0-tmp /tmp            xfs     defaults         0       2
UUID="eab79503-c8fa-4e41-8cb0-189097bdc9c0" /tmp            xfs     defaults        0       2
/dev/mapper/vg0-usr /usr            xfs     defaults        0       2
#UUID="ba592437-caa9-46e9-9080-8401b9417d9e" /usr            xfs     defaults        0       2
#/dev/mapper/vg0-var /var            xfs     defaults         0       2
UUID="c81978b9-784c-4be3-a0dd-38c86294d614" /var            xfs     defaults        0       2
#/dev/mapper/vg0-log /var/log        xfs     defaults         0       2
UUID="d8095ea6-38c4-40f6-8400-d5e5ebb342a9" /var/log        xfs     defaults        0       2
#/dev/mapper/vg0-swap none           swap    sw              0       0
UUID="31d8978f-42bf-48a4-be41-c698a19d3a9c" none            swap    sw              0       0
```

Je suis tombé sur un bug assez agaçant, il n'est pas possible l'avoir le / et /usr  en UUID en LVM : <https://lists.debian.org/debian-kernel/2015/04/msg00005.html>

On paramètre le réseau avec la configuration récupérée précédemment dans **/etc/network/interfaces**:

```bash
# interfaces(5) file used by ifup(8) and ifdown(8)
# Include files from /etc/network/interfaces.d:
source-directory /etc/network/interfaces.d

# The loopback network interface
auto lo
iface lo inet loopback

auto ens3
iface ens3 inet static
    address 164.132.41.191
    netmask 255.255.255.255
    post-up ip route add 164.132.40.1 dev ens3
    post-up ip route add default via 164.132.40.1
    pre-down ip route del default via 164.132.40.1
    pre-down ip route del 164.132.40.1 dev ens3
    dns-nameserver 213.186.33.99
    dns-search ovh.net
```

Il se peut que votre interface réseau ne soit pas **ens3**, pour l'indetifier il suffit de taper la commande :

```bash
ip link show
```

Le DNS

```bash
cat /etc/resolv.conf

search ovh.net
nameserver 213.186.33.99
```

La déclaration des hostnames :

```bash
echo vps.vmanchot.net > /etc/hostname
vi /etc/hosts


127.0.0.1 localhost vps.vmanchot.net vps
::1 localhost ip6-localhost ip6-loopback
ff02::1 ip6-allnodes
ff02::2 ip6-allrouters
```

On choisit notre locale

```bash
apt-get install locales && dpkg-reconfigure locales
```

On se cale sur le bon fuseau horaire

```bash
dpkg-reconfigure tzdata
```

Et maintenant on installe notre noyau

```bash
apt-get install linux-image-amd64
```

Puis GRUB en sélectionnant bien le disque qui contient notre partition /boot

```bash
apt-get install grub-pc

    [ ] /dev/sda (2621 MB; ???)
    [*] /dev/sdb (21474 MB; ???)
    [ ] - /dev/sdb3 (104 MB; /boot)
    [ ] /dev/sdb4 (21366 MB; ???)  
    [ ] /dev/dm-2 (1073 MB; vg0-root)
```

On autorise le compte root à se connecter en ssh en positionnant le **PermitRootLogin** à **yes** dans le fichier **/etc/ssh/sshd_config**

On quitte le chroot :

```bash
exit
```

On reboot et on passe à l'étape suivante, la sécurisation de notre OS tout neuf.

## Troubleshooting LVM

Si quelque chose s'est mal passé, vous pouvez redémarrer en mode rescue, et modifier ce qui existe. Il vous suffit de réinstaller les paquets du début et de remonter les partitions en LVM:

* pvscan : scanner les partitions à la recherche des physical volumes
* pvs : lister les physical volumes
* vgscan : scanner les partitions à la recherche des volumes groups
* vgs : lister les volumes groups
* lvscan : scanner les partitions à la recherche des logicals volumes
* lvs : lister les logicals volumes

Pour remonter tous les logicals volumes trouvés :

```bash
vgchange -ay vg0
```
