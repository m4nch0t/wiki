# Grub

## Changer le splash screen de Grub sur Debian Squeeze

Tout d'abord, il nous faut récupérer le paquet grub2-splashimages:

```bash
apt install grub2-splashimages
```

Les images pour grub sont stockées dans le dossier </usr/share/images/desktop-base/>, d'ailleurs nous pouvons voir dans ce dossier la fameuse image desktop-grub.png qui est l'image du thème par défaut.

Donc sachant cela, la première étape est de mettre notre nouvelle image dans ce dossier.

## Modification de grub

Maintenant que nous avons une nouvelle image toute belle, il faut modifier grub pour qu'il la prenne en compte. Pour cela nous allons éditer le fichier **/etc/default/grub** en rajoutant cette ligne :

```bash
GRUB_BACKGROUND=/usr/share/images/desktop-base/ma-super-image.png
```

Une fois la ligne ajoutée, il faut recharger la configuration de grub :

```bash
update-grub
```

Si tout c'est bien passé il devrait répondre :

```bash
Generating grub.cfg ...
Found background: /usr/share/images/desktop-base/ma-super-image.png
Found background image: /usr/share/images/desktop-base/ma-super-image.png
Found linux image: /boot/vmlinuz-2.6.32-5-amd64
Found initrd image: /boot/initrd.img-2.6.32-5-amd64
done
```
