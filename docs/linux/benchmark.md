# Linux benchmarking

## Benchmark FileSystem Linux

Pour effectuer un benchmark rapide de votre système de fichier afin d’évaluer les optimisation que vous avez apportés, je vous propose un petit paquet fort sympathique : **bonnie++**
Commençons par un disque rajouté spécialement pour les tests sur du reiserfs qui devrait rendre nostalgique les plus anciens:

```bash
cfdisk /dev/sdb
mkfs.reiserfs /dev/sdb1
mount /dev/sdb1 /mnt/bench
apt-get install bonnie++
bonnie++ -u 0 -n 256 -s 1024 -d /mnt/bench
```

* -u permet de spécifier un uid pour lancer la commande.
* -n, est le nombre de fichiers à utiliser, à moins de 256, je n’avais pas tous les résultats, le calcul était trop rapide.
* -s est la taille à utiliser sur le disque
* -d sert à spécifier le répertoire à utiliser, ici un disque dur spécifique.
