# Gérer la swap sous Linux

Vous en avez marre que votre système swap pour rien? Voici des pistes pour remédier à votre problème:

## Identification

Pour savoir si votre système utilise son swap, vous pouvez exécuter les commandes suivantes :

```bash
free -m
swapon -s
```

Mais normalement vous devriez sentir votre système qui ralenti…La commande free vous indiquera la quantité de Ram utilisé, libre, caché, etc, et de même pour la swap. Le -m c’est pour l’avoir en Mo. La commande swapon vous indique les partitions de swap, leur utilisation et leurs priorités.

## Swappiness

La swappiness est la quantité de RAM utilisé à partir de laquelle le système va utiliser la swap. Si vous n’y avez jamais touché, le système va commencer à swaper à partir de 60% de ram utilisée.

```bash
cat /proc/sys/vm/swappiness
60
```

Pour modifier cette valeur, vous devez rajouter une ligne dans un fichier:

```bash
echo " vm.swappiness = 15" >> /etc/sysctl.conf
```

## Priorités sur la swap

Vous avez plusieurs disques dur? Dont un tout petit très rapide? Nikel, vous pouvez y mettre une partition swap dessus. Vous en avez plusieurs? Mettez plusieurs partitions swap sur plusieurs disques comme ci après. Pour voir les priorités actuelles :

```bash
swapon -s

Nom de fichier                Type        Taille    Utilisé    Priorité
/dev/dm-1                                  partition    2097148    0    -2
/dev/dm-2                                  partition    3905528    0    -1
```

Pour changer les priorités,éditez le fichier /etc/fstab comme suit :

```bash
vi /etc/fstab

/dev/mapper/rhel-root   /                       xfs     defaults        0 0
UUID=703f76ae-ec89-4213-9215-33c7f2bb060f /boot                   xfs     defaults        0 0
/dev/mapper/rhel-swap-1   swap                    swap    sw,pri=1        0 0
/dev/mapper/rhel-swap-2   swap                    swap    sw,pri=2        0 0
```

**ATTENTION** ! Plus le chiffre est élevé, plus la priorité est élevée.
