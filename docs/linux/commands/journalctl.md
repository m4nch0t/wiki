# Journalctl

Les données de journalisation sont collectées, stockées et analysées par le journal de **systemd**. **journalctl** est un utilitaire pour voir tous les événements collectés par **systemd-journald**.

## Voir les logs systemctl

```bash
journalctl
```

La sortie est une liste de tous les logs générés par le système. La structure est similaire à ce que l'on peut trouver dans le fichier */var/log/messages*

## Voir les logs en cours

Vous pouvez voir en temps réel les logs pris en compte par journald :

```bash
journalctl -f
```

## Voir les logs des démarrages

```bash
journalctl --list-boots
```

Vous pouvez spécifier l'identifiant du redémarrage que vous souhaitez analyser :

```bash
journalctl -b 0
```

## Voir les logs en fonction du temps

Voici quelques exemples d'utilisation :

```bash
journalctl --since today
journalctl --since "2020-01-01 12:00:00"
journalctl --since "2020-01-01 12:00:00" --until "2020-01-01 14:00:00"
journalctl --since yesterday --until now
```

## Voir les messages d'un service

Il suffit de passer l'option **-u** pour *unit*.

```bash
journalctl -u systemd-journald
journalctl -u sshd.service
journalctl /sbin/sshd
```

## Rendre plus verbeux les logs d'un service

Il suffit de passer l'option **-x** pour *explanation*.

```bash
journalctl -u sshd.service -x
```

## Voir les logs d'un PID

Il suffit de passer l'option **_PID**.

```bash
journalctl _PID=42
```

## Voir les logs d'un utilisateur

Il suffit de passer l'option **_UID**.

```bash
journalctl _UID=1042
```

## Voir les logs en fonction de leur priorité

Il suffit de passer l'option **-p** pour *priority*.

Les priorités sont les suivantes :

* emerg (0)
* alert (1)
* crit (2)
* err (3)
* warning (4)
* notice (5)
* info (6)
* debug (7)

```bash
journalctl -p 0
```

On peut aussi demander une fourchette (de..à):

```bash
journalctl -p 2..4
```

## Voir les logs kernel

```bash
journalctl -k
```

## Voir les logs en fonction du transporteur

* driver
* syslog
* journal
* stdout
* kernel

```bash
journalctl _TRANSPORT=syslog
```

## Gérer l'espace utilisé par journald

```bash
journalctl --disk-usage
```

Réduire la taille des journaux jusqu'à une taille définie:

```bash
journalctl --vacuum-size=100M
```

Les suffixes utilisables :

* K
* M
* G
* T

Réduire la taille des journaux jusqu'à une date définie:

```bash
journalctl -vacuum-time=1months
```

Les suffixes utilisables :

* s
* min
* h
* days
* months
* weeks
* years
