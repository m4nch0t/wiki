# Cheat Sheet commande kill

## SIGHUP

Certains Linux et Unix-like daemon utilisent SIGHUP comme signal pour se redémarrer. La raison la plus fréquente est la nécessité de relire les fichiers de configuration.
Dans ce cas nous pouvons utiliser ce type de commande:

    kill -HUP $(cat /var/run/service.pid)
    kill -HUP `cat /var/run/service.pid`

## Tuer un processus

Tuer un processus parent ne tue pas necessairement les processus enfants et tout processus à un parent.

Créons 2 processus:

                sleep 3600 &
                sleep 3601 &

Vérifions leur arborescence

                pstree -p  
                systemd(1)─┬─sshd(12526)───sshd(12540)┬─pstree(26250)  
                                                      │
                                                      ├─sleep(26236)  
                                                      │
                                                      └─sleep(26241)  

                ps j -A
                PPID   PID  PGID   SID TTY      TPGID STAT   UID   TIME COMMAND
                12560 12566 12560 12543 pts/0    26517 S        0   0:00 su -
                12566 12571 12571 12543 pts/0    26517 S        0   0:00 -su
                12571 26477 26477 12543 pts/0    26517 S        0   0:00 sleep 3600
                12571 26479 26479 12543 pts/0    26517 S        0   0:00 sleep 3601
                12571 26517 26517 12543 pts/0    26517 R+       0   0:00 ps j -A

Grâce à la commande *ps* nous pouvons voir le **PID** (id du processus), ainsi que le **PPID**, (id du processus parent.

Si nous tuons le processus parent, le processus enfant devient orphelin (*orphaned*)

Mais nous avons aussi d'autres liens:

* **PGID**: process group ID
* **SID**: session ID

Les *process group* sont observables sous bash par exemple, ils créent un process group pour chaque pipeline de commande. C'est une collection de processus qui peuvent être affectés durant la même session.
Une *session* est une collection de **process group**.

## Tuer tous les processus d'un process group

Nous utilisons le PGID pour envoyer un signal à tout le group:

                kill -SIGTERM -- -26479

On tulise un nombre négatif pour envoyer un sigterm à tout le groupe. Si nous avions utilisé un nombre positif, on aurait tué le processus avec cet ID.

## Tuer tous les processus d'une session

Pour voir les processus d'une session:

                pgrep -s 12543 -a
                12543 -bash
                26477 sleep 3600

Et les arréter:

                pkil -s 12543
