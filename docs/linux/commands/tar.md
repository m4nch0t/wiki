# Cheat Sheet commande tar

## Extraire

Extraire localement votre fichier:

    tar -xvzf backup.tar.gz

Extraire un fichier présent dans votre archive:

    tar -xvzf backup.tar.gz dump.sql

* -z : gzip compression
* -x : extraction
* -v : mode verbeux
* -f : fichier
* -t : lister les fichiers de l'archive
* -r : rajoute des fichiers à la fin de l'archive
* --delete : supprime un fichier de l'archive

## Compresser

Archive sans gzip

    tar -cvf backup.tar dump.sql my.cnf

Archive compressée avec gzip

    tar -cvzf backup.tar.gz dump.sql my.cnf

Rajout de fichier à l'archive

   tar -rf backup.tar dump_complete.sql

Suppression de fichier dans l'archive

   tar -f backup.tar --delete dump_complete.sql
