# Cheat Sheet commande IP

## Routing

Voir la table de routage:

    ip route show
    ip route list

Rajouter une route via une interface ou une gateway:

    ip route add 192.168.1.0/24 dev eth0
    ip route add 192.168.2.0/24 via 192.168.0.254 dev eth0

Supprimer une route

    ip route delete 192.168.1.0/24 dev eth0
    ip route delete 192.168.2.0/24 via 192.168.0.254 dev eth0

Attention, ces commandes permettent de modifier à chaud la configuration, celle-ci ne sera pas sauvée.


## Voir les statistiques de sa carte réseau

```shell
ip -s link show eth0
```