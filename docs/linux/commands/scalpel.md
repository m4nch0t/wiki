# Scalpel

Scalpel est un outil de forensic vous permettant de récupérer des données sur vos disques.

Dans un premier temps il est fortement conseillé de faire une image de votre disque avec la commande **dd**.

Il est particulièrement efficace si vous limitez la recherche à certain types de fichiers, pour y parvenir vous devez récupérer la signature du fichier.

Exemple pour un fichier pdf:

```bash
hexdump -C -n 32 document.pdf
00000000  25 50 44 46 2d 31 2e 35  0a 25 e4 f0 ed f8 0a 34  |%PDF-1.5.%.....4|
00000010  20 30 20 6f 62 6a 0a 3c  3c 2f 46 69 6c 74 65 72  | 0 obj.<</Filter|
```

Vous n'avez plus qu'à rajouter dans votre fichier de configuration le type de fichier à rechercher:

    pdf y 1000000:30000000 \x25\x50\x44\x46\x2d\x31
    
On va ainsi rechercher le type de fichier pdf faisant entre 1 et 30 Mo qui possède la signature précédement déterminée.
Le **y** va spécifier à scalpel sera sensible à la casse.

Et vous pouvez lancer la recherche:

   scalpel -c scalpel.conf -o output_folder /dev/sda
   scalpel -c scalpel.conf -o output_folder image_dd.img
