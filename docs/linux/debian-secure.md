# Sécurisation de Debian 9 aka Stretch

## Sécurité physique et bas niveau

Un petit rappel ne fait jamais de mal, la sécurité physique est importante, on peut utiliser tous les logiciels de sécurité que l'on veut, si l'on peut accéder à la machine physique, on a perdu. Que ce soit pour un DOS basique via un USB Killer ou un vol de données, il est très important de protéger vos machines.

### Physique

* fermer à clé la baie serveurs
* positionner le serveur dans un local fermé à clé/code avec climatisation, moyen anti-feu, alarme anti-intrusion, etc.
* restreindre le nombre de personnes pouvant intervenir sur ces moyens, une clé en libre accès ne sert à rien. Je ne pense pas que vous partez en laissant les clés de votre appartement à l'entrée de votre bâtiment. C'est pratique, mais totalement inefficace.
* dans le cas d'un cloud provider, s'il est certifié PCI-DSS ou ISO 27002, c'est un plus non négligeable.

### BIOS

Je considère cela comme une sécurité physique, même si vous êtes dans une machine virtuelle, sur le BIOS vous devez :

* mettre un mot de passe
* désactiver le démarrage sur les supports amovibles (cd/dvd, usb) et par le réseau
* il est aussi possible dans certains cas de mettre un mot de passe au démarrage du serveur, mais cela peut devenir contraignant.

Pour ceux qui ne le savent pas il existe aussi un BIOS dans une machine virtuelle, si vous y avez accès, ne pas hésiter à effectuer les modification précédentes.
