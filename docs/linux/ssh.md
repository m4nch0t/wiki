# Astuces SSH

## Génération d'une paire de clé SSH pour se connecter sans mot de passe sur vos serveurs

Attention, ceci n'est valable que si votre utilisateur est présent sur tous les serveurs!

```bash
ssh-keygen -t rsa -b 4096
```

Vous n'avez plus qu'à envoyer votre clé publique sur votre serveur de destination:

```bash
ssh-copy-id manchot@198.168.1.100
```

Ce qui aura pour effet de la rajouter dans le fichier **authorized_keys**.
Si la commande précédente ne fonctionne pas, vous pouvez le faire manuellement :

```bash
cat ~/.ssh/id_rsa.pub | ssh manchot@198.168.1.100 "mkdir -p ~/.ssh && chmod 700 ~/.ssh && cat >>  ~/.ssh/authorized_keys"
```

## Tester son fichier de configuration

```bash
sshd -T
```


## Config

Nous pouvons utiliser un fichier de configuration pour se simplifier les connexions :

```shell
# cat ~/.ssh/config

host webserver
    Hostname web-01.vmanchot.net
    User manchot
    Port 2222
    IdentityFile ~/.ssh/id_ed25519_webserver
```

Ainsi nous n'avons plus besoin de renseigner tous les paramètres, mais simplement :

```shell
ssh webserver
```
