# Extension LVM sur VM

L’un des principal avantage des machines virtuelles est la flexibilité du "virtual hardware". On peut rajouter/enlever du matériel à volonté.
Mais pour la partie stockage sous Linux, c’est assez délicat de rajouter un disque ou de l'agrandir à chaud sans avoir choisit un système de fichier ou un gestionnaire de stockage aui le permet. Comme dans le monde physique. Le LVM permet de palier à ce problème. Je vais vous expliquer comment augmenter vos partitions à chaud lorsque vous avez fait un extend ou un ajout de disque virtuel.
Vous pouvez vérifiez le nom de votre nouveau disque en regardans simplement dans ce fichier :

```bash
root@vps-manchot:~# cat /proc/partitions
major minor  #blocks  name

   8        0   20971520 sda
   8        1      96256 sda1
   8        2       1024 sda2
   8        3   20872192 sda3
   8       16     104448 sdb
  11        0     227328 sr0
 254        0   14647296 dm-0
 254        1     999424 dm-1
```

Nous voyons donc ici un nouveau disque /dev/sdb. Car au départ, je ne possédais qu'un seul disque, le sda.
J'ai pris pour habitude de configurer tous mes disques avec une table de partition en GPT, ce qui m'oblige à délaisser ce bon vieux fdisk/cfdisk pour l'utilitaire parted.

```bash
root@vps-manchot:~# parted
```

Maintenant que la partition a été créée et qu'elle possède le flag LVM, il nous faut créer le physical volume. La commande suivante va nous permettre de lister les physicals volumes présent sur notre serveur, et leurs taille.

```bash
root@vps-manchot:~# pvs
  PV         VG     Fmt  Attr PSize  PFree
  /dev/sda3  system lvm2 a--  19,90g 4,98g
```

Nous voyons qu’un seul volume, et nous devons donc initialiser le nouveau sur votre nouvelle partition:

```bash
root@vps-manchot:~# cat /proc/partitions
major minor  #blocks

   8        0   20971520 sda
   8        1      96256 sda1
   8        2       1024 sda2
   8        3   20872192 sda3
   8       16     104448 sdb
   8       17     104432 sdb1
  11        0     227328 sr0
 254        0   14647296 dm-0
 254        1     999424 dm-1
root@vps-manchot:~# pvcreate /dev/sdb1
  Writing physical volume data to disk "/dev/sdb1"
  Physical volume "/dev/sdb1" successfully created
```

Si vous relancez la commande pvs, vous verrez votre nouveau volume, mais non rattaché à unvolume groupe. Pour cela il vous suffit d’étendre votre volume group sur votre nouveau physical volume :

```bash
root@vps-manchot:~# pvs
  PV         VG     Fmt  Attr PSize   PFree  
  /dev/sda3  system lvm2 a--   19,90g   4,98g
  /dev/sdb1         lvm2 a--  101,98m 101,98m
root@vps-manchot:~# vgextend system /dev/sdb1
  Volume group "system" successfully
```

Si vous relancez pvs, vous verrez votre pv rattaché à un vg et un pvdisplay vous donnera plus d’informations.

```bash
root@vps-manchot:~# pvs
  PV         VG     Fmt  Attr PSize   PFree  
  /dev/sda3  system lvm2 a--   19,90g   4,98g
  /dev/sdb1  system lvm2 a--  100,00m 100,00m
root@vps-manchot:~# pvdisplay
  --- Physical volume ---
  PV Name               /dev/sda3
  VG Name               system
  PV Size               19,91 GiB / not usable 3,00 MiB
  Allocatable           yes
  PE Size               4,00 MiB
  Total PE              5095
  Free PE               1275
  Allocated PE          3820
  PV UUID               cM2AqP-uMVZ-VmWD-2Tyf-zZym-dnHn-pcqUOF

  --- Physical volume ---
  PV Name               /dev/sdb1
  VG Name               system
  PV Size               101,98 MiB / not usable 1,98 MiB
  Allocatable           yes
  PE Size               4,00 MiB
  Total PE              25
  Free PE               25
  Allocated PE          0
  PV UUID               B7aa5Q-6brP-7hrS-MUvt-SDCV-M03z-DDtSe7
```

Maintenant que nous nous sommes des parties physical volume et rattachement au volume group, nous pouvons agrandir la dernière couche du LVM, le logical volume :

```bash
root@vps-manchot:~# lvextend -L+10M /dev/mapper/system-root
  Rounding up size to full physical extent 12,00 MiB
  Extending logical volume root to 13,98 GiB
  Logical volume root successfully resized
```

Explication de la commande :
-L on augment en spécifiant une taille et non des PE (physical extend) petits blocs de 4MiB que l'on voit dans la sortie de la commande vgdisplay.
+10M, je souhaite donc rajouter 10Megabytes. On peut utiliser les suffixes suivants: M pour megabytes, G pour gigabytes, T pour terabytes, P pour petabytes et E pour exabytes si vous êtes vraiment riches.
/dev/mapper/system-root, le chemin absolu vers le lv que l'on veut agrandir.

Avec la commande lvdiplay, nous vérifions que notre lv a bien été agrandi.

```bash
root@vps-manchot:~# lvdisplay
  --- Logical volume ---
  LV Path                /dev/system/root
  LV Name                root
  VG Name                system
  LV UUID                k4LoYY-5VFK-q8t0-avIY-vNtj-T95f-kLkrjg
  LV Write Access        read/write
  LV Creation host, time vps-manchot, 2014-10-19 21:00:35 +0200
  LV Status              available
  # open                 1
  LV Size                13,98 GiB
  Current LE             3579
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           254:0
```

Pour savoir sur quel partitions ou disque se situent physiquement vos données vous devez passesr par cette commande un peu plus verbeuse :

```bash
root@vps-manchot:~# lvdisplay -m
  --- Logical volume ---
  LV Path                /dev/system/root
  LV Name                root
  VG Name                system
  LV UUID                k4LoYY-5VFK-q8t0-avIY-vNtj-T95f-kLkrjg
  LV Write Access        read/write
  LV Creation host, time vps-manchot, 2014-10-19 21:00:35 +0200
  LV Status              available
  # open                 1
  LV Size                13,99 GiB
  Current LE             3582
  Segments               2
  Allocation             inherit
  Read ahead sectors     auto
  - currently set to     256
  Block device           254:0
--- Segments ---
  Logical extent 0 to 3575:
    Type inear
    Physical volume /dev/sda3
    Physical extents 0 to 3575
Logical extent 3576 to 3581:
    Type linear
    Physical volume /dev/sda3
    Physical extents 3820 to 3825
```

Et vous voyez sur quel pv se situent vos extents.
Dernière étape, il faut étendre votre système de fichier, ici XFS :

```bash
root@vps-manchot:~# xfs_growfs /dev/mapper/system-root
meta-data=/dev/mapper/system-root isize=256    agcount=4, agsize=915456 blks
         =                       sectsz=512   attr=2
data     =                       bsize=4096   blocks=3661824, imaxpct=25
         =                       sunit=0      swidth=0 blks
naming   =version 2              bsize=4096   ascii-ci=0
log      =internal               bsize=4096   blocks=2560, version=2
         =                       sectsz=512   sunit=0 blks, lazy-count=1
realtime =none                   extsz=4096   blocks=0, rtextents=0
data blocks changed from 3661824 to 3691520
```

Attention, j’utilise l’utilitaire pour reiserfs, si vous n’êtes pas dans ce fs, pensez à utiliser resize2fs à la place.
Et voilà, un simple df vous permettra de vous rendre compte de l'agrandissement de votre partition grâce à LVM.
