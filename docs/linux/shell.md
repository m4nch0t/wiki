# Astuces Shell

Vous trouverez ici un ensemble de documentation pour mieux exploiter votre bash et gagner du temps.

## Rappel de commandes

* `!$` - Permet de rappeler le dernier argument de la ligne de commande précédente.

```bash
manchot$ cd /tmp
manchot$ ls !$
ls /tmp
sample.test
```

Vous pouvez aussi rappeler une n-ième commande, exemple nous allons rappeler la deuxième (avant dernière)  commande  :

```bash
manchot$ ls
manchot$ cd /root
manchot$ x-2
ls
sample.test
```

* `!!` - Permet de rappeler la dernière ligne de commande. Exemple si vous avez oublié d'utiliser sudo.

Remplace la première occurrence de *foo* par bar dans la commande la plus récente

```bash
!!:s/foo/bar/
```

Remplace toutes les occurrences de *foo* par bar dans la commande la plus récente

```bash
!!:gs/foo/bar/
```

## Redirection

```bash
sh test.sh > output.txt   # redirige la sortie standard vers un fichier
sh test.sh >> output.txt  # redirige la sortie standard vers un fichier, rajoute à la suite
sh test.sh 2> error.log   # redirige la sortie erreur vers un fichier
sh test.sh 2>&1           # redirige la sortie erreur vers la sortie standard
sh test.sh 2>/dev/null    # redirige la sortie erreur vers null
sh test.sh &>/dev/null    # redirige les sorties standard et erreur vers null
```

## Conditions

&& et || sont des séparateurs de commandes conditionnels.

### Opérateur &&

N'exécute la seconde commande que si la première a réussie

```shell
❯ true && echo it works!
it works!
❯ false && echo it works!
```

### Opérateur ||

N'exécute la seconde commande que si la première a échouée

```shell
❯ true || echo it works!
❯ false || echo it works!
it works!
```

## SSH Know Host

Si vous rencontrez une erreur de ce type :

```bash
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the ECDSA key sent by the remote host is
SHA256:oJElWxAHgX76z/lajz1csfe4daQ5ypPluud+51Vic00.
Please contact your system administrator.
Add correct host key in /home/manchot/.ssh/known_hosts to get rid of this message.
Offending ECDSA key in /home/manchot/.ssh/known_hosts:13
ECDSA host key for 192.168.0.22 has changed and you have requested strict checking.
Host key verification failed.
```

Au lieu d'éditer manuelle le ficher **known_hosts**, vous pouvez supprimer la ligne directement :

```bash
ssh-keygen -f "/root/.ssh/known_hosts" -R 192.168.0.22
```

## Tâches de fond

Pour stopper un processus, et le mettre en arrière plan, utiliser la combinaison de touche **ctrl** et **z**. Ce qui vous permet de lancer une commande et de revenir sur votre travail. Vous pouvez mettre plusieurs tâches en arrière plan et y revenir.

```bash
mbp-de-manchot:~ manchot$ tail -f /var/log/wifi.log
Mon Jun 10 14:36:40.597 <kernel>  IOCTL (from pid 190) not recognized: 69 out of 267
Mon Jun 10 14:36:40.598 <kernel>  IOCTL (from pid 190) not recognized: 5 out of 267
Mon Jun 10 15:34:32.718 <kernel>  IOCTL (from pid 190) not recognized: 5 out of 267
^Z
[1]+  Stopped                 tail -f /var/log/wifi.log
mbp-de-manchot:~ manchot$ tail -f /var/log/system.log

Jun 10 15:59:47 mbp-de-manchot AGMService[472]: ProcessPath : /Applications/TextEdit.app/Contents/MacOS/TextEdit
Jun 10 15:59:48 mbp-de-manchot TextEdit[18945]: assertion failed: 18F203: libxpc.dylib + 90677 [9A0FFA79-082F-3293-BF49-63976B073B74]: 0x89
Jun 10 15:59:48 mbp-de-manchot com.apple.xpc.launchd[1] (com.apple.imfoundation.IMRemoteURLConnectionAgent): Unknown key for integer: _DirtyJetsamMemoryLimit
Jun 10 16:03:49 mbp-de-manchot syslogd[43]: ASL Sender Statistics
^Z
[2]+  Stopped                 tail -f /var/log/system.log
mbp-de-manchot:~ manchot$ fg 2
tail -f /var/log/system.log
```

Vous pouvez remplacer le fg précédent par bg, ceci rappelle la commande précédemment stoppée et la relance en tache de fond (symbole &)

```bash
mbp-de-manchot:~ manchot$ bg
[2]+ tail -f /var/log/system.log &
```

## Tester la syntaxe d'un script

```bash
bash -n script.sh
```

## Suppression d’un type de fichier en bash

```bash
find / -type f -name « *.m4a » -exec /bin/rm ‘{}’ \;
```

Quand la commande précédente ne fonctionne pas (comme sur busybox, où le -exec n’est pas implémenté)

```bash
find / -type f -name « *.m4a » | xargs rm
```

Suppression dossier vide

```bash
find ./ -type d -exec rmdir 2>/dev/null {} \;
```

Quand la commande précédente ne fonctionne pas (comme sur busybox, où le -exec n’est pas implémenté)

```bash
find ./ -depth -type d -empty -exec rmdir
```

## Codes retour

La variable spéciale $? contient le code de retour d’exécution de la commande précédente.
Un succès est toujours à 0 et un échec à 1.

```shell
❯ true ; echo $?
0
❯ false ; echo $?
1
```

## Cheat Sheet

### String quotes

L'usage des simple quote ou double quote est limité par l'interprétation ou non du code à l'intérieur, exemple:
  
```bash
NAME="John"
echo "Hi $NAME"  #=> Hi John
echo 'Hi $NAME'  #=> Hi $NAME
```

### Substitution de commande

Les commandes suivantes auront le même résultat:

```bash
echo "I'm in $(pwd)"
echo "I'm in `pwd`"
```

### Accolades

```bash
    {A,B}.js    A.js B.js
    {1..5}      1 2 3 4 5
```

Exemples:

```bash
mkdir -p /home/{work,image,private}
mv test.{txt,md}    # change l'extension txt en md
mv test.txt{,.bak}  # rajout extension .bak
```

On peut lancer des commandes sur plusieurs hosts:

```bash
for server in apache-{1..4}
do
   ssh -t manchot@${server} sudo -- sh -c 'uptime'
done
```

### Substitution et découpage

```bash
name="John"
echo ${name}
echo ${name/J/j}    #=> "john" (substitution)
echo ${name:0:2}    #=> "Jo" (slicing)
echo ${name:(-1)}   #=> "n" (slicing from right)
echo ${name:(-2):1} #=> "h" (slicing from right)
```

### Lecture d'un fichier d'entrée

```bash
< file.txt | while read line; do
  echo $line
done
```


### Monitoring

```shell
## pass options to free ##
free -m -l -t
 
## lister les processus consommant de la mémoire
ps auxf | sort -nr -k 4
ps auxf | sort -nr -k 4 | head -10
 
## lister les processus consommant du cpu
ps auxf | sort -nr -k 3
ps auxf | sort -nr -k 3 | head -10
```

### Lancer plusieurs commandes sur une seule ligne

```shell
$ ls test/ ; ls test2/
test1.md
test2.md
```