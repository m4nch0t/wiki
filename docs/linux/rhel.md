# Red Hat Enterprise Linux

Vous trouverez ici un ensemble de documentation pour gérer votre serveur RHEL.

## Activation de la souscription et activation des repos

Nettoyage des anciennes souscriptions :

```bash
subscription-manager remove --all
subscription-manager unregister
subscription-manager clean
```

Enregistrer le serveur et attacher les souscriptions :

```bash
subscription-manager register
subscription-manager refresh
subscription-manager attach --auto
```

Lister les repos activés, disponibles et en activer:

```bash
subscription-manager repos --list-enabled
subscription-manager repos --list
subscription-manager repos --enable <repo>
subscription-manager attach --auto
```

## Activer le repos EPEL

EPEL (Extra Packages for Enterprise Linux) est un repository maintenu par la communauté de Fedora.

```bash
wget http://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm
rpm -ivh epel-release-latest-7.noarch.rpm
```
