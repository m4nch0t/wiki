# Ubuntu

Vous trouverez ici un ensemble de documentation pour gérer votre serveur Ubuntu.

## Configurer une IP statique sour Ubuntu 18.04 Bionic Beaver

Nous pouvons utiliser Netplan, pour cela, modifier le fichier yaml présent dans le répertoire Netplan. Par exemple :
**/etc/netplan/50-cloud-init.yaml**

```bash

# This file describes the network interfaces available on your system
# For more information, see netplan(5).
network:
  version: 2
  renderer: networkd
  ethernets:
    enp0s3:
     dhcp4: no
     addresses: [192.168.42.200/24]
     gateway4: 192.168.42.à
     nameservers:
       addresses: [8.8.8.8,1.1.1.1]
```

Un fois le fichier modifié il suffit de charger la conf :

```bash
sudo netplan apply
```

Ou en cas de problème :

```bash
sudo netplan --debug apply
```
