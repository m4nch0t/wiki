# Protéger vous des virus et malware sous Linux

## ClamAV

Voici l'antivirus OpenSource le plus connu et utilisé sous Linux.

Tout d'abord l'installation

```bash
apt install clamav
```

Pour avoir une interface graphique :

```bash
apt install clamtk
```

Mettons à jour la base de signature :

```bash
freshclam
```

Scan à la demande du répertoire home :

```bash
clamscan -r -i /home
```

* r : récursif
* i : montre les fichiers infectés

## Sophos

Si vous êtes en quête d'un antivirus/trojans/malwares non libre et qui fonctionne en temps réel, vous pouvez vous tourner vers celui de Sophos.
Il est à récupérer [ici](https://www.sophos.com/en-us/products/free-tools/sophos-antivirus-for-linux.aspx)
Un fois récupérer, il est simple à installer après extraction:

```bash
sh install.sh
```

Une fois installer, vous pouvez vérifier son état :

```bash
/opt/sophos-av/bin/savdstatus
```

Scan à la demande du répertoire home :

```bash
savscan /home
```

## chkrootkit ou rkhunter

Ces deux logiciels sont importants pour vérifier la présence de malware, rootkit et complèteront ClamAV.

Tout d'abord l'installation

```bash
apt install chkrootkit
```

Ou rkhunter sous CentOS et dérivés :

```bash
yum install rkhunter
```

Pour scanner, au choix :

```bash
chkrootki
rkunter -c
```
