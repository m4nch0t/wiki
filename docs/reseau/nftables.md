# Règles de filtrage netfilter via nftables

NFTABLES est une des solutions visant à remplacer le bon vieux iptables.

## Hooks

Nous pouvons **accrocher** nos règles de pare feu à des hooks (crochets en français) :

* INPUT : paquet à destination d'un processus local
* OUTPUT : paquet à destination d'une ressources distante
* FORWARD : paquet traversant le routeur
* PREROUTING : paquet modifié avant la décision de routage
* POSTROUTING : paquet modifié après la décision de routage
* INGRESS : filtrer traffic entrant (ethernet)

## TLDR

```shell
### ajout de règles
nft add table ip filter
### effacement de règle
nft delete table ip filter
### visualiser les règles
nft list tables
nft list table ip filter
### vider
nft flush table ip filter
nft flush rulset
```

## Pré-requis

Avoir les paquets iptables et conntrack installés sur votre serveur.

Liste des classes d'addresse en IPv4

* 10.0.0.0/8 -j (A)
* 172.16.0.0/12 (B)
* 192.168.0.0/16 (C)
* 224.0.0.0/4 (MULTICAST D)
* 240.0.0.0/5 (E)
* 127.0.0.0/8 (LOOPBACK)

Activer au besoin le routage sur votre machine :

```shell
echo 1 > /proc/sys/net/ipv4/ip_forward
### ou ###
echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/net-forward.conf
echo "net.ipv6.conf.all.forwarding = 1" > /etc/sysctl.d/net-forward.conf
```

Désactiver **iptables**

```shell
systemctl disable iptables.services
```

## Persistance

```shell
nft list ruleset > /etc/nftables.conf
systemctl enable nftables.service
```

## Actions

* accept, action terminale (immediate). Accepte le paquet comme son nom l'indique. Le paquet peut être refusé par une règle ultérieure.
* drop, action terminale (immediate). Jette le paquet comme son nom l'indique. Le paquet est immédiatement rejetté.
* queue, met en file d'attente le paquet
* continue, action par défaut si aucune action terminale
* return, renvoi à la première regle de la dernière chaine
* jump chain, renvoi à la première règle de la chaine
* log, permet de logger

## Monitor

Voir tous les evenements :

```shell
nft monitor
```

Voir tous les evenements dans les chains au format JSON :

```shell
nft -j monitor chains
```

Voir tous les évenements de la ruleset (chain, rule, set, counters and quotas) :

```shell
nft monitor ruleset
```
