# Règles de filtrage netfilter via iptables

Il esxiste actuellement plusieurs systèmes pour gérer Netfilter, voici quelques règles iptables qui vont vous permettre de mieux appréhender cet outil. Il est selon moins un des meilleurs pour apprendre.


## Hooks

Nous pouvons **accrocher** nos règles de pare feu à des hooks (crochets en français) :

* INPUT : paquet à destination d'un processus local
* OUTPUT : paquet à destination d'une ressources distante
* FORWARD : paquet traversant le routeur
* PREROUTING : paquet modifié avant la décision de routage
* POSTROUTING : paquet modifié après la décision de routage
* INGRESS : filtrer traffic entrant (ethernet)


## Pré-requis

Avoir les paquets iptables et conntrack installés sur votre serveur.

Liste des classes d'addresse en IPv4

* 10.0.0.0/8 -j (A)
* 172.16.0.0/12 (B)
* 192.168.0.0/16 (C)
* 224.0.0.0/4 (MULTICAST D)
* 240.0.0.0/5 (E)
* 127.0.0.0/8 (LOOPBACK)

Activer au besoin le routage sur votre machine :

```shell
echo 1 > /proc/sys/net/ipv4/ip_forward
### ou ###
echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/net-forward.conf
echo "net.ipv6.conf.all.forwarding = 1" > /etc/sysctl.d/net-forward.conf
```

## Création des règles

### Explication de certains arguments

* -F : Flush, supprime toutes les règles
* -X : Supprime une chaine
* -t table_name : supprime les règles d'une table (NAT/MANGLE)
* -P : définit la politique par défaut
* -D : supprime une règle
### Nettoyage

Avant d'écrire une quelconque règle, il est préférable de nettoyer votre pare-feu pour se faire piéger.

#### Nettoyage de la table filter

Supprime toutes les chaines

```bash
iptables -t filter -F
ip6tables -t filter -F
```

Supprime toutes les chaines utilisateurs

```bash
iptables -t filter -X
ip6tables -t filter -X
```

#### Nettoyage de la table nat

Supprime toutes les chaines

```bash
iptables -t nat -F
```

#### Supprime toutes les chaines utilisateurs

```bash
iptables -t nat -X
```

#### Supprime toutes les trames dans les chaines INPUT/OUTPUT/FORWARD de la table nat

```bash
iptables -t nat -P PREROUTING ACCEPT
iptables -t nat -P POSTROUTING ACCEPT
iptables -t nat -P OUTPUT ACCEPT
```

#### Nettoyage de la table mangle (taguage des paquets)

Supprime toutes les chaines

```bash
iptables -t mangle -F
ip6tables -t mangle -F
```

Supprime toutes les chaines utilisateurs

```bash
iptables -t mangle -X
ip6tables -t mangle -X
```

#### Supprime toutes les trames dans les chaines INPUT/OUTPUT/FORWARD

```bash
iptables -t mangle -P PREROUTING ACCEPT
iptables -t mangle -P POSTROUTING ACCEPT
iptables -t mangle -P INPUT ACCEPT
iptables -t mangle -P OUTPUT ACCEPT
iptables -t mangle -P FORWARD ACCEPT
ip6tables -t mangle -P PREROUTING ACCEPT
ip6tables -t mangle -P POSTROUTING ACCEPT
ip6tables -t mangle -P INPUT ACCEPT
ip6tables -t mangle -P OUTPUT ACCEPT
ip6tables -t mangle -P FORWARD ACCEPT
```

### Création des règles par défaut

Nous allons appliquer une politique de drop par défaut.

```bash
iptables -t filter -P INPUT DROP
iptables -t filter -P OUTPUT DROP
iptables -t filter -P FORWARD DROP
ip6tables -t filter -P INPUT DROP
ip6tables -t filter -P OUTPUT DROP
ip6tables -t filter -P FORWARD DROP
```

Sans casser les connexions déjà établies (ssh par exemple)

```bash
iptables -A INPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A OUTPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
iptables -A FORWARD -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -A INPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -A OUTPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
ip6tables -A FORWARD -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
```

### Autorisation loopback

```bash
iptables -A INPUT -p all -i lo -j ACCEPT
iptables -A OUTPUT -p all -o lo -j ACCEPT
```

### Dropper une addresse mac :

```bash
iptables -A INPUT -m mac --mac-source 00:60:56:45:00:08 -j DROP
```

### ICMP V4

Selon la RFC 792, tous les serveurs doivent répondre àu Type 0 (réponse echo)

```bash
iptables -A INPUT -p icmp --icmp-type echo-request -i eth0 -m limit --limit 6/s -mconntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
iptables -A INPUT -p icmp --icmp-type 0  -m conntrack --ctstate NEW -j ACCEP T
#Type 3 (destinataire inaccessible)
iptables -A INPUT -p icmp --icmp-type 3  -m conntrack --ctstate NEW -j ACCEP T
#Type 11 (temps dépassé)
iptables -A INPUT -p icmp --icmp-type 11 -m conntrack --ctstate NEW -j ACCEP T
```

### ICMP V6

Selon la RFC 4890, les serveurs doivent répondre à ces différents types:

```bash
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 1   -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 2   -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 3   -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 4   -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 133 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 134 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 135 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 136 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 137 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 141 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 142 -j ACCEPT
ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 130 -j ACCEPT
ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 131 -j ACCEPT
ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 132 -j ACCEPT
ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 143 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 148 -j ACCEPT
ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 149 -j ACCEPT
ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 151 -j ACCEPT
ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 152 -j ACCEPT
ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 153 -j ACCEPT
```

### Atténuation du DDOS

Déclarons les variables des limites de notre serveur:

```bash
MAXHIT=40
BURST=60
```

Création d'une nouvelle chaine:

```bash
iptables -N DDOS
```

Définissons maintenant imites de trafic au delà desquelles on drop les paquets. On vérifie que le paquet entre dans les limites et l'accepte puis stocke l'ip dans une table de hash pendant 1min pour comparaison ultérieure

```bash
iptables -A DDOS -m hashlimit --hashlimit-name DDOS --hashlimit-upto $MAXHIT /minute --hashlimit-mode srcip --hashlimit-burst $BURST --hashlimit-htable-expir e 60000 -j ACCEPT
```

Si le paquet est hors limite, il est dropé:

```bash
iptables -A DDOS -j DROP
```

Maintenant on va passer tout le trafic HTTP dans la chaîne:

```bash
iptables -A INPUT -p tcp --dport 80 -j DDOS
iptables -A INPUT -p tcp --dport 443 -j DDOS
```

### Ajout d'une chaîne dédiée aux logs

```bash
iptables -N DROP_INPUT
iptables -A INPUT -j DROP_INPUT
iptables -A DROP_INPUT -j LOG --log-prefix "[DROP_INPUT - firewall]"
iptables -A DROP_INPUT -p all -s $NET_ANYWHERE -j REJECT
iptables -A DROP_INPUT -j DROP

iptables -N DROP_OUTPUT
iptables -A OUTPUT -j DROP_OUTPUT
iptables -A DROP_OUTPUT -j LOG --log-prefix "[DROP_OUTPUT - firewall]"
iptables -A DROP_OUTPUT -p all -s $NET_ANYWHERE -j REJECT
iptables -A DROP_OUTPUT -j DROP

iptables -N DROP_FORWARD
iptables -A FORWARD -j DROP_FORWARD
iptables -A DROP_FORWARD -j LOG --log-prefix "[DROP_FORWARD - firewall] "
iptables -A DROP_FORWARD -p all -s $NET_ANYWHERE -j REJECT
iptables -A DROP_FORWARD -j DROP
```

### Attaques communes

#### DROP paquet NULL (sans flag)

Maintenons un log de l'attaque

```bash
iptables -A INPUT -p tcp --tcp-flags ALL NONE -m limit --limit 3/m --limit-b urst 5 -j LOG --log-prefix "[DROP_INPUT - log Null Scan]"
```

DROP et blacklist de l'IP de l'attaquant pendant 60 sec

```bash
iptables -A INPUT -p tcp --tcp-flags ALL NONE  -m recent --name blacklist_60 --set -m comment --comment "[DROP_INPUT - Null Scan]" -j DROP
```

#### DROP attaque XMAS

Maintenons un log de l'attaque

```bash
iptables -A INPUT -p tcp --tcp-flags ALL FIN,PSH,URG -m limit --limit 3/m -- limit-burst 5 -j LOG --log-prefix "[DROP_INPUT - Xmas scan]"
iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -m limit --limi t 3/m --limit-burst 5 -j LOG --log-prefix "[DROP_INPUT - log Xmas/PSH scan]"
iptables -A INPUT -p tcp --tcp-flags ALL ALL -m limit --limit 3/m --limit-bu rst 5 -j LOG --log-prefix "[DROP_INPUT - Xmas/All scan]"
```

DROP et blacklist de l'IP de l'attaquant pendant 60 sec

```bash
iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -m recent --nam e blacklist_60 --set  -m comment --comment "[DROP_INPUT - Xmas scan]" -j DROP # nmap -sX (Xmas tree scan)
iptables -A INPUT -p tcp --tcp-flags ALL FIN,PSH,URG -m recent --name blackl ist_60 --set  -m comment --comment "[DROP_INPUT - Xmas/PSH scan]" -j DROP
iptables -A INPUT -p tcp --tcp-flags ALL ALL -m recent --name blacklist_60 - -set  -m comment --comment "[DROP_INPUT - Xmas/All scan]" -j DROP
```

#### DROP attaque FIN scan, évite la découverte de ports fermés

Maintenons un log de l'attaque

```bash
iptables -A INPUT -p tcp --tcp-flags ALL FIN -m limit --limit 3/m --limit-bu rst 5 -j LOG --log-prefix "[DROP_INPUT - log FIN scan]"
```

DROP et blacklist de l'IP de l'attaquant pendant 60 sec

```bash
iptables -A INPUT -p tcp --tcp-flags ALL FIN -m recent --name blacklist_60 --set  -m comment --comment "[DROP_INPUT - FIN scan]" -j DROP
```

#### DROP des connections ne commençant pas par un SYN

```bash
iptables   -A INPUT -p tcp ! --syn -m state --state NEW  -m comment --comment "[DROP_INPUT - TCP connection not starting by SYN] " -j DROP
```

#### DROP des cans UDP

```bash
iptables -A INPUT -p udp  -m limit --limit 6/h --limit-burst 1 -m length --length 0:28 -j LOG --log-prefix "[DROP_INPUT - paquet UDP avec une taille à 0]"
iptables -A INPUT -p udp -m length --length 0:28 -m comment --comment "[DROP _INPUT - paquet UDP vide]" -j DROP
```

#### Protection contre les amplifications DNS

```bash
iptables -A INPUT  -p udp -m udp --dport 53 -m string --from 50 --algo bm -- hex-string '|0000FF0001|' -m recent --set --name dnsanyquery --rsource
iptables -A INPUT  -p udp -m udp --dport 53 -m string --from 50 --algo bm -- hex-string "|0000FF0001|" -m recent --name dnsanyquery --rcheck --seconds 10 --h itcount 1 -m comment --comment "[DROP_INPUT - ampli DNS]" -j DROP
```

#### Dropper silencieusement tous les paquets broadcastés

```bash
iptables -A INPUT -m pkttype --pkt-type broadcast -j DROP
```

#### Dropper les paquets non conformes, avec des en tetes mal formées

```bash
iptables -A INPUT -m conntrack --ctstate INVALID -j DROP
```

#### Dropper les paquets à destination d'adresses broadcast,multicast ou anyc ast

```bash
iptables -A INPUT -m addrtype --dst-type BROADCAST -j DROP
iptables -A INPUT -m addrtype --dst-type MULTICAST -j DROP
iptables -A INPUT -m addrtype --dst-type ANYCAST -j DROP
iptables -A INPUT -d 224.0.0.0/4 -j DROP
```

#### Dropper les paquets ressemblant à une attaque brute force sur SSH

Permet 5 nouvelles connections en 5 minutes pour une machine source puis drop des connections venant de cette machine

```bash
iptables -N SSHBRUTE
iptables -A SSHBRUTE -m recent --name SSH --set
iptables -A SSHBRUTE -m recent --name SSH --update --seconds 300 --hitcount 5 -mlimit --limit 1/second --limit-burst 100 -j LOG --log-prefix "[DROP_INPUT - attaquebrute force SSH]"
iptables -A SSHBRUTE -m recent --name SSH --update --seconds 300 --hitcount 5 -mcomment --comment "[DROP_INPUT - attaque brute force SSH]" -j DROP
iptables -A SSHBRUTE -j ACCEPT
```

On peut aussi utiliser le module *connlimit* :

```bash
iptables -A INPUT -p tcp --syn --dport 22 -m connlimit --connlimit-above 5 -j REJECT
iptables -p tcp --syn --dport 80 -m connlimit --connlimit-above 20 --connlimit-mask 24 -j DROP
```

* --connlimit-above 5 : La règle est match s'il y a plus de 5 connexion
* --connlimit-mask 24 : Groupe les IP par leur préfix (entre 0 et 32 en ipv4)

#### Dropper les paquets ICMP flooding

On autorise 6 pings par secondes pour une machine source
Protège aussi de l'ICMP REPLY lorsque nous répondrons à une machine vérolé envoyant des ICMP ECHO ##

```bash
iptables -N ICMPFLOOD
iptables -A ICMPFLOOD -m recent --name ICMP --rsource --set
iptables -A ICMPFLOOD -m recent --name ICMP --update --seconds 1 --hitcount 6--rsource --rttl -m limit --limit 1/sec --limit-burst 1 -j LOG --log-prefix "[ DROP- log ICMP FLOOD]"
iptables -A ICMPFLOOD -m recent --name ICMP --update --seconds 1 --hitcount 6--rsource --rttl -m comment --comment "[DROP - ICMP FLOOD]" -j DROP
iptables -A ICMPFLOOD -j ACCEPT
ip6tables -N ICMPFLOODIPV6
ip6tables -A ICMPFLOODIPV6 -m recent --name ICMPIPV6 --rsource --set
ip6tables -A ICMPFLOODIPV6 -m recent --name ICMPIPV6 --update --seconds 1 --hitcount 6 --rsource --rttl -m limit --limit 1/sec --limit-burst 1 -j LOG --log-prefix "[DROP - log ICMP FLOOD IPV6]"
ip6tables -A ICMPFLOODIPV6 -m recent --name ICMPIPV6 --update --seconds 1 --hitcount 6 --rsource --rttl -m comment --comment "[DROP - ICMP FLOOD IPV6]" -j D ROP
ip6tables -A ICMPFLOODIPV6 -j ACCEPT
iptables -A INPUT -p icmp --icmp-type 8  -m conntrack --ctstate NEW -j ICMPF LOOD
ip6tables -A INPUT -p ipv6-icmp --icmpv6-type 128 -j ICMPFLOODIPV6
```


### Rediriger

Si vous souhaitez rediriger vers un autre port :

```bash
iptables -t nat -I PREROUTING --src 0/0 --dst 192.168.20.2 -p tcp --dport 80 -j REDIRECT --to-ports 8080
```
## Script de démarrage

Voici un exemple de sript init que j'ai utilisé (*/etc/init.d/iptables*) il y a quelques années.

```bash
### BEGIN INIT INFO
# Provides:          IptablesRulesM4nch0t
# Required-Start:    $network
# Required-Stop:     $network
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Demarrage du script lors de la sequence de boot
# Description:       Ajout des regles de parefeu
### END INIT INFO

#!/bin/sh
case "$1" in
start)

    echo - Initialisation du firewall :

    ## Déclartion des variables
    HOME="150.190.185.42"
    VPS_IPV4=`ip -f inet -o addr show eth0 | awk '{ print $4}' | awk -F "/" '{ p rint $1}'`
    VPS_IPV6=`cat /etc/network/interfaces |grep address |grep "[0-9A-Z]*:" | awk -F " " '{ print $2}'`
    NET_ANYWHERE="0.0.0.0/0"
    NTP_DEBIAN="195.154.216.35 5.39.71.117 195.154.108.164 195.154.115.15 37.123 .115.64 37.187.107.140 178.32.216.71 194.57.169.1 37.187.105.73 91.121.60.158 10 9.69.184.209 37.59.118.50 195.154.216.44 212.83.131.33 213.186.36.183 195.154.11 3.163"
    DNS_OVH="213.186.33.102 213.186.33.99 213.251.128.136 213.251.188.129 213.25 1.128.129 213.251.188.130 213.251.128.130 213.251.188.131 213.251.128.131 213.25 1.188.132 213.251.128.132 213.251.188.133 213.251.128.133 213.251.188.134 213.25 1.128.134 213.251.188.135 213.251.128.135 213.251.188.136 213.251.128.136 213.25 1.188.136 213.251.128.137 213.251.188.136 213.251.128.138 213.251.188.136 213.25 1.128.139 213.251.188.136 213.251.128.140 213.251.188.136 213.251.128.141 213.25 1.188.136 213.251.128.142 213.251.188.136 213.251.128.143 213.251.188.136 213.25 1.128.144 213.251.188.136 213.251.128.145 213.251.188.136 213.251.128.146 213.25 1.188.136 213.251.128.147 213.251.188.136 213.251.128.148 213.251.188.136 213.25 1.128.149 213.251.188.136 213.251.128.150"
    DNS_ROOT="198.41.0.4 192.228.79.201 192.33.4.12 199.7.91.13 192.203.230.10 1 92.5.5.241 192.112.36.4 128.63.2.53 192.36.148.17 192.58.128.30 193.0.14.129 199 .7.83.42 202.12.27.33 42.83.7.199 53.2.63.128"
    DNS_NET="192.5.6.30 192.33.14.30 192.26.92.30 192.31.80.30 192.12.94.30 192. 35.51.30 192.42.93.30 192.54.112.30 192.43.172.30 192.48.79.30 192.52.178.30 192 .41.162.30 192.55.83.30"
    DNS_WORDPRESS="192.0.80.93 69.174.248.148 207.198.112.47 192.0.81.10 69.174. 248.149 207.198.112.48"
    DNS_GANDI="173.246.97.2 217.70.182.20 217.70.184.40"
    DNS_CLOUDFLARE="173.245.58.100 173.245.58.106 173.245.58.114 173.245.58.117 173.245.58.131 173.245.58.137 173.245.58.138 173.245.59.109 173.245.59.115 173.2 45.59.120 173.245.59.134 173.245.59.142 173.245.59.143 173.245.59.145 173.245.59 .131 173.245.58.134"
    DNS_CLOUDFLARE_IPV6="2400:cb00:2049:1::adf5:3a63 2400:cb00:2049:1::adf5:3b63 2400:cb00:2049:0001:0000:0000:adf5:3b6d 2400:cb00:2049:0001:0000:0000:adf5:3a75 "
    DNS_VMWARETIPS="72.15.252.38 72.15.252.39"
    DNS_ANYCAST_ME="46.105.223.200 46.105.198.200"
    DNS_TUX_FAMILY="188.121.227.54 212.85.158.2"
    DNS_LINUX_FUNDATION="140.211.169.10 140.211.169.11"
    DNS_GODADDY="216.69.185.35 208.109.255.35 216.69.185.9 208.109.255.9"
    DNS_GODADDY_IPV6="2607:f208:0206:0000:0000:0000:0000:0023 2607:f208:0302:000 0:0000:0000:0000:0023 2607:f208:0206:0000:0000:0000:0000:0009 2607:f208:0302:000 0:0000:0000:0000:0009"
    DNS_NAMESPACE4YOU="193.223.77.3 80.67.16.124"
    DNS_CNET="64.30.231.191 64.30.227.124 62.108.138.30"
    DNS_1AND1="217.160.80.170 217.160.81.170"
    DNS_GOOGLE="216.239.34.10 216.239.32.10 216.239.36.10 216.239.38.10 8.8.8.8 8.8.4.4"
    DNS_1AND1_IPV6="2001:08d8:00fe:0053:0000:d9a0:50aa:0100 2001:08d8:00fe:0053: 0000:d9a0:51aa:0100"
    DNS_IPV4="$DNS_OVH $DNS_ROOT $DNS_NET $DNS_WORDPRESS $DNS_GANDI $DNS_CLOUDFL ARE $DNS_VMWARETIPS $DNS_ANYCAST_ME $DNS_TUX_FAMILY $DNS_GODADDY $DNS_NAMESPACE4 YOU $DNS_CNET $DNS_1AND1 $DNS_LINUX_FUNDATION $DNS_GOOGLE"
    DNS_IPV6="$DNS_CLOUDFLARE_IPV6 $DNS_GODADDY_IPV6 $DNS_1AND1_IPV6"
    MIROIR_OVH="37.187.68.202"
    MIROIR_DEBIAN="212.211.132.32"
    MIROIR_DOT_DEB="195.154.242.153"
    LST_MIROIRS="$MIROIR_OVH $MIROIR_DEBIAN $MIROIR_DOT_DEB"
    LST_WORDPRESS="66.155.40.250 66.155.40.249"
    BLACKLIST_IP="93.125.8.129 104.192.0.20 66.215.83.137 119.10.4.37 61.240.144 .64 37.187.45.250 222.186.15.239 222.161.4.149"
    echo - Déclaration des variables : [OK]

    ### Test si le module conntrack est lancé
    # Modules
    MODULE1="nf_conntrack_ipv4"
    MODULE2="nf_conntrack_ipv6"
    MODULE3="nf_conntrack_netlink"
    MODULE4="nf_conntrack"

    #Si les modules ne sont pas lancés, lancement de ces derniers
    MODEXIST1=/sbin/lsmod | grep "$MODULE1"

    if [ -z "$MODEXIST1" ]; then
        /sbin/modprobe "$MODULE1" >/dev/null 2>&1
    fi
    MODEXIST2=/sbin/lsmod | grep "$MODULE2"

    if [ -z "$MODEXIST2" ]; then
        /sbin/modprobe "$MODULE2" >/dev/null 2>&1
    fi
    MODEXIST3=/sbin/lsmod | grep "$MODULE3"

    if [ -z "$MODEXIST3" ]; then
        /sbin/modprobe "$MODULE3" >/dev/null 2>&1
    fi
    MODEXIST3=/sbin/lsmod | grep "$MODULE4"

    if [ -z "$MODEXIST4" ]; then
        /sbin/modprobe "$MODULE4" >/dev/null 2>&1
    fi

    ### Test si le paquet conntrack est installé
    PAQUET1="conntrack"
    if [ -z "$PAQUET1" ]; then
        /usr/bin/apt-get -y install "$PAQUET1" >/dev/null 2>&1
    fi

    echo - Chargement de conntrack : [OK]

    ### Nettoyage de la table filter ###
    ## Supprime toutes les chaines ##
    iptables -t filter -F
    ip6tables -t filter -F
    ## Supprime toutes les chaines utilisateurs ##
    iptables -t filter -X
    ip6tables -t filter -X

    ### Nettoyage de la table nat ###
    ## Supprime toutes les chaines ##
    iptables -t nat -F
    ## Table nat non initialisée en ipv6
    #ip6tables -t nat -F
    ## Supprime toutes les chaines utilisateurs ##
    iptables -t nat -X
    ## Table nat non initialisée en ipv6
    #ip6tables -t nat -X
    ## Supprime toutes les trames dans les chaines INPUT/OUTPUT/FORWARD ##
    iptables -t nat -P PREROUTING ACCEPT
    iptables -t nat -P POSTROUTING ACCEPT
    iptables -t nat -P OUTPUT ACCEPT
    ## Table nat non initialisée en ipv6
    #ip6tables -t nat -P PREROUTING ACCEPT
    #ip6tables -t nat -P POSTROUTING ACCEPT
    #ip6tables -t nat -P OUTPUT ACCEPT

    ### Nettoyage de la table mangle (tagage des paquets) ###
    ## Supprime toutes les chaines ##
    iptables -t mangle -F
    ip6tables -t mangle -F
    ## Supprime toutes les chaines utilisateurs ##
    iptables -t mangle -X
    ip6tables -t mangle -X
    ## Supprime toutes les trames dans les chaines INPUT/OUTPUT/FORWARD ##
    iptables -t mangle -P PREROUTING ACCEPT
    iptables -t mangle -P POSTROUTING ACCEPT
    iptables -t mangle -P INPUT ACCEPT
    iptables -t mangle -P OUTPUT ACCEPT
    iptables -t mangle -P FORWARD ACCEPT
    ip6tables -t mangle -P PREROUTING ACCEPT
    ip6tables -t mangle -P POSTROUTING ACCEPT
    ip6tables -t mangle -P INPUT ACCEPT
    ip6tables -t mangle -P OUTPUT ACCEPT
    ip6tables -t mangle -P FORWARD ACCEPT

    echo - Vidage des regles et des tables : [OK]

    ############################################################################ ###

    ### Politique de DROP par défaut ###
    iptables -t filter -P INPUT DROP
    iptables -t filter -P OUTPUT DROP
    iptables -t filter -P FORWARD DROP
    ip6tables -t filter -P INPUT DROP
    ip6tables -t filter -P OUTPUT DROP
    ip6tables -t filter -P FORWARD DROP
    echo - Interdire toutes les connexions entrantes et sortantes : [OK]

    ############################################################################ ###

    ### Ne pas casser les connexions etablies ###
    iptables -A INPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
    iptables -A OUTPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
    iptables -A FORWARD -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
    ip6tables -A INPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
    ip6tables -A OUTPUT -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
    ip6tables -A FORWARD -p all -m state --state ESTABLISHED,RELATED -j ACCEPT
    echo - Ne pas casser les connexions établies : [OK]

    ############################################################################ ###

    ### Atténuation DDOS ###

    MAXHIT=40
    BURST=60

    ## créé une nouvelle chaine iptables
    iptables -N DDOS

    # definir les limites de trafic au delà desquelles on vire les paquets
    # vérifie que le paquet entre dans les limites et l'accepte
    # puis stocke l'ip dans une table de hash pendant 1mn pour comparaison ultér ieure
    iptables -A DDOS -m hashlimit --hashlimit-name DDOS --hashlimit-upto $MAXHIT /minute --hashlimit-mode srcip --hashlimit-burst $BURST --hashlimit-htable-expir e 60000 -j ACCEPT

    # si le paquet est hors limite, il est dropé
    iptables -A DDOS -j DROP

    # passer tout le trafic dans le chaine seuil
    iptables -A INPUT -p tcp --dport 80 -j DDOS
    iptables -A INPUT -p tcp --dport 443 -j DDOS

    echo - DDOS : [OK]

    ############################################################################ ###

    ### DROP paquet NULL (sans flag) ###
    # Log de l'attaque
    iptables -A INPUT -p tcp --tcp-flags ALL NONE -m limit --limit 3/m --limit-b urst 5 -j LOG --log-prefix "[DROP_INPUT - log Null Scan]"
    # DROP et blacklist de l'IP de l'attaquant pendant 60 sec
    iptables -A INPUT -p tcp --tcp-flags ALL NONE  -m recent --name blacklist_60 --set -m comment --comment "[DROP_INPUT - Null Scan]" -j DROP

    echo - DROP paquets NULL : [OK]

    ############################################################################ ###

    ### DROP attaque XMAS ###
    # Log de l'attaque
    iptables -A INPUT -p tcp --tcp-flags ALL FIN,PSH,URG -m limit --limit 3/m -- limit-burst 5 -j LOG --log-prefix "[DROP_INPUT - Xmas scan]"
    iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -m limit --limi t 3/m --limit-burst 5 -j LOG --log-prefix "[DROP_INPUT - log Xmas/PSH scan]"
    iptables -A INPUT -p tcp --tcp-flags ALL ALL -m limit --limit 3/m --limit-bu rst 5 -j LOG --log-prefix "[DROP_INPUT - Xmas/All scan]"
    # DROP et blacklist de l'IP de l'attaquant pendant 60 sec
    iptables -A INPUT -p tcp --tcp-flags ALL SYN,RST,ACK,FIN,URG -m recent --nam e blacklist_60 --set  -m comment --comment "[DROP_INPUT - Xmas scan]" -j DROP # nmap -sX (Xmas tree scan)
    iptables -A INPUT -p tcp --tcp-flags ALL FIN,PSH,URG -m recent --name blackl ist_60 --set  -m comment --comment "[DROP_INPUT - Xmas/PSH scan]" -j DROP
    iptables -A INPUT -p tcp --tcp-flags ALL ALL -m recent --name blacklist_60 - -set  -m comment --comment "[DROP_INPUT - Xmas/All scan]" -j DROP

    echo - DROP XMAS : [OK]

    ############################################################################ ###

    ### DROP attaque FIN scan, évite la découverte de ports fermés ###
    # Log de l'attaque
    iptables -A INPUT -p tcp --tcp-flags ALL FIN -m limit --limit 3/m --limit-bu rst 5 -j LOG --log-prefix "[DROP_INPUT - log FIN scan]"
    # DROP et blacklist de l'IP de l'attaquant pendant 60 sec
    iptables -A INPUT -p tcp --tcp-flags ALL FIN -m recent --name blacklist_60 - -set  -m comment --comment "[DROP_INPUT - FIN scan]" -j DROP

    echo - DROP FIN : [OK]

    ############################################################################ ###

    ### DROP des connections ne commençant pas par un SYN ###
    iptables   -A INPUT -p tcp ! --syn -m state --state NEW  -m comment --commen t "[DROP_INPUT - TCP connection not starting by SYN] " -j DROP

    echo - DROP SYN : [OK]

    ############################################################################ ###

    ### DROP des cans UDP ###
    iptables -A INPUT -p udp  -m limit --limit 6/h --limit-burst 1 -m length --l ength 0:28 -j LOG --log-prefix "[DROP_INPUT - paquet UDP avec une taille à 0]"
    iptables -A INPUT -p udp -m length --length 0:28 -m comment --comment "[DROP _INPUT - paquet UDP vide]" -j DROP

    echo - DROP UDP : [OK]

    ############################################################################ ###

    ### Protection contre les amplifications DNS ###
    iptables -A INPUT  -p udp -m udp --dport 53 -m string --from 50 --algo bm -- hex-string '|0000FF0001|' -m recent --set --name dnsanyquery --rsource
    iptables -A INPUT  -p udp -m udp --dport 53 -m string --from 50 --algo bm -- hex-string "|0000FF0001|" -m recent --name dnsanyquery --rcheck --seconds 10 --h itcount 1 -m comment --comment "[DROP_INPUT - ampli DNS]" -j DROP

    echo - DROP AMPLI DNS : [OK]

    ############################################################################ ###

    ### Dropper silencieusement tous les paquets broadcastés. ###
    iptables -A INPUT -m pkttype --pkt-type broadcast -j DROP

    echo - DROP BROADCAST : [OK]

    ############################################################################ ###

    ### Dropper les paquets non conformes, avec des en tetes mal formées ###
    iptables -A INPUT -m conntrack --ctstate INVALID -j DROP

    ############################################################################ ###

    ### Dropper les paquets à destination d'adresses broadcast,multicast ou anyc ast ###
    iptables -A INPUT -m addrtype --dst-type BROADCAST -j DROP
    iptables -A INPUT -m addrtype --dst-type MULTICAST -j DROP
    iptables -A INPUT -m addrtype --dst-type ANYCAST -j DROP
    iptables -A INPUT -d 224.0.0.0/4 -j DROP

    echo - DROP MULTICAST BROADCAST ANYCAST : [OK]

    ############################################################################ ###

    ### Dropper les paquets ressemblant à une attaque brute force sur SSH ###
    ## Permet 5 nouvelles connections en 5 minutes pour une machine source puis drop des connections venant de cette machine ##
    iptables -N SSHBRUTE
    iptables -A SSHBRUTE -m recent --name SSH --set
    iptables -A SSHBRUTE -m recent --name SSH --update --seconds 300 --hitcount 5 -m limit --limit 1/second --limit-burst 100 -j LOG --log-prefix "[DROP_INPUT - attaque brute force SSH]"
    iptables -A SSHBRUTE -m recent --name SSH --update --seconds 300 --hitcount 5 -m comment --comment "[DROP_INPUT - attaque brute force SSH]" -j DROP
    iptables -A SSHBRUTE -j ACCEPT

    echo - DROP SSH BRUTE FORCE : [OK]

    ############################################################################ ###

    ### Dropper les paquets ICMP flooding ###
    ## On autorise 6 pings par secondes pour une machine source ##
    ## Protège aussi de l'ICMP REPLY lorsque nous répondont à une machine vérolé envoyant des ICMP ECHO ##
    iptables -N ICMPFLOOD
    iptables -A ICMPFLOOD -m recent --name ICMP --rsource --set
    iptables -A ICMPFLOOD -m recent --name ICMP --update --seconds 1 --hitcount 6 --rsource --rttl -m limit --limit 1/sec --limit-burst 1 -j LOG --log-prefix "[ DROP - log ICMP FLOOD]"
    iptables -A ICMPFLOOD -m recent --name ICMP --update --seconds 1 --hitcount 6 --rsource --rttl -m comment --comment "[DROP - ICMP FLOOD]" -j DROP
    iptables -A ICMPFLOOD -j ACCEPT
    ip6tables -N ICMPFLOODIPV6
    ip6tables -A ICMPFLOODIPV6 -m recent --name ICMPIPV6 --rsource --set
    ip6tables -A ICMPFLOODIPV6 -m recent --name ICMPIPV6 --update --seconds 1 -- hitcount 6 --rsource --rttl -m limit --limit 1/sec --limit-burst 1 -j LOG --log- prefix "[DROP - log ICMP FLOOD IPV6]"
    ip6tables -A ICMPFLOODIPV6 -m recent --name ICMPIPV6 --update --seconds 1 -- hitcount 6 --rsource --rttl -m comment --comment "[DROP - ICMP FLOOD IPV6]" -j D ROP
    ip6tables -A ICMPFLOODIPV6 -j ACCEPT

    #Anti ICMPFLOOD
    iptables -A INPUT -p icmp --icmp-type 8  -m conntrack --ctstate NEW -j ICMPF LOOD
    ip6tables -A INPUT -p ipv6-icmp --icmpv6-type 128 -j ICMPFLOODIPV6

    echo - DROP ICMP FLOOD : [OK]

    ############################################################################ ###

    #BLACKLIST IP RELOU
    for blacklist_ip in $BLACKLIST_IP
    do
        iptables -A INPUT -s $blacklist_ip -j LOG --log-prefix "[DROP - BLACKLIS T IP]"
        iptables -A INPUT -s $blacklist_ip -m comment --comment "[DROP - BLACKLI ST IP]" -j DROP
    done
    echo - DROP IP RELOU : [OK]

    #### LOOPBACK ####
    iptables -A INPUT -p all -i lo -j ACCEPT
    iptables -A OUTPUT -p all -o lo -j ACCEPT

    echo - ACCEPT LOOPBACK : [OK]

    #### ICMP V4 ####
    # Selon la RFC 792, tous les serveurs doivent répondre àu Type 0 (réponse ec ho)
    iptables -A INPUT -p icmp --icmp-type echo-request -i eth0 -m limit --limit 6/s -m conntrack --ctstate ESTABLISHED,RELATED -j ACCEPT
    iptables -A INPUT -p icmp --icmp-type 0  -m conntrack --ctstate NEW -j ACCEP T
    #Type 3 (destinataire inaccessible)
    iptables -A INPUT -p icmp --icmp-type 3  -m conntrack --ctstate NEW -j ACCEP T
    #Type 11 (temps dépassé)
    iptables -A INPUT -p icmp --icmp-type 11 -m conntrack --ctstate NEW -j ACCEP T

    echo - ACCEPT ICMP IPV4 : [OK]

    #### ICMP V6 ####
    # Permit needed ICMP packet types for IPv6 per RFC 4890.
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 1   -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 2   -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 3   -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 4   -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 133 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 134 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 135 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 136 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 137 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 141 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 142 -j ACCEPT
    ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 130 -j ACCEPT
    ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 131 -j ACCEPT
    ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 132 -j ACCEPT
    ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 143 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 148 -j ACCEPT
    ip6tables -A INPUT              -p ipv6-icmp --icmpv6-type 149 -j ACCEPT
    ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 151 -j ACCEPT
    ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 152 -j ACCEPT
    ip6tables -A INPUT -s fe80::/10 -p ipv6-icmp --icmpv6-type 153 -j ACCEPT

    echo - ACCEPT ICMP IPV6 : [OK]

    #### DNS ####
    for dns_ipv4 in $DNS_IPV4
    do
        iptables -A OUTPUT -p udp -s $VPS_IPV4 --dport 53 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
        iptables -A OUTPUT  -p udp  --dport 53 -m limit --limit 30/minute --limi t-burst 5 -j LOG --log-prefix "[DROP_OUTPUT - MAX DNS OUT]"
        iptables -A OUTPUT -p udp -s $VPS_IPV4 -d $dns_ipv4 --dport 53 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    done

    echo - ACCEPT DNS IPV4 : [OK]

    for dns_ipv6 in $DNS_IPV6
    do
       ip6tables -A OUTPUT -p udp -s $VPS_IPV6 -d $dns_ipv6 --dport 53 -m conntr ack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    done

    echo - ACCEPT DNS IPV6 : [OK]

    #### NTP ####
    for ntp in $NTP_DEBIAN
    do
        iptables -A OUTPUT -p udp -s $VPS_IPV4 -d $ntp --dport 123 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    done

    echo - ACCEPT NTP : [OK]

    #### SMTP ####
    iptables -A OUTPUT -p tcp -s $VPS_IPV4 --dport 25 -m conntrack --ctstate NEW ,ESTABLISHED,RELATED -j ACCEPT

    echo - ACCEPT SMTP : [OK]

    #### GLANCES  ####
    #iptables -A INPUT -p tcp -s $HOME -d $VPS_IPV4 --dport 61208 -m conntrack - -ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    #iptables -A INPUT -p tcp -s $HOME_AM -d $VPS_IPV4 --dport 61208 -m conntrac k --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT

    #echo - ACCEPT GLANCES : [OK]

    #### MIROIRS ###
    for lst_miroirs in $LST_MIROIRS
    do
        iptables -A OUTPUT -p tcp -s $VPS_IPV4 -d $lst_miroirs --dport 80 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    done

    echo - ACCEPT MIROIRS : [OK]

    #### SSH ####
    #iptables -A INPUT -p tcp -s $NET_ANYWHERE -d $VPS_IPV4 --dport 22 --syn -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    iptables -A INPUT -p tcp -s $NET_ANYWHERE -d $VPS_IPV4 --dport 22 --syn -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j SSHBRUTE

    echo - ACCEPT SSH : [OK]

    #### HTTP ####
    iptables -A INPUT -p tcp -s $NET_ANYWHERE -d $VPS_IPV4 -m multiport --sport 1024:65535 -m multiport --dport 80,443 -m conntrack --ctstate NEW,ESTABLISHED,RE LATED -j ACCEPT
    iptables -A OUTPUT -p tcp -s $VPS_IPV4 -d $NET_ANYWHERE -m multiport --sport 1024:65535 -m multiport --dport 80,443 -m conntrack --ctstate NEW,ESTABLISHED,R ELATED -j ACCEPT

    echo - ACCEPT HTTP : [OK]

    #### TEAMSPEAK ####

    iptables -A INPUT -p tcp -s $NET_ANYWHERE -d $VPS_IPV4 -m multiport --dport 2008,10011,30033,41144 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    iptables -A INPUT -p udp -s $NET_ANYWHERE -d $VPS_IPV4 -m multiport --dport 2010,9987 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    iptables -A OUTPUT -p udp -s $VPS_IPV4 -d $NET_ANYWHERE -m multiport --dport 2010:2110 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    iptables -A OUTPUT -p tcp -s $VPS_IPV4 -d $NET_ANYWHERE --dport 2008 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT

    echo - ACCEPT TEAMSPEAK : [OK]

    #### LOG ####
    iptables -A OUTPUT -p tcp -s $VPS_IPV4 -d $HOME --dport 1514 -m conntrack -- ctstate NEW,ESTABLISHED,RELATED -j ACCEPT

    echo - ACCEPT LOG : [OK]

    #### wordpress.org ####
    for lst_wordpress in $LST_WORDPRESS
    do
        iptables -A OUTPUT -p tcp -s $VPS_IPV4 -d $lst_wordpress -m multiport -- dport 80,443 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    done

    echo - ACCEPT WORDPRESS OUTPUT : [OK]

    ### check my website
    iptables -A OUTPUT -p tcp -s $VPS_IPV4 -d 37.187.92.143  --sport 80 -m conntrack --ctstate NEW,ESTABLISHED,RELATED -j ACCEPT
    echo - ACCEPT MONITORING CHECK MY WEBSITE : [OK]

    ### sonde ovh
    iptables -A INPUT -p icmp -s 5.39.111.9 -d $VPS_IPV4 -j ACCEPT

    echo - ACCEPT MONITORING OVH : [OK]

    ############################################################################ ###############################################

    #Ajout d'une chaine dédiée aux logs
    iptables -N DROP_INPUT
    iptables -A INPUT -j DROP_INPUT
    iptables -A DROP_INPUT -j LOG --log-prefix "[DROP_INPUT - firewall]"
    iptables -A DROP_INPUT -p all -s $NET_ANYWHERE -j REJECT
    iptables -A DROP_INPUT -j DROP

    iptables -N DROP_OUTPUT
    iptables -A OUTPUT -j DROP_OUTPUT
    iptables -A DROP_OUTPUT -j LOG --log-prefix "[DROP_OUTPUT - firewall]"
    iptables -A DROP_OUTPUT -p all -s $NET_ANYWHERE -j REJECT
    iptables -A DROP_OUTPUT -j DROP

    iptables -N DROP_FORWARD
    iptables -A FORWARD -j DROP_FORWARD
    iptables -A DROP_FORWARD -j LOG --log-prefix "[DROP_FORWARD - firewall] "
    iptables -A DROP_FORWARD -p all -s $NET_ANYWHERE -j REJECT
    iptables -A DROP_FORWARD -j DROP

;;
status)

    echo - Liste des regles :
    iptables -n -L

;;
stop)

    # Vidage des tables et des regles personnelles
    iptables -t filter -F
    iptables -t filter -X
    echo - Vidage des regles et des tables : [OK]

    iptables -P INPUT ACCEPT
    iptables -P FORWARD ACCEPT
    iptables -P OUTPUT ACCEPT
    echo - Autoriser toutes les connexions entrantes et sortantes : [OK]
;;
restart|reload)
        /etc/init.d/iptables stop
        /etc/init.d/iptables start
        ;;
*)
        echo $"Usage: $0 {start|stop|restart|reload|status}"
        exit 1

esac
exit 0
```

## Exemple de commandes pouvant vous faire gagner du temps

Lister toutes les règles ou pour chaque CHAIN :

```shell
iptables -L -n -v --line-numbers
iptables -L INPUT -n -v --line-numbers
iptables -L OUTPUT -n -v --line-numbers
iptables -L FORWARD -n -v --line-numbers
```

Explication :

* -L : Lister les règles
* -v : Mode verbeux :
    * nom de l'interface
    * options de la règle
* -n : Affcihe les IP et port en format numérique (sans DNS)
* --line-numbers : affiche un numéro de ligne pour chaque règle

Grâce au numéro de ligne on peut supprimer une règle rapidement :

```shell
iptables -D INPUT 42
```

Ou insérer une ligne entre deux lignes, par exemple rajouter en seconde ligne (la règle de la ligne 2 passera en ligne 3) :

```shell
iptables -I INPUT 2 -s 192.168.1.1 -j DROP
```