# DevOps Song

Brothers of the IT rejoice!  
Type, type, type with me  
Raise your `board and raise your voice!  
Sing, sing, sing with me  
Down and down into the deep  
Who knows what we'll find beneath?  
Playbooks, handlers, tasks and more  
Hidden in the kernel store  
Born underground, suckled from a line of code  
Raised in the dark, the safety of our office home  
Skin made of bits, copper in our bones  
To code and code makes us free  
Come on brothers sing with me!  
I am a dev and I'm codding a role  
Coddy coddy role, coddy coddy role  
I am an ops and I'm codding a role  
Coddy coddy role, codding a role  
The sunlight will not reach this low  
Deep, deep in the code  
Never seen the blue moon glow  
DevOps won't fly so high  
Fill a glass and down some coke!  
Stuff your bellies at the feast!  
Stumble home and refacto  
Living in our gitlab keep  
Born underground, grown inside a gaming womb  
The pc's our cradle; the laptop shall become our tomb  
Face us on the ctf; you will meet your doom  
We do not fear what lies beneath  
We can never code too deep  
I am a dev and I'm codding a role  
Coddy coddy role, coddy coddy role  
I am an ops and I'm codding a role  
Coddy coddy role, codding a role  
I am a dev and I'm codding a role  
Coddy coddy role, coddy coddy role  
I am an ops and I'm codding a role  
Coddy coddy role, codding a role  
Born underground, suckled from a line of code  
Raised in the dark, the safety of our office home  
Skin made of bits, copper in our bones  
To code and code makes us free  
Come on brothers sing with me!  
I am a dev and I'm codding a role  
Coddy coddy role, coddy coddy role  
I am an ops and I'm codding a role  
Coddy coddy role, codding a role  
