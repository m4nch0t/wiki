# Disaster recovery

## Réseau

### Lexique

* 802.1q rajout d'un tag vlan à une trame ethernet
* 802.1ad (QinQ) permet de rajouter n tag vlan dans une trame déjà taguée
* ECMP (equal cost multi-pathing), permet de router un flux sur différents liens de même coût réseau
* control plane, décision de routage/forwarding
* data plane, action de routage/forwarding
* VPLS (virtual private lan segment), technique de L2VPN permettant d'interconnecter plusieurs segments ethernet distant afin de créer un seul domaine de broadcast sur un réseau qui repose le plus souvent sur MPLS
  * point de vue CE (customer edge), le réseau de l'opérateur se comporte comme un switch. Les PE sont "reliés" par des PW (pseudo wires) en full-mesh (souvent via BGP), ils dupliquent le traffic BUM (Broadcast, Unknown Unicast, Multicast)
  * les paquets VPLS ont deux labels :
    * extérieur, utilisé pour la commutation entre les routeurs MPLS
    * intérieur, utilisé pour dissocier les instances VPLS. Il est possible de transporter plusieurs VLANs par instances VPLS
* MPLS (multi protocol label switching), technique permettant de transmettre des trames de n'importe quel protocole.
  * LDP ( label distribution protocol), se base sur OSPF pour choisir le meilleur chemin vers une destination
  * RSVP-TE (ressource reservation protocol traffic engineering), permet de gérer pus finement l'utilisation de chacun des liens. Grâce à FRR ( MPLS fast reroute), il permet de basculer en moins de 50 ms sur des chemins pré-calculés.
  * chaque paquet ethernet entrant dans un réseau MPLS se voit taggée avec deux labels
    * tunnel label, le label extérieur, utilisé pour le transfert dans le backbone
    * vc label, le label intérieur, utilisé pour indiquer l'interface de sortie du PE (provider edge), routeur connecté au client
  * la trame MPLS traverse le réseau en suivant la LSP (label switched path, chemin unidirectionnel), plus rapide que du routage traditionnel
* DIMA, durée d'interruption maximale acceptable, RTO en anglais, Recovery Time Objective
* PDMA, perte de donnée maximale acceptable, RDO en anglais, Recovery Point Objective
* RTT, distance aller/retour. Elle est de 1ms pour 100 kms de fibre

Le principal problème de VPLS est l'apprentissage des MAC par le *data plane* (en flood & learn) comme dans un switch traditionnel. Mais via duplication et transfert vers le premier PE. Solution en court de dev, E-VPN (ethernet over vpn), c'est le *control plane* qui apprend les MAC et utilisation de MP-BGP (multiprotocol bgp).

* VxLAN (virtual extensible LAN), est une encapsulation d'une trame de niveau 2 dans des fragments UDP de niveau 4 en utilisant un réseau IP de niveau 3 pour étendre les domaines de broadcast.
  * VNI (virtual network indentifier) permet la création de 16 millions de réseaux VxLAN.
  * VTEP (vxlan tunnel endpoint), interface IP permettant d'encapsuler et dé-encapsuler le segment UPD
  * un paquet est envoyé au VTEP participant au VxLAN d'un même VNI et étant connue comme portant l'adresse mac de destination
  * si l'adresse mac n'est pas connue, il y a duplication du traffic BUM sur l'ensemble des VTEP participant au réseau VxLAN

Méthode de déploiement VxLAN:

* multicast, chaque VNI constitue un groupe multicast et un *RendezVousPoint* duplique les paquets vers les VTEP en cas de BUM, c'est une gestion centralisée mais difficilement scalable.
* HER (Head End Replication), dans ce cas de figure chaque VTEP participant à un VxLAN doit connaître l'ensemble des VTEP de destination où les BUM doivent être transmis et répliqués. Peut impliquer un overhead CPU important en cas de fort traffic BUM, et une perte de paquet en cas de contention. Attention à la **roguer** VTEP, on ne peut pas savoir si une VTEP a légitimité à encapsuler une trame. C'est une gestion décentralisée.
* MP-BGP & E-VPN, on remplace le *RendezVousPoint* par un ou plusieurs *route reflector* qui occupera le rôle de *control plane*. Il maintient la table de routage des addresses MAC et les distribue via BGP aux switchs clients portant chacun une VTEP, tout en conservant HER, ils seront donc uniquement en *data plane*. Cette méthode introduit des mécanismes de NLRI
* NLRI (Network Layer Reachability Information), permet de limiter les floods de traffic BUM, de plus elle permet de sécuriser les VTEP en utilisant les mécanismes d'authentification de BGP
* LISP (Locator Identifier Separation Protocol), protocole de routage et virtualisation de réseaux. Le principe est un encapsulation IP sur IP où les addresses IP externes (RLOC, routing locator) indiquent le localisateur de la source de et la destination. L'idée est d'émuler le même LAN dans différents endroits où la VM peut migrer. En cas de migration, elle garde son IP, sa MAC, sa table de routage et sa table ARP. Chaque route par défaut de ces vms doit être le routeur LISP qui aura la même addresses MAC sur les différents sites, mais un RLOC différent. Un des avantages est de ne pas nécessiter une liaison L2 entre les DC

à regarder :

* FRR : <http://forums.juniper.net/t5/TheRoutingChurn/MPLS-LDP-and-IP-Fast-Re-route-Mechanisms/ba-p/284832>
* mpls vpls evpn: <https://www.ciscolive.com/online/connect/sessionDetail.ww?SESSION_ID=78610>
* learning mac vxlan : <http://www.bortzmeyer.org/6820.html>
* MP-BGP EVPN <http://blogs.cisco.com/perspectives/a-summary-of-cisco-vxlan-control-planes-multicast-unicast-mp-bgp-evpn-2>
* LISP :
  <http://www-phare.lip6.fr/~secci/papers/PhSeSaIa-NETMAG14.pdf>
  <https://github.com/lip6-lisp>
  <http://www.cisco.com/c/en/us/solutions/enterprise/data-center-designs-data-center-interconnect/index.html>
  <http://www.cisco.com/c/dam/en/us/products/collateral/ios-nx-os-software/locator-id-separation-protocol-lisp/lisp-vm_mobility_wp.pdf>
  <http://www.cisco.com/c/en/us/td/docs/solutions/Enterprise/Data_Center/DCI/4-0/EMC/mobdisasterrecapps.html#wp417459>
  <https://www-phare.lip6.fr/~secci/papers/RaCoPhSeCiGaPu-TNSM14.pdf>
  <https://wiki.opendaylight.org/view/OpenDaylight_Lisp_Flow_Mapping:Architecture>

### Se documenter sur

* genève
* LISP

## Stockage

### Outils

* VPLEX d'EMC, il virtualise complètement le stockage effectif. Il peut être réparti sur différentes baies ou sites :
  * vplex metro : RTT max 5ms
  * vplex geo : RTT max 50ms
* GlusterFS, système de fichier libre mais propriété de Red Hat. Il permet de distribuer des volumes sur des bricks (serveurs) et de spécifier un nombre de répliques. Il n'y a pas de master, lors d'une écriture c'est le client qui va pousser les données sur les différents bricks (réplicas). Il existe des mécanismes de réplication intersites basés sur une relation maitre-esclave (site principal - site secondaire).
* HDFS, hadoop file system.
  * Le client crée un fichier temporaire local jusqu'à atteindre la taille d'un bloc (vue HDFS)
  * une fois plein le fichier est créé et écrit sur le datanode désigné par le namenode
  * les replicas sont transmis aux datanodes suivants.

## Base de données

* oracle dataguard
* Cassandra, système de gestion de base de donnée distribuée. Écrit en java, en cours de réécriture C++ sous le nom de ScyllaDB. Est est scallable, résiliente est tolérant à la panne.
<https://www.miscmag.com/references-de-larticle-retour-dexperience-sur-cassandra-publie-dans-misc-n84/>

## Services

### HaProxy

VRRP : <https://www.ietf.org/rfc/rfc2338.txt>
Keepalived : <http://keepalived.org/>
<https://github.com/acassen/keepalived/blob/master/doc/samples/keepalived.conf.vrrp.localcheck>
haproxy : <http://cbonte.github.io/haproxy-dconv/configuration-1.6.html#5.2-backup>
