# vmware hardenning

## Hardenning VM

Ce script powershell date de vSphere 5.1 mais peut servir de base pour les dernières versions du *hardenning guide* vmware.  
Il permet de modifier toutes les machines virtuelles en une éxécution, pour que ces modifications soient prises en compte, il faut arrêter la vm complètement.La redémarrer ne suffit pas car les paramètres se chargent au moment de la création du process de la vm sur l'ESXi.
Tout d'abord, nous devons nous connecter au vCenter

    Connect-VIServer -Server @IP


Puis lancer le script comme ceci:

    Get-Cluster cluster-name | Get-VM | .\hardenningVM51.ps1

```powershell
$SecurityOptions = @{
	"RemoteDisplay.maxConnections"="1";
	"tools.setInfo.sizeLimit"="1048576";
	"floppyX.present"="false";
	"ideX:Y.present"="false";
	"parallelX.present"="false";
	"serialX.present"="false";
	"usb.present"="false";
	"isolation.device.connectable.disable"="true";
	"isolation.device.edit.disable"="true";
	"isolation.tools.copy.disable"="true";
	"isolation.tools.dnd.disable"="true";
	"isolation.tools.setGUIOptions.enable"="false";
	"isolation.tools.paste.disable"="true";
	"isolation.tools.hgfsServerSet.disable"="true";
	"isolation.monitor.control.disable"="true";
	"isolation.tools.ghi.autologon.disable"="true";
	"isolation.bios.bbs.disable"="true";
	"isolation.tools.getCreds.disable"="true";
	"isolation.tools.ghi.launchmenu.change"="true";
	"isolation.tools.memSchedFakeSampleStats.disable"="true";
	"isolation.tools.ghi.protocolhandler.info.disable"="true";
	"isolation.ghi.host.shellAction.disable"="true";
	"isolation.tools.dispTopoRequest.disable"="true";
	"isolation.tools.trashFolderState.disable"="true";
	"isolation.tools.ghi.trayicon.disable"="true";
	"isolation.tools.unity.disable"="true";
	"isolation.tools.unityInterlockOperation.disable"="true";
	"isolation.tools.unity.taskbar.disable"="true";
	"isolation.tools.unityActive.disable"="true";
	"isolation.tools.unity.windowContents.disable"="true";
	"isolation.tools.unity.push.update.disable"="true";
	"isolation.tools.vmxDnDVersionGet.disable"="true";
	"isolation.tools.guestDnDVersionSet.disable"="true";
	"isolation.tools.diskShrink.disable"="true";
	"isolation.tools.diskWiper.disable"="true";
	"isolation.tools.autoInstall.disable"="true";
	"isolation.tools.vixMessage.disable"="true";
	"tools.guestlib.enableHostInfo"="false";
	"log.rotateSize"="100000";
	"log.keepOld"="10"
}

$SecurityConfigurationSpec = New-Object VMware.Vim.VirtualMachineConfigSpec

Foreach ($SecurityOption in $SecurityOptions.GetEnumerator()) {
    $SecurityOptionValue = New-Object VMware.Vim.optionvalue
    $SecurityOptionValue.Key = $SecurityOption.Key
    $SecurityOptionValue.Value = $SecurityOption.Value
    $SecurityConfigurationSpec.extraconfig +=  $SecurityOptionValue
}

# Faisons une recherche pour trouver les vms qui ne sont pas des templates...
$VMs2Update = Get-View -ViewType VirtualMachine -Property Name -Filter @{"Config.Template"="false"}

# Effectuons les modifications sur celles-ci
foreach($VM in $VMs2Update){
    $VM.ReconfigVM_Task($SecurityConfigurationSpec)
    Write  "$($VM.Name) - changed"
}
```
