# Commandes Powershell VMWare

## Lister les machines virtuelles du dossier PROD

    $VMs = Get-VM -Location (Get-Folder -Name "PROD")

## Faire un snapshot des machines virtuelles précédement trouvées

    New-Snapshot -Name "os ready" -Description "ready to install application" -Memory -Quiesce -VM $vms

## Restaurer les snapshots

    foreach ($vm in $vms) {Set-VM -VM $vm -Snapshot (Get-Snapshot -VM $vm -Name "ready to install application") -Confirm:$false}

## Supprimer en boucle les snapshots

    foreach ($vm in $vms) {Remove-Snapshot -Snapshot (Get-Snapshot -VM $vm -Name "ready to install application") -Confirm:$false}

## Supprimer les snapshot pour les machines virtuelles dont le nom commence par debian-*

    get-snapshot -name Demo -VM debian-* | remove-snapshot

## Supprimer tous les snapshot

    get-snapshot -vm * | remove-snapshot –whatif

## Voir les snapshots de la machine virtuelle debian01

    Get-VM debian01 | Get-Snapshot | Select-Object -Property Name, Description, Created, SizeMB | Format-List *
