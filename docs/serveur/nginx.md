# Gestion d'un serveur Web ou reverse proxy NGINX

## Supprimer le cache NGINX

```bash
find /var/cache/nginx/ -type f -exec rm -f {} \;
```

## Tester son fichier de configuration

```bash
nginx -t
nginx -t -c /etc/nginx/conf/nginx.conf
```

## Sécurisation

### ServerTokens

Comme sur un serveur Apache, cette directive permet de contrôler le contenu de l'en-tête Server inclus dans la réponse envoyée au client : cet en-tête peut contenir le type de système d'exploitation du serveur, ainsi que des informations à propos des modules compilés avec le serveur.
A modifier dans le fichier **/etc/nginx/nginx.conf** :

```nginx
server_tokens off;
```

### Limitons les connexions maximum par IP

```nginx
limit_conn_zone $binary_remote_addr zone=limit_per_ip:10m;
limit_conn limit_per_ip 20;
```

Ici nous limitons à 20 connexions par IP sur une période de 10 minutes.

### Limitons le nombre de requêtes maximum par IP

```nginx
limit_req_zone $binary_remote_addr zone=allips:10m rate=200r/s;
limit_req zone=allips burst=200 nodelay;
```

Ici nous limitons à 200 requêtes par secondes par IP sur une période de 10 minutes.


### Augmenter la taille d'envoi

Pour augmenter la taille des envois qui peuvent transiter par NginX (reverse proxy devant un serveur de fichier par exemple), modifier le fichier */etc/nginx/nginx.conf* dans la balise **http**:

```
client_max_body_size 20M;
```
