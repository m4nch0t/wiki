# Bind DNS

## Tester la configuration

Tester le fichier de configuration:

    named-checkconf /etc/named.conf

Tester une zone en particulier:

    named-checkzone acme.fr /var/named/zone.acme.fr
