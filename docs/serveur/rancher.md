# Installation d'un serveur K8S sous Centos 7

## Pré-requis

Suppression d'un potentielle ancienne installation

```bash
yum remove docker docker-client docker-client-latest docker-common docker-latest docker-latest-logrotate docker-logrotate docker-engine
```

```bash
yum install -y yum-utils device-mapper-persistent-data lvm2
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install docker-ce docker-ce-cli containerd.io
systemctl start docker
```

Pour vérifier que docker fonctionne correctement:

```bash
docker run hello-world
```

Rajoutons notre utilisateur dans le groupe docker ainsi que dans le sudoers:

```bash
usermod -aG docker manchot
echo -e "manchot\tALL=(ALL:ALL) NOPASSWD: ALL" >>/etc/sudoers
```

Voici les quelques règles de pare-feu à ouvrir:

```bash
firewall-cmd --zone=public --add-port=6443/tcp --permanent
firewall-cmd --reload
```

 Une fois votre serveur prêt, vous devez préparer votre workstation pour gérer votre serveur/cluster. Pour cela, installer le clients **rke**,**kubectl** et **helm**. Vous pouvez les trouver via votre distib, brew, github.

## Choix d'architecture

### Réseau

Un des choix possible pour la gestion du réseau dans Kubernetes est d'utiliser le CNI (Container Network Interface) et un plugin réseau. On se retrouve dans un véritable réseau virtualisé typé SDN (Software Defined Network). Dans ce cas on retrouve deux types de réseaux :

* underlay, réseau physique, traditionnel
* overlay, réseau virtualisé basé suivants les cas sur l'utilisation de tunnel, encapsulation des trames de niveau 2 dans des paquets de niveau 3, etc. Ce qui est assez impactant, par exemple augmentation du MTU, utilisation multicast...

Le plugin maintenant. Il y en a plusieurs, mais ceux proposés par défaut par la commande **rke config** sont ceux-ci:

* flannel: fonctionne en niveau 2, seul l’IPv4 est supporté, et il utilise par défaut le VXLAN pour transporter les paquets. Sur chaque noeud un bridge cni0 est chargé et possède une partie du sous-réseau du cluster. Utilise etcd pour stocker ses données.
* calico, fonctionne en niveau 2, supporte IPv4 et IPv6, kube-proxy est utilisé pour gérer les règles de filtrage (via netfilter). Sur chaque noeud on retrouvera un agent **felix** qui va servir de point de sortie. Un agent **bird**, routeur dynamique et distribué qui utilise le protocole BGP. Calico est simple, et supporte les networkPolicies de Kubernetes, son fichier de configuration est présent ici: /etc/calico/calicoctl.cfg (écriture yaml). Utilise etcd pour stocker ses données. On peut simplement déployer plusieurs réseau pour cloisonner les pods.
* weave, fonctionne en niveau 2, supporte IPv4 et IPv6. La gestion est dévolue à deux container : weave et weave-npc(Network Policy Controller). Le conteneur weave gère tout le fonctionnement de weave, et le conteneur weave-npc s’occupe uniquement de la partie filtrage mis en place avec les NetworkPolicies de Kubernetes. On retrouvera un bridge sur chaque host, et transport grâce à VXLAN.
* canal, plugin en désuétude qui rassemblait flannel et calico.

## Installation de votre server

Maintenant, vous avez le choix d'utiliser la commande **rke config** ou utiliser directement un fichier yaml. Lorsque vous avez terminé, il vous suffit de lancer la commande suivante pour l'installation de votre serveur.

```bash
rke up
```

Une fois que vos noeuds sont installés, un nouveau fichier est créé (kube_config_cluster.yml). Vous pouvez maintenant lancer vos premières commandes, avec au préalable configuré votre environnement shell :

```bash
export KUBECONFIG=kube_config_cluster.yml
kubectl version

Client Version: version.Info{Major:"1", Minor:"14", GitVersion:"v1.14.1", GitCommit:"b7394102d6ef778017f2ca4046abbaa23b88c290", GitTreeState:"clean", BuildDate:"2019-04-19T22:12:47Z", GoVersion:"go1.12.4", Compiler:"gc", Platform:"darwin/amd64"}
Server Version: version.Info{Major:"1", Minor:"13", GitVersion:"v1.13.5", GitCommit:"2166946f41b36dea2c4626f90a77706f426cdea2", GitTreeState:"clean", BuildDate:"2019-03-25T15:19:22Z", GoVersion:"go1.11.5", Compiler:"gc", Platform:"linux/amd64"}

kubectl get nodes
NAME              STATUS   ROLES                      AGE     VERSION
192.168.189.167   Ready    controlplane,etcd,worker   4m40s   v1.13.5

kubectl get cs
NAME                 STATUS    MESSAGE              ERROR
scheduler            Healthy   ok
controller-manager   Healthy   ok
etcd-0               Healthy   {"health": "true"}
```

## Installation de rancher

Tout d'abord, il vous est nécessaire de configurer helm avec tiller.

Helm sert à gérer les charts Kubernetes, qui sont des packages de ressources Kubernetes pré-configurés.
Un chart est une description détaillée de comment doit être configurée, déployée et mise à jour.

```bash
kubectl -n kube-system create serviceaccount tiller

serviceaccount/tiller created

kubectl create clusterrolebinding tiller --clusterrole=cluster-admin --serviceaccount=kube-system:tiller
clusterrolebinding.rbac.authorization.k8s.io/tiller created

helm init --service-account tiller
$HELM_HOME has been configured at /Users/manchot/.helm.

Tiller (the Helm server-side component) has been installed into your Kubernetes Cluster.

Please note: by default, Tiller is deployed with an insecure 'allow unauthenticated users' policy.
To prevent this, run `helm init` with the --tiller-tls-verify flag.
For more information on securing your installation see: https://docs.helm.sh/using_helm/#securing-your-helm-installation
Happy Helming!

kubectl -n kube-system  rollout status deploy/tiller-deploy

deployment "tiller-deploy" successfully rolled out

helm version
Client: &version.Version{SemVer:"v2.13.1", GitCommit:"618447cbf203d147601b4b9bd7f8c37a5d39fbb4", GitTreeState:"clean"}
Server: &version.Version{SemVer:"v2.13.1", GitCommit:"618447cbf203d147601b4b9bd7f8c37a5d39fbb4", GitTreeState:"clean"}
```
