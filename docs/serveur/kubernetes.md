# Installation et utilisation de Kubernetes

## Lexique

* **master** est une machine qui va orchestrer votre infrastructure
* **node** est une vm ou un serveur physique qui va héberger votre workload
* **pod** est une vue d'une application, il peut contenir plusieurs containers
* **CNI** Container Network Interface, il défini comment les différents nodes et leur workloads devront communiquer. Voir la partie réseau.

## Commandes de base

* **kubectl describe** : affiche la configuration précise de la ressource
* **kubectl logs** : affiche les logs
* **kubectl exec** : exécute une commande sur un conteneur ou POD
* **kubectl exec -ti nom_du_conteneur bash** : permet de se connecter en bash sur le conteneur
* **kubectl cluster-info** : donne les détails du cluster, son état de santé
* **kubectl get** : liste les ressources
* **kubectl get nodes** : permet de voir les nodes présents dans le cluster
* **kubectl get pods** : permet de voir l'état des pods
* **kubectl expose deployment nom_du_pod --port=80 --type=NodePort** : une commande qui va simplement exposer un pod avec un port spécifique (ici 80) à un port dynamique interne

## Cheatsheet

<https://kubernetes.io/docs/reference/kubectl/cheatsheet/>


## Pré-requis

Activer au besoin le routage sur votre machine :

```shell
echo 1 > /proc/sys/net/ipv4/ip_forward
### ou ###
echo "net.ipv4.ip_forward = 1" > /etc/sysctl.d/net-forward.conf
echo "net.ipv6.conf.all.forwarding = 1" > /etc/sysctl.d/net-forward.conf
```

Activer le module noyau **netfilter bridge** s'il ne l'est pas déjà

```shell
modprobe br_netfilter
echo br_netfilter > /etc/modules
echo "net.bridge.bridge-nf-call-ip6tables = 1" > /etc/sysctl.d/k8s.conf
echo "net.bridge.bridge-nf-call-iptables = 1" >> /etc/sysctl.d/k8s.conf
sysctl --system
```

Activer le module noyau **overlay** pour fournir *overlayfs*, gestion de la couche/layer au déssus d'une image de container.

```shell
modprobe overlay
echo overlay > /etc/modules
```

Désactivation du swapp, c'est une bonne pratique :

```shell
swappoff -a
sed -e '/swap/ s/^\#\*/\#/' -i /etc/fstab
```

## Container Runtime Interface

Une alternative à Docker pour l'éxécution des containers est le couple runc/CRI-O. Docker étant plus une trousse à outils complète, on pourrait réserver Docker plus à un usage Desktop/Workstation.
On obtiens ainsi un runtime léger (runc ou containerd), un CRI (CRI-O) capable de faire le lien avec kubelet.

```shell
echo 'deb http://deb.debian.org/debian buster-backports main' > /etc/apt/sources.list.d/backports.list
apt update
apt install -y libseccomp2 || apt update -y libseccomp2
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable.list
echo "deb https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable:/cri-o:/$VERSION/$OS/ /" > /etc/apt/sources.list.d/devel:kubic:libcontainers:stable:cri-o:$VERSION.list
curl -L https://download.opensuse.org/repositories/devel:kubic:libcontainers:stable:cri-o:$VERSION/$OS/Release.key | apt-key add -
curl -L https://download.opensuse.org/repositories/devel:/kubic:/libcontainers:/stable/$OS/Release.key | apt-key add -
apt update
apt install cri-o cri-o-runc
systemctl enable --nom crio
```

### Réseau

Un des choix possible pour la gestion du réseau dans Kubernetes est d'utiliser le CNI (Container Network Interface) et un plugin réseau. On se retrouve dans un véritable réseau virtualisé typé SDN (Software Defined Network). Dans ce cas on retrouve deux types de réseaux :

* underlay, réseau physique, traditionnel
* overlay, réseau virtualisé basé suivants les cas sur l'utilisation de tunnel, encapsulation des trames de niveau 2 dans des paquets de niveau 3, etc. Ce qui est assez impactant, par exemple augmentation du MTU, utilisation multicast...

Le plugin maintenant. Il y en a plusieurs, mais ceux proposés par défaut par la commande **rke config** sont ceux-ci:

* flannel, fonctionne en niveau 2, seul l’IPv4 est supporté, et il utilise par défaut le VXLAN pour transporter les paquets. Sur chaque noeud un bridge cni0 est chargé et possède une partie du sous-réseau du cluster. Utilise etcd pour stocker ses données.
* calico, fonctionne en niveau 2, supporte IPv4 et IPv6, kube-proxy est utilisé pour gérer les règles de filtrage (via netfilter). Sur chaque noeud on retrouvera un agent **felix** qui va servir de point de sortie. Un agent **bird**, routeur dynamique et distribué qui utilise le protocole BGP. Calico est simple, et supporte les networkPolicies de Kubernetes, son fichier de configuration est présent ici: /etc/calico/calicoctl.cfg (écriture yaml). Utilise etcd pour stocker ses données. On peut simplement déployer plusieurs réseau pour cloisonner les pods.
* weave, fonctionne en niveau 2, supporte IPv4 et IPv6. La gestion est dévolue à deux container : weave et weave-npc(Network Policy Controller). Le conteneur weave gère tout le fonctionnement de weave, et le conteneur weave-npc s’occupe uniquement de la partie filtrage mis en place avec les NetworkPolicies de Kubernetes. On retrouvera un bridge sur chaque host, et transport grâce à VXLAN.
* canal, plugin en désuétude qui rassemblait flannel et calico.

Ex d'une activation de weave:

```bash
master $ cat weave-kube
apiVersion: v1
kind: List
items:
  - apiVersion: v1
    kind: ServiceAccount
    metadata:
      name: weave-net
      annotations:
        cloud.weave.works/launcher-info: |-
          {
            "original-request": {
              "url": "/k8s/v1.8/net.yaml?k8s-version=v1.10.0",
              "date": "Wed Mar 28 2018 10:24:34 GMT+0000 (UTC)"
            },
            "email-address": "support@weave.works"
          }
      labels:
        name: weave-net
      namespace: kube-system
  - apiVersion: rbac.authorization.k8s.io/v1beta1
    kind: ClusterRole
    metadata:
      name: weave-net
      annotations:
        cloud.weave.works/launcher-info: |-
          {
            "original-request": {
              "url": "/k8s/v1.8/net.yaml?k8s-version=v1.10.0",
              "date": "Wed Mar 28 2018 10:24:34 GMT+0000 (UTC)"
            },
            "email-address": "support@weave.works"
          }
      labels:
        name: weave-net
      namespace: kube-system
    rules:
      - apiGroups:
          - ''
        resources:
          - pods
          - namespaces
          - nodes
        verbs:
          - get
          - list
          - watch
      - apiGroups:
          - networking.k8s.io
        resources:
          - networkpolicies
        verbs:
          - get
          - list
          - watch
  - apiVersion: rbac.authorization.k8s.io/v1beta1
    kind: ClusterRoleBinding
    metadata:
      name: weave-net
      annotations:
        cloud.weave.works/launcher-info: |-
          {
            "original-request": {
              "url": "/k8s/v1.8/net.yaml?k8s-version=v1.10.0",
              "date": "Wed Mar 28 2018 10:24:34 GMT+0000 (UTC)"
            },
            "email-address": "support@weave.works"
          }
      labels:
        name: weave-net
      namespace: kube-system
    roleRef:
      kind: ClusterRole
      name: weave-net
      apiGroup: rbac.authorization.k8s.io
    subjects:
      - kind: ServiceAccount
        name: weave-net
        namespace: kube-system
  - apiVersion: rbac.authorization.k8s.io/v1beta1
    kind: Role
    metadata:
      name: weave-net
      annotations:
        cloud.weave.works/launcher-info: |-
          {
            "original-request": {
              "url": "/k8s/v1.8/net.yaml?k8s-version=v1.10.0",
              "date": "Wed Mar 28 2018 10:24:34 GMT+0000 (UTC)"
            },
            "email-address": "support@weave.works"
          }
      labels:
        name: weave-net
      namespace: kube-system
    rules:
      - apiGroups:
          - ''
        resourceNames:
          - weave-net
        resources:
          - configmaps
        verbs:
          - get
          - update
      - apiGroups:
          - ''
        resources:
          - configmaps
        verbs:
          - create
  - apiVersion: rbac.authorization.k8s.io/v1beta1
    kind: RoleBinding
    metadata:
      name: weave-net
      annotations:
        cloud.weave.works/launcher-info: |-
          {
            "original-request": {
              "url": "/k8s/v1.8/net.yaml?k8s-version=v1.10.0",
              "date": "Wed Mar 28 2018 10:24:34 GMT+0000 (UTC)"
            },
            "email-address": "support@weave.works"
          }
      labels:
        name: weave-net
      namespace: kube-system
    roleRef:
      kind: Role
      name: weave-net
      apiGroup: rbac.authorization.k8s.io
    subjects:
      - kind: ServiceAccount
        name: weave-net
        namespace: kube-system
  - apiVersion: extensions/v1beta1
    kind: DaemonSet
    metadata:
      name: weave-net
      annotations:
        cloud.weave.works/launcher-info: |-
          {
            "original-request": {
              "url": "/k8s/v1.8/net.yaml?k8s-version=v1.10.0",
              "date": "Wed Mar 28 2018 10:24:34 GMT+0000 (UTC)"
            },
            "email-address": "support@weave.works"
          }
      labels:
        name: weave-net
      namespace: kube-system
    spec:
      minReadySeconds: 5
      template:
        metadata:
          labels:
            name: weave-net
        spec:
          containers:
            - name: weave
              command:
                - /home/weave/launch.sh
              env:
                - name: HOSTNAME
                  valueFrom:
                    fieldRef:
                      apiVersion: v1
                      fieldPath: spec.nodeName
              image: 'weaveworks/weave-kube:2.2.1'
              livenessProbe:
                httpGet:
                  host: 127.0.0.1
                  path: /status
                  port: 6784
                initialDelaySeconds: 30
              resources:
                requests:
                  cpu: 10m
              securityContext:
                privileged: true
              volumeMounts:
                - name: weavedb
                  mountPath: /weavedb
                - name: cni-bin
                  mountPath: /host/opt
                - name: cni-bin2
                  mountPath: /host/home
                - name: cni-conf
                  mountPath: /host/etc
                - name: dbus
                  mountPath: /host/var/lib/dbus
                - name: lib-modules
                  mountPath: /lib/modules
                - name: xtables-lock
                  mountPath: /run/xtables.lock
            - name: weave-npc
              args: []
              env:
                - name: HOSTNAME
                  valueFrom:
                    fieldRef:
                      apiVersion: v1
                      fieldPath: spec.nodeName
              image: 'weaveworks/weave-npc:2.2.1'
              resources:
                requests:
                  cpu: 10m
              securityContext:
                privileged: true
              volumeMounts:
                - name: xtables-lock
                  mountPath: /run/xtables.lock
          hostNetwork: true
          hostPID: true
          restartPolicy: Always
          securityContext:
            seLinuxOptions: {}
          serviceAccountName: weave-net
          tolerations:
            - effect: NoSchedule
              operator: Exists
          volumes:
            - name: weavedb
              hostPath:
                path: /var/lib/weave
            - name: cni-bin
              hostPath:
                path: /opt
            - name: cni-bin2
              hostPath:
                path: /home
            - name: cni-conf
              hostPath:
                path: /etc
            - name: dbus
              hostPath:
                path: /var/lib/dbus
            - name: lib-modules
              hostPath:
                path: /lib/modules
            - name: xtables-lock
              hostPath:
                path: /run/xtables.lock
      updateStrategy:
        type: RollingUpdate

master $ kubectl apply -f weave-kube
master $ kubectl get pod -n kube-system
NAME                             READY     STATUS    RESTARTS   AGE
...
weave-net-46lp2                  2/2       Running   0          2m
weave-net-k8cjs                  2/2       Running   0          2m
```

On peut voir nos deux pods qui vont gérer le réseau sur chaque node.

Pour flannel, c'est aussi un yaml disponible sur github:

```bash
kubectl apply -f https://github.com/coreos/flannel/raw/master/Documentation/kube-flannel.yml
```

Ce ne sont ici que des exemple de déploiement de CNI, je souhaitais simplement montrer les méthodes.

### Ingress Controller

* **Kubernetes Ingress Controller**: basé sur nginx
* **NGINX Ingress Controller**: plus rapide que celui de Kubernetes.
* **Kong Ingress**: ne fonctionne que sur un seul namespace, grande isolation
* **Traefik**: multiple load balancing algorithms, web UI, metrics export, REST API, canary releases
* **HAProxy Ingress**: DNS-based service discovery, configuration dynamique au travers de l'API
* **Istio Ingress**: projet porté par IBM, Google, et Lyft, gère le traffic entrant (comme tous les Ingress controller) ainsi que le traffic à l'intérieur du cluster

Un tableau récapitulatif:

![ingress_compare](img/ingress_compare.png) ingress_compare.png

## Installation sur un VPS OVH

### Désactivation du SWAP

Tout d'abord, nous devons désactiver le swap. Dans mon cas le swap est sur un lv de 2Go que je vais réattribuer à la partition  var :

```bash
swapoff
vremove /dev/mapper/vg0-swap
lvextend /dev/mapper/vg0-var -L +2G
xfs_growfs /dev/mapper/vg0-var
```

### Installation de Docker

Installation commune master & node

```bash
apt-get install -y apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/$(. /etc/os-release; echo "$ID")/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian stretch stable"
apt-get update
apt-get install -y docker-ce
```

### Installation de Kubernetes

Installation sur le master

```bash
 curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
add-apt-repository "deb http://apt.kubernetes.io/ kubernetes-xenial main"
apt-get update
apt-get install -y kubelet kubeadm kubectl
```

Mon master est un VPS SSD 1 vCPU et 2 Go de ram, ce qui pose problème à l'installation, car le minimum de CPU requis est 2, j'ignore donc ce test.

```bash
kubeadm init --pod-network-cidr=10.244.0.0/16 --ignore-preflight-errors=NumCPU
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
```

Les trois dernière commandes vous permettent d'utilisaer la commande kubectl en tant qu'utilisateur non-root.

Vous devez noter le retour qui commence par kubeadm join, il contient votre token d'authentification pour vos nodes:

```bash
kubeadm join 164.132.41.191:6443 --token pcbe39.4id63v262if38iqc --discovery-token-ca-cert-hash sha256:8823486288b56bbe8bb3c9a6c36b75c47ed58ad0486629cfa7aec9acfd1b37f6
```

Vérification :

```bash
kubectl get pods --all-namespaces
```

#### Rajout d'un node

A éxécuter sur le node:

```bash
scp  root@master:/etc/kubernetes/admin.conf $HOME/.kube/config
mkdir -p $HOME/.kube
sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
sudo chown $(id -u):$(id -g) $HOME/.kube/config
sudo kubeadm join 164.132.41.191:6443 --token pcbe39.4id63v262if38iqc --discovery-token-ca-cert-hash sha256:8823486288b56bbe8bb3c9a6c36b75c47ed58ad0486629cfa7aec9acfd1b37f6
```

Vérification sur le master :

```bash
kubectl get nodes
NAME                 STATUS   ROLES    AGE     VERSION
node-01.vmanchot.net   Ready    master   21h     v1.13.1
nopde-02.vmanchot.net   Ready    <none>   3m50s   v1.13.1
```

#### Gestion du cluster

Nous pouvons avoir les infos basique de notre cluster, à savoir notre master et forwarder DNS que nous verrons plus tard.

```bash
kubectl cluster-info
Kubernetes master is running at https://w.x.y.z:6443
KubeDNS is running at https://w.x.y.z:6443/api/v1/namespaces/kube-system/services/kube-dns:dns/proxy

To further debug and diagnose cluster problems, use 'kubectl cluster-info dump'.
```

La version du cluster :

```bash
kubectl version
Client Version: version.Info{Major:"1", Minor:"13", GitVersion:"v1.13.1", GitCommit:"eec55b9ba98609a46fee712359c7b5b365bdd920", GitTreeState:"clean", BuildDate:"2018-12-13T10:39:04Z", GoVersion:"go1.11.2", Compiler:"gc", Platform:"linux/amd64"}
Server Version: version.Info{Major:"1", Minor:"13", GitVersion:"v1.13.1", GitCommit:"eec55b9ba98609a46fee712359c7b5b365bdd920", GitTreeState:"clean", BuildDate:"2018-12-13T10:31:33Z", GoVersion:"go1.11.2", Compiler:"gc", Platform:"linux/amd64"}
```

#### Installation du dashboard Kubernetes

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/dashboard/master/src/deploy/recommended/kubernetes-dashboard.yaml
```

Création des utilisateurs et groupes :
1.**admin-user.yaml**

```yml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
```

2.**admin-role.yaml**

```yml
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
```

3.Création de l'utilisateur admin et de son role :

```bash
kubectl create -f admin-user.yaml
kubectl apply -f admin-role.yaml
```

### Connexion au dashboard

#### Récupération du token

```bash
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')
```

#### Lancement du proxy

Pour s'y connecter à distance, vous devez lancer le proxy sur le master et monter un tunnel SSH à partir de votre machine cliente et avec votre utilisateur :

```bash
kubectl proxy
ssh -L 8001:127.0.0.1:8001 manchot@node-01
```

Puis accedez à l'url avec votre navigateur préféré: <http://localhost:8001/api/v1/namespaces/kube-system/services/https:kubernetes-dashboard:/proxy/>

## Utilisation

### Vocabulaire

* Un **POD** est une vue logique représentant une application. Il représente un ou plusieur conteneur et leurs ressources partagées (volumes, shared ip, metadata, etc...). Chaque POD possède une adresse IP unique.
* Un **NODE** est une machine qui va héberger les **POD**. Ils sont managés par le **Master**. Chaque NODE exécute à minima :
* **Kubelet** : un service qui gère la communication entre le master et les nodes, ainsi que l'éxécution des PODs et conteneurs.
* **un environnement d'exécution** comme Docker qui est responsable de récupérer, et éxécuter les conteneurs.
* **Services**, c'est une politique d'accès à un ou plusieurs POD.

Elle peut être via :

* ClusterIP (defaut), on expose le service via une IP interne au cluster.
* NodePort, on expose le service sur le même port sur tous les nodes, que les pods soient présents ou non, et on nat les flux pour accéder aux pods.
* LoadBalancer, uniquement disponible chez les cloud providers ( Node Port)
* ExternalName, on expose le service via DNS (CNAME). Kubernetes v1.7 mini pour avoir kube-dns.
* **Ingress** est une règle qui permet de relier une URL à un Service
* **Ingress Controller** composant qui permet de faire le lien entre un reverse proxy et une règle Ingress.

### Déployons notre application

Une fois que notre cluster est en ligne, nous allons y déployer une application en container. Pour ce faire, nous devons créer une configuration **Deployment** qui va décrire à Kubernetes commet créer et maintenir à jour les instances de notre application. Une fois ce **Deployment** créé, le master va positionner nos instances dans différents nodes.

Le **Deployment Controller** va monitorer ces instances. Si un node est indisponible, il va redéployer l'instance sur un autre node.

Lançons nous :

```bash
kubectl run nginx --image=nginx --port=9000
```

Malheureusement ceci va échouer, cherchons maintenant pourquoi :

```bash
kubectl get pods
NAME                    READY   STATUS    RESTARTS   AGE
nginx-d89c7dfb5-jz299   0/1     Pending   0          96s
```

Notre pod reste en **pending**, continuons à creuser :

```bash
kubectl describe pod nginx-d89c7dfb5-jz299
Name:               nginx-d89c7dfb5-jz299
Namespace:          default
Priority:           0
PriorityClassName:  <none>
Node:               <none>
Labels:             pod-template-hash=d89c7dfb5
                    run=nginx
Annotations:        <none>
Status:             Pending
IP:
Controlled By:      ReplicaSet/nginx-d89c7dfb5
Containers:
  nginx:
    Image:        nginx
    Port:         9000/TCP
    Host Port:    0/TCP
    Environment:  <none>
    Mounts:
      /var/run/secrets/kubernetes.io/serviceaccount from default-token-96bz4 (ro)
Conditions:
  Type           Status
  PodScheduled   False
Volumes:
  default-token-96bz4:
    Type:        Secret (a volume populated by a Secret)
    SecretName:  default-token-96bz4
    Optional:    false
QoS Class:       BestEffort
Node-Selectors:  <none>
Tolerations:     node.kubernetes.io/not-ready:NoExecute for 300s
                 node.kubernetes.io/unreachable:NoExecute for 300s
Events:
  Type     Reason            Age                  From               Message
  ----     ------            ----                 ----               -------
  Warning  FailedScheduling  107s (x2 over 107s)  default-scheduler  0/2 nodes are available: 2 node(s) had taints that the pod didn't tolerate.
```

On voit dans le dernier event qu'aucun node n'est disponible car ils possèdent un **taint** qui bloque le déploiement. Déterminons ce qui bloque:

```bash
kubectl get nodes -o json | jq .items[].spec.taints
null
[
  {
    "effect": "NoSchedule",
    "key": "node.kubernetes.io/disk-pressure",
    "timeAdded": "2018-12-30T07:05:56Z"
  }
]
```

On voit rapidement qu'il y a un problème de place sur notre node: **node.kubernetes.io/disk-pressure**.

Regardons l'état de nos nodes :

```bash
kubectl describe node node-02.vmanchot.net
Name:               node-02.vmanchot.net
Roles:              <none>
Labels:             beta.kubernetes.io/arch=amd64
                    beta.kubernetes.io/os=linux
                    kubernetes.io/hostname=vps-2.vmanchot.net
Annotations:        kubeadm.alpha.kubernetes.io/cri-socket: /var/run/dockershim.sock
                    node.alpha.kubernetes.io/ttl: 0
                    volumes.kubernetes.io/controller-managed-attach-detach: true
CreationTimestamp:  Fri, 14 Dec 2018 21:12:26 +0100
Taints:             node.kubernetes.io/disk-pressure:NoSchedule
Unschedulable:      false
Conditions:
  Type             Status    LastHeartbeatTime                 LastTransitionTime                Reason                       Message
  ----             ------    -----------------                 ------------------                ------                       -------
  MemoryPressure   False     Tue, 15 Jan 2019 17:46:22 +0100   Sun, 30 Dec 2018 08:05:56 +0100   KubeletHasSufficientMemory   kubelet has sufficient memory available
  DiskPressure     True      Tue, 15 Jan 2019 17:46:22 +0100   Sun, 30 Dec 2018 08:05:56 +0100   KubeletHasDiskPressure       kubelet has disk pressure
  PIDPressure      False     Tue, 15 Jan 2019 17:46:22 +0100   Sun, 30 Dec 2018 08:05:56 +0100   KubeletHasSufficientPID      kubelet has sufficient PID available
  Ready            True      Tue, 15 Jan 2019 17:46:22 +0100   Sun, 30 Dec 2018 08:05:56 +0100   KubeletReady                 kubelet is posting ready status
  OutOfDisk        Unknown   Fri, 14 Dec 2018 21:12:26 +0100   Sun, 30 Dec 2018 07:59:47 +0100   NodeStatusNeverUpdated       Kubelet never posted node status.
Addresses:
  InternalIP:  164.132.41.188
  Hostname:    node-02.vmanchot.net
Capacity:
 cpu:                1
 ephemeral-storage:  520868Ki
 hugepages-2Mi:      0
 memory:             3952564Ki
 pods:               110
Allocatable:
 cpu:                1
 ephemeral-storage:  480031949
 hugepages-2Mi:      0
 memory:             3850164Ki
 pods:               110
System Info:
 Machine ID:                 04adee8f7b1d4f61946eda0f4c9ace17
 System UUID:                04ADEE8F-7B1D-4F61-946E-DA0F4C9ACE17
 Boot ID:                    60b18cf2-c397-4721-b763-1c467e15db1a
 Kernel Version:             4.9.0-8-amd64
 OS Image:                   Debian GNU/Linux 9 (stretch)
 Operating System:           linux
 Architecture:               amd64
 Container Runtime Version:  docker://18.9.0
 Kubelet Version:            v1.13.1
 Kube-Proxy Version:         v1.13.1
PodCIDR:                     10.244.1.0/24
Non-terminated Pods:         (0 in total)
  Namespace                  Name    CPU Requests  CPU Limits  Memory Requests  Memory Limits  AGE
  ---------                  ----    ------------  ----------  ---------------  -------------  ---
Allocated resources:
  (Total limits may be over 100 percent, i.e., overcommitted.)
  Resource           Requests  Limits
  --------           --------  ------
  cpu                0 (0%)    0 (0%)
  memory             0 (0%)    0 (0%)
  ephemeral-storage  0 (0%)    0 (0%)
Events:
  Type     Reason                Age                       From                         Message
  ----     ------                ----                      ----                         -------
  Warning  EvictionThresholdMet  3m50s (x275871 over 31d)  kubelet, vps-2.vmanchot.net  Attempting to reclaim ephemeral-storage
```

Effectivement nous avons un problème de place car la partition var était à moins de 10% d'espace libre et par défaut kubernetes stocke ses données dans son **ephemeral-storage** :

* /var/lib/kubelet/
* /var/log/pods
* /var/log/containers

Une fois la partition étendue, notre application démarre correctement.

```bash
kubectl get deployments
NAME    READY   UP-TO-DATE   AVAILABLE   AGE
nginx   1/1     1            1           4s
```

```bash
kubectl get pods
NAME                     READY   STATUS    RESTARTS   AGE
nginx-7cdbd8cdc9-mvk5v   1/1     Running   0          9s
```

Récupérons son IP et testons son accès réseau :

```bash
kubectl describe pod nginx-7cdbd8cdc9-mvk5v |grep IP
IP:                 10.244.1.5
```

```bash
ping 10.244.1.5
PING 10.244.1.5 (10.244.1.5) 56(84) bytes of data.
64 bytes from 10.244.1.5: icmp_seq=1 ttl=63 time=1.10 ms
```

```bash
curl 10.244.1.5
<!DOCTYPE html>
<html>
<head>
<title>Welcome to nginx!</title>
<style>
    body {
        width: 35em;
        margin: 0 auto;
        font-family: Tahoma, Verdana, Arial, sans-serif;
    }
</style>
</head>
<body>
<h1>Welcome to nginx!</h1>
<p>If you see this page, the nginx web server is successfully installed and
working. Further configuration is required.</p>

<p>For online documentation and support please refer to
<a href=http://nginx.org/>nginx.org</a>.<br/>
Commercial support is available at
<a href=http://nginx.com/>nginx.com</a>.</p>

<p><em>Thank you for using nginx.</em></p>
</body>
</html>
```

Maintenant rendons la accessible en utilisant les **service**

```bash
kubectl expose deployment/nginx --port=80
service/nginx exposed
```

Nous pouvons vérifier son activation :

```bash
kubectl get service
NAME         TYPE        CLUSTER-IP     EXTERNAL-IP   PORT(S)   AGE
kubernetes   ClusterIP   10.96.0.1      <none>        443/TCP   32d
nginx        ClusterIP   10.106.114.6   <none>        80/TCP    3m51s
```

Pas très propre si vous n'avez pas un reverse proxy en interne.

### Publication en externe via Ingress et Ingress controller

On va se baser sur le Ingress Controller de NGINX (<https://kubernetes.github.io/ingress-nginx/deploy/>   <https://github.com/kubernetes/ingress-nginx)>

```bash
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/provider/baremetal/service-nodeport.yaml
kubectl -n ingress-nginx get pod -o wide
kubectl get pods --all-namespaces -l app.kubernetes.io/name=ingress-nginx --watch

```

### Scale application

Pour voir l'état des déploiement en cours:

```bash
kubectl get deployments
```

Une petite explication des états :

* **DESIRED** montre le nombre de répliques configurées
* **CURRENT** montre le nombre de répliques démarrées
* **UP-TO-DATE** montre le nombre de répliques mises à jours pour correspondre avec le nombre configuré.
* **AVAILABLE** montre le nombre de répliques accessibles à l'utilisateur

Pour augmenter leur nombre à 4 par exemple :

```bash
kubectl scale deployments/test-app --replicas=4
```

Pour voir les pods déployé et leurs node d'affectation :

```bash
kubectl get pods -o wide
```

## Sécurité

Quelques bonnes pratiques:

* être sur de ses sources
* stocker ses secrets dans un vault
* scanner vos images et leurs dépendances
* cgroups, chaque container doit avoir des limites cpu/ram
* les namespace fournissent une isolation mais restent faillibles
* n'utiliser pas le compte root
* activer SELinux/AppArmor
* RBAC
* utiliser le mutual TLS

## Liens

Un IDE par Red Hat:

<https://developers.redhat.com/products/codeready-workspaces/overview>

### Razee

Automatiser et gérer le déploiement de ressources Kubernetes sur de multiples clusters, cloud providers. Ainsi que la visualisation des déploiements.

<https://github.com/razee-io/Razee/blob/master/README.md>

### Kubecost

Permet de vérifier ce que vous consommer réellement sur votre infra K8S.

<https://github.com/kubecost/cost-model>

## K3S

K3S est une implémentation allégée de Kubernetes aka K8S, en effet il fait table rase du legacy et se limite à un seul binaire.

Voici un exemple à deployer sur un raspi par exemple:

```bash
cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
curl -sfL https://get.k3s.io | sh -
systemctl status k3s
cat /var/lib/rancher/k3s/server/node-token

K1089729d4KAOIAHABbd29efc::node:1fcdc168812f3elkiozcad635c7b7a9b7
```

La commande **k3s** fait ainsi son apparition pour gérer votre node:

```bash
k3sup install --ip $SERVER --user $USER
k3sup join --ip $AGENT --server-pi $SERVER --user $USER
```

Installation d'un worker:

```bash
export K3S_URL="https://192.168.0.32:6443"
export K3S_TOKEN="K1089729d4ab5e51a44b1871768c7c04ad80bc6319d7bef5d94c7caaf9b0bd29efc::node:1fcdc14840494f3ebdcad635c7b7a9b7"
curl -sfL https://get.k3s.io | sh -
```

Ajout d'un worker déjà installé

```bash
k3s agent --server ${K3S_URL} --token ${K3S_TOKEN}
```

Vous pouvez vous baser aussi sur ce très bon travail pour votre lab:

<https://www.pixelpiloten.se/blog/kubernetes-running-in-5min/>
