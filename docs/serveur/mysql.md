# MySQL

Vous trouverez ici un ensemble de documentation pour gérer votre serveur MySQL.

## Tester son fichier de configuration

    mysqld --verbose --help 1>/dev/null
    mysqld --defaults-file=/root/test-my.cnf --verbose --help 1>/dev/null

## Reset root password

Que ce soit lors de la première installation ou après la perte du mot de passe principal de MySQL, il est nécessaire de pouvoir modifier le mot de passe administrateur (root) de MySQL.
Vous avez perdu le mot de passe root de MySQL ?

Pour pouvoir modifier le mot de passe root de MySQL, il faut pouvoir s'y connecter, Or, si vous n'avez pas le mot de passe root actuel, vous vous retrouvez alors dans une situation kafkaïenne. Si vous connaissez le mot de passe actuel de MySQL et que vous souhaitez juste changer le mot de passe root, vous pouvez sauter cette étape !

Heureusement, il est possible de se connecter à MySQL en échappant à la phase d'authentification (ce qui est fortement recommandé sur un serveur en production).

### Relancer le serveur sans authentification

Pour ce faire il convient d'arrêter le serveur MySQL :

```bash
/etc/init.d/mysql stop
```

Redémarrer MySQL en passant outre l'identification et en désactivant l'écoute du réseau (afin d'éviter d'être piraté à ce moment donné où MySQL est vulnérable) :

```bash
mysqld --skip-grant-tables --skip-networking &
```

### Réinitialiser le mot de passe

Une fois l'étape précédente réalisée ou si vous souhaitez modifier le mot de passe root (MySQL est installé par défaut sans mot de passe root), connectez-vous à la base de données système (mysql) de MySQL : # mysql mysql -u root
Puis, saisissez la commande suivante pour mettre à jour le mot de passe root (en remplaçant évidemment nouveaumotdepasse par le votre) :

```sql
UPDATE user SET password=PASSWORD('nouveaumotdepasse') WHERE user="root";
```

Si vous n'êtes pas passé par la première étape, il suffit de prendre en compte les changement en tapant la commande suivante : FLUSH PRIVILEGES;
Si par contre vous avez démarré MYSQL à la main en mode dégradé, il suffit de redémarrer le serveur :

```bash
/etc/init.d/mysql restart
```

### Sauvegarde de base de données sans fournir le mot de passe

```sql
mysql -u root

mysql> GRANT RELOAD ON *.* TO 'backup'@'localhost';
mysql> GRANT CREATE, INSERT, DROP ON mysql.ibbackup_binlog_marker TO 'backup'@'localhost';
mysql> GRANT CREATE, INSERT, DROP ON mysql.backup_progress TO 'backup'@'localhost';
mysql> GRANT CREATE, INSERT, SELECT, DROP ON mysql.backup_history TO 'backup'@'localhost';
mysql> GRANT REPLICATION CLIENT ON *.* TO 'backup'@'localhost';
#mysql> GRANT SUPER ON *.* TO 'backup'@'localhost';
mysql> GRANT SELECT, LOCK TABLES, SHOW VIEW, EVENT, TRIGGER ON *.* TO 'backup'@'localhost';
mysql> GRANT CREATE TEMPORARY TABLES ON mysql.* TO 'backup'@'localhost';
mysql> SET PASSWORD FOR 'backup'@'localhost' = PASSWORD('newpass');
mysql> FLUSH PRIVILEGES;
```

Il nous reste maintenant à l'implementer dans un job cron de l'utilisateur **backup**.

```bash
vi /home/backup/.my.cnf

[mysqldump]
user = backup
password = secret
```

Maintenant la sécurité minimum:

```bash
chmod 600 ~/.my.cnf
```

En tant que backup, éditons notre **crontab**

```bash
crontab -e

0 5 * * * mysqldump -u backup -h localhost --events --all-databases | gzip -9  > /sauvegarde/mysql/`date "+%d-%m"`-db.sql.gz
```
