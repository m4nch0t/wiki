# Red Hat Satellite

## Configuration via hammer

Création d'un lifecycle environment (LE):

    hammer lifecycle-environment create --description le-eng-rhel7-std-server --prior Library --name le-eng-rhel7-std-server

Création d'une content view (CV):

    hammer content-view create --description cv-rhel7-std-server --name cv-rhel7-std-server

List les ID des repository:

    hammer repository list --order id | awk -F '|' '{ print $1, $2 }'

Ajouter les repositories récupérés à l'étape précédente, dans la Content View:

    hammer content-view update --repository-ids 1,2 --name "cv-rhel7-std-server"

Publier une nouvelle version de la Content View:

    hammer content-view publish --name "cv-rhel7-std-server" --async

Après publication nous devons promouvoir la Content View dans notre Lifecycle Environment:

    hammer content-view version promote --content-view "cv-rhel7-std-server" --to-lifecycle-environment "le-eng-rhel7-std-server" --async

Création d'une Activation Key (AK) qui pourra être utilisée par nos hosts:

    hammer activation-key create --content-view "cv-rhel7-std-server" --lifecycle-environment "le-eng-rhel7-std-server" --name "ak-eng-rhel7-std-server"

Lister les subscriptions disponibles (celles chargées dans le Satellite à partir de son manifest):

    hammer subscription list | awk -F'|' '{ print $1, $3, $9, $10 }'
    --- ------------------------------------ ---------- ---------
    ID   NAME                                 QUANTITY   CONSUMED
    --- ------------------------------------ ---------- ---------
    1    Red Hat Satellite (Self-Supported)   1              0
    2    Red Hat Enterprise Linux             1              0
    --- ------------------------------------ ---------- ---------

Ajouter les subscriptions à l'Activation Key:

    hammer activation-key add-subscription --name "ak-eng-rhel7-std-server" --subscription-id 2

Créer un nouveau subnet pour y provisionner des hosts:

    hammer subnet create --name "sn-default-servers" --network "192.168.0.0" --mask "255.255.255.0" --gateway "192.168.0.1" --dns-primary "192.168.0.8" --domains example.com --locations loc-example --organizations org-example

Création d'une Compute Resource

username@domain et domain\username peuvent être utilisés.

```bash
hammer compute-resource create --caching-enabled 1 --datacenter "dc-example" --name "cr-vcenter" --password "password"  --provider "Vmware" --server "vcenter.example.com" --user 'vsphere.local\administrator' --locations "loc-example" --organizations "org-example"
hammer compute-resource create --caching-enabled 1 --datacenter "dc-example" --name
```

## Installation et configuration de virt-who

Installer virt-who package et ses dépendances:

```bash 
yum install -y virt-who
```

Créé une configuration virt-who pour votre vCenter

```bash
hammer virt-who-config create --name vcenter.example.com --organization org-example --interval 60 --filtering-mode none --hypervisor-id hostname --hypervisor-type esx --hypervisor-server vcenter.example.com --hypervisor-username 'administrator@vsphere.local' --hypervisor-password 'password' --satellite-url satellite.example.com
```

Déployer la configuration virt-who

```bash
hammer virt-who-config deploy --id 1
```

En cas de problèmes, virt-who peut être débuggé en arrêtant le service et en le relançant en mode debud en tâche de fond.

```bash
systemctl stop virt-who.service
virt-who -d -o
```

## Souscrire au Satellite

```bash
rpm -ivh http://satellite.example.com/pub/katello-ca-consumer-latest.noarch.rpm
subscription-manager register --org=org-example --activationkey=ak-eng-rhel7-std-server
```
