# MongoDB

## ReplicaSet

Si vous ne souhaitez pas répartir la charge mais simplement vosu assurer que votre donnée est répliquée, vous pouvez partir sur un déploiement avec **ReplicaSet**.

Attention, la réplication est asynchrone.

```shell
# cat /etc/mongodb.conf

# /etc/mongodb.conf
# mongod server config file
port = 27017
dbpath = /opt/mongodb/
fork = true
replSet = rs0
logpath = /var/log/mongodb.log
logappend = yes
```

Sur le premier noeud du cluster :

```mongo
mongo> rs.initiate()
```

Sur tous les noeuds :

```mongo
mongo> rs.add(“mongo-01:27017”)
mongo> rs.add(“mongo-02:27017”)
mongo> rs.add(“mongo-03:27017”)
```

Vérifier l'état :

```mongo
mongo> rs.status()
```

## Sharded CLuster

Deux roles supplémentaires font leur apparition :

* **mongos** : détermine la localisation de la donnée et envoie les requêtes au bon shard
* **mongod** : possède les metadata du cluster.

Le port pour **mongod** doit être différent du **mongos** qui va répondre aux requêtes des clients.

### Config server

```shell
# cat /etc/mongodb_config.conf

# /etc/mongodb_config.conf
# mongod configdb server config file
port = 27019
dbpath = /opt/configdb/
fork = true
configsvr = true
logpath = /var/log/mongodb_config.log
logappend = yes
```

### Mongos

```shell
# cat /etc/mongodb_mongos.conf

# /etc/mongos.conf
# mongos config file
port = 27017
configdb = mongo-01:27019,mongo-02:27019,mongo-03:27019
fork = true
logpath = /var/log/mongos.log
logappend = yes
```

### Shard

```shell
# cat /etc/mongodb_shard.conf

# /etc/mongodb_shard.conf
# mongodb shard with replica set config file
port = 27018
dbpath = /opt/mongodb/
fork = true
shardsvr = true
replSet = rs0
logpath = /var/log/mongodb.log
logappend = yes
```

### Activer le replicaset sur le sharded cluster

```mongo
mongo> use local
mongo> cfg = db.system.replset.findOne({_id:"rs0"})
mongo> cfg.members[0].host="mongo-01:27018"
mongo> cfg.members[1].host="mongo-02:27018"
mongo> cfg.members[2].host="mongo-03:27018"
mongo> db.system.replset.update({_id:"rs0"},cfg)
mongo> sh.addShard("rs0/mongo-01:27018,mongo-02:27018,mongo-03:27018");
mongo> sh.enableSharding("database_name")
mongo> sh.status()
```

## Loadbalancer

La répartition de charge intégrée à MongDB permet de répartir les shards grâce à un scaling horizontal.

### Get balancer state

```shell
sh.getBalancerState()
```

### Activer ou désactiver le balancer

Vous pouvez activer le **balancer** avec la commande suivante. Mais la répartition de charge ne sera démarrer qu'ultérieurement, lorsque le processus **Balancer** sera lancé.

```shell
sh.enableBalancing(<collection_name/namespace>)
```

Pour le désactiver :

```shell
sh.stopBalancer()
```

Utile lors des backups pour avoir un dump consistent.
Il est désactivable aussi pour une collection spécifique :

```shell
sh.disableBalancing("<db_name>.<collection_name>")
```

### Voir l'état du processus de répartition de charge

```shell
sh.isBalancerRunning()
``` 

## Changer la taille du chunk

Par défaut 64MB, il peut être intéressant de le réduire si le nombre d'IO est trop important.

```shell
use config
db.settings.save( { _id:"chunksize", value: <sizeInMB> } )
```

Attention, cette modification ne sera effective qu'en cas d'insert ou update. Et une taille réduite implique un temps de traitement plus important pour découper cette donnée lors de la modification.
