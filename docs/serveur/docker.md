# Installation et utilisation de Docker

## Lexique

* **Container**, c'est une image démarrée
* **Image**, c'est un paquetage qui contient tout le code nécessaire pour exécuter une application.

## Installation sur Debian 9 aka Stretch

```bash
apt-get update
apt-get install apt-transport-https ca-certificates curl gnupg2 software-properties-common
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce
```

Nous pouvons maintenant exécuter notre premier container, Hello World, et si tout se passe bien, le stopper:

```bash
docker run hello-world
docker stop hello-world
```

Une fois installé, vous allez trouver sur votre host une nouvelle interface réseau (docker0) qui va faire transiter vos paquets par IP masquerade.

## Commandes utiles pour bien commencer

Lister les images disponibles sur votre host:

```bash
docker image ls --all
```

Lister les containers:

```bash
docker container ls --all
docker ps -a
```

Connaître les informations d'une image:

```bash
docker inspect debian:stretch
```

Lister les containers en cours d'exécution :

```bash
docker ps
```

Connaître sa version de Docker:

```bash
docker version
```

Connaître les informations de son installation:

```bash
docker info
```

Lister les containers Docker (en cours d'utilisation, tous, et tous ceux en mode silencieux)

```bash
docker container ls
docker container ls --all
docker container ls -aq
```

Par défaut un container va se lancer avec un nom aléatoire composé de deux mots. Pour spécifier un nom, il suffit d'ajouter l'argument --name:

```bash
docker run --name debian_ftw debian:stretch
```

Récupérer les informations de la sortie standard liée à l'éxécution de notre container :

```bash
docker logs debian_ftw
```

Lancer un container en mode intéractif (-i) et utiliser un pseudo terminal (-t):

```bash
docker run -it debian /bin/bash
docker exec -it debian /bin/bash
```

Exécuter une commande dans votre container:

```bash
docker exec -it debian uptime
 14:19:40 up 2 days, 22:06,  0 users,  load average: 0.55, 0.66, 0.57
```

Pour relancer un container, arrêter et s'y reconnecter :

```bash
docker start debian_ftw
docker attach debian_ftw
```

Mettre en pause un container et enlever la pause :

```bash
docker pause debian_ftw
docker unpause debian_ftw
```

Si vous vous êtes connecté à un conteneur pour y installer un serveur nginx pas exemple, vous l'avez configuré correctement, vous pouvez sauvegarder vos modifications en le transformant en une nouvelle image:

```bash
docker commit debian_ftw nginx
```

Démarrer son container nginx en mappant le port 80 (-p), l'éxécuter en arrière plan (-d) et en lançant le process nginx en daemon (-g):

```bash
docker run -d -p 80 nginx /usr/sbin/nginx -g "daemon off"
```

Supprimer un container, ou tous ceux qui traînent, containers et images :

```bash
docker rm debian_ftw
docker rm $(docker ps -aq)
docker rmi $(docker images --list)
```

## Networks

Lister les réseaux présents :

```bash
docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
182bf8a04984        bridge              bridge              local
e70bd15b7f9d        host                host                local
634398d60018        none                null                local
```

Les différents réseaux disponibles :

* bridge: correspond au NAT, le container peut accéder à l'extérieur
* host: le container ne peut communiquer qu'avec le host
* none: ne peut communiquer qu'avec lui même

Pour créer un réseau de test spécifique en bridge pour que chaque container de ce réseau puisse communiquer ensemble et
 avec l'extérieur (ex: cas de cluster avec molecule).

```bash
docker network create -d bridge test
```

Supprimer un réseau:

```bash
docker network rm test
```

## Dockerfile

Le dockerfile est un fichier qui va décrire votre image, et ce qu'il y aura à l'intérieur. Nous allons créer une simple image contenant un serveur web apache. Pour cela on va créer un répertoire vide (ex: webserver) et y créer un dockerfile :

```Dockerfile
# Webserver
# Version 1
MAINTAINER manchot

# Utilisons l'image Debian officielle comme base
FROM debian

# Mapper le port 80 externe du host vers ce container
EXPOSE 80

# Liste des commandes à éxécuter
RUN apt update
RUN apt install -y apache2
```

Une fois que votre dockerfile est complet, vous pouver créer votre image :

```bash
docker build -t apache .
```

## Outils complémentaires

### Dive

Il permet de visualiser les différentes couches d'une image, d’identifier un espace perdu ou dupliqué, et de transmettre les résultats à un pipeline d'intégration continue
<https://github.com/wagoodman/dive>

### Docker Compose UI

Interface web en Flask pour les commandes Docker Compose
<https://github.com/francescou/docker-compose-ui>

### Dockly

Interface pour les commandes Docker
<https://github.com/lirantal/dockly>

### Elsy

Il permet de construire un référentiel logiciel de manière cohérente à travers tous les environnements.

<https://github.com/cisco/elsy>

### Kitematic

Il fournit une interface pour gérer ses containers sur votre poste de travail (Windows, Linux, MacOs)

<https://github.com/docker/kitematic>
