# Sécu container

tutos : <https://github.com/docker/labs/tree/master/security>
Scanner de vulnérabilité d'image : <https://github.com/docker/docker-bench-security>
<https://docs.docker.com/engine/security/https>

* fork bomb, un processus se fork jusqu'à épuisement des ressources. La parade simple est de limiter le nombre de tache pouvant être créer par un même utilisateur, via ulimit nproc.
* un container est lancé en tant que docker, donc root, il est donc préférable de lancer le container avec un autre user, à spécifier dans le docker file
* supprimer le réseau bridge par défaut (docker0) en passant l'option icc à false
* renvoyer les logs des processus exécutés dans les containers vers la sortie standart. Ce qui sera récupérable facilement pour être transféré vers syslog ou alors renvoyer vers uyn fichier particulier sur le système hôte.
* option *healthcheck*, permet de lancer une commande de façon récurrente pour vérifier le bon fonctionnement du processus exécuté dans la container.
* option restart : à positionner à *always* pour des tests, *on-failure* avec une limite de 5 en prod pour se rendre compte d'un réel problème
* multi-stage build, permet de compiler dans un container sans y laisser les librairies de dev dans le container final.
* directive *USER*, toutes les commandes exécutées au sein de l'image et du container seront effectuées avec cet utilisateur après la directive. Cela évite qu'un processus lancé dans un container, ne le soit en root
* alpine, distribution linux minimaliste, réduit l'empreinte ainsi que la surface d'attaque
* avoir un repository privé
