# Sécurisation et optimisation Apache 2

## Tester son fichier de configuration

    /usr/sbin/apache2 -t
    /usr/sbin/httpd -t
    apachectl configtest
    apachectl -t && apachectl -t -D DUMP_VHOSTS

## Sécurisation

Pour commencer, il nous faut réduire la surface d'attaque en limitant les informations fournies par notre serveur Web. Ceci sera à modifier dans ce fichier de configuration suivant si nous sommes sur Debian par exemple :
**/etc/apache2/apache2.conf**

Un extrait de la documentation officielle Apache 2 (<http://httpd.apache.org/docs/current/mod/core.html)>

### ServerTokens

Cette directive permet de contrôler le contenu de l'en-tête Server inclus dans la réponse envoyée au client : cet en-tête peut contenir le type de système d'exploitation du serveur, ainsi que des informations à propos des modules compilés avec le serveur.

* ServerTokens Full (ou non spécifié)
Le serveur envoie par exemple : Server: Apache/2.4.2 (Unix) PHP/4.2.2 MyMod/1.2
* ServerTokens Prod[uctOnly]
Le serveur renvoie (par exemple): Server: Apache
* ServerTokens Major
Le serveur renvoie (par exemple): Server: Apache/2
* ServerTokens Minor
Le serveur renvoie (par exemple): Server: Apache/2.4
* ServerTokens Min[imal]
Le serveur renvoie (par exemple): Server: Apache/2.4.2
* ServerTokens OS
Le serveur renvoie (par exemple): Server: Apache/2.4.2 (Unix)

Cette définition s'applique à l'ensemble du serveur et ne peut être activée ou désactivée pour tel ou tel serveur virtuel.

Le meilleur choix sera ici **ServerTokens Prod** qui est le minimum requis.

### ServerSignature

La directive **ServerSignature** permet de définir une ligne de pied de page fixe pour les documents générés par le serveur (messages d'erreur, listings de répertoires ftp de mod_proxy, sorties de mod_info, etc...). Dans le cas d'une chaîne de mandataires, l'utilisateur n'a souvent aucun moyen de déterminer lequel des mandataires chaînés a généré un message d'erreur, et c'est une des raisons pour lesquelles on peut être amené à ajouter un tel pied de page.

La valeur par défaut **Off** supprime la ligne de pied de page (et est ainsi compatible avec le comportement des versions 1.2 et antérieures d'Apache). la valeur On ajoute simplement une ligne contenant le numéro de version du serveur ainsi que le nom du serveur virtuel issu de la directive ServerName, alors que la valeur EMail ajoute en plus une référence "mailto:" à l'administrateur du document référencé issu la directive ServerAdmin.

Pour rester cohérent avec la directive **ServerTokens Prod**, nous choisirons de positionner **ServerSignature Off**

### mod_evasive

Nous pouvons rajouter le module **mod_evasive** pour limiter les dégâts d'une attaque DoS :

```bash
apt install mod_evasive
a2enmod mod_evasive
```

Et rajouter dans votre configuration Apache le bloc suivant :

```apache
<IfModule mod_evasive20.c>
    DOSHashtableSize    3097
    DOSPageCount        4
    DOSSiteCount        100
    DOSPageInterval     1
    DOSSiteInterval     1
    DOSBlockingPeriod   3600
    DOSEmailNotify      admin@vmanchot.net
    DOSLogDir           "/var/log/mod_evasive"
</IfModule>
```

Petite explication des paramètres :

**DOSHashTableSize** : Taille de la table hash, attention, peu consommer une importante quantité de mémoire vive.
**DOSEmailNotify** permet d'envoyer un mail d'alerte (attention, il utilise /bin/mail)
**DOSWhitelist** comme son nom l'indique, on donne une adresse IP qui ne sera jamais bloquée (attention, une IP par ligne)
**DOSPageInterval** Intervalle de temps en secondes, pour l'analyse des requêtes sur une page
**DOSSiteInterval** Intervalle de temps en secondes, pour l'analyse des requêtes sur un site
**DOSPageCount** Nombre de requêtes autorisés par page dans l'intervalle DOSPageInterval avant que l'IP ne soit bloquée
**DOSSiteCount** Nombre de requêtes autorisés par site dans l'intervalle DOSPageInterval avant que l'IP ne soit bloquée
**DOSBlockingPeriod** Durée en seconde de mise en quarantaine de l'IP, pendant laquelle le serveur répondra 403
**DOSLogDir** Emplacement pour écrire les fichiers de log
**DOSSystemCommand** En cas de blocage d'une IP, vous pouvez exécuter une commande

### Autre méthode contre DoS

Nous pouvons aussi limiter le nombre de connexion simultanées et persistantes.

Exemple pour un serveur personnel :

```ini
MaxClients 150
KeepAlive On
MaxKeepAliveRequests 100
KeepAliveTimeout 5
```

### Gestion des droits d'accès

#### Directory, Files, Location

La gestion des accès est effectuée par le module mod_access. Les trois catégories d'objets :

* Directory : répertoire sur serveur
* Location :  arborescence du site web
* Files : un fichier

Il est primordial de tout interdire par défaut et de n'autoriser que ce qui est nécessaire :

```ini
<Directory />
Order deny,allow
Deny from all
</Directory>
```

* Order : l'ordre d'évaluation des directive **Allow** et **Deny**
* Deny from all : interdire l'accès de toutes les sources. Mais il est possible d'interdire seulement un réseau, un nom de domaine, etc. 

#### Options, AllowOverride

Options permet de contrller :

* le suivi des liens symboliques FollowSymLinks/SymLinksIfOwnerMatch, la dernière option est la plus sécuritaire
* l'exécution des scripts CGI ExecCGI, à bannir au possible
* les Server Side Includes Includes et IncludesNOEXEC 
* la génération de pages d'index Indexes en l'absence de celles-ci 
* ainsi que l'orientation multilingue MultiViews
* All regroupe les différentes options sauf MultiViews, None supprime les options
* MultiViews redirige une demande pour index.html vers index.html.en ou index.html.fr selon la préférence signalée par le navigateur au serveur web

```ini
<Directory />
    Options SymLinksIfOwnerMatch
    AllowOverride None
</Directory />
```

La directive AllowOverride peut prendre n'importe quel paramètre qu'aurait pris Options.

### Tester sa configuration SSL

Le site de Qualys disponible [ici] (https://www.ssllabs.com/ssltest/index.html)

## Commandes utiles

### Recharger le service

```shell
apachectl -k graceful
```


## Optimisation

### Choisir le bon MPM

Apache peut utiliser plusieurs MPM (Multi-Processing Modules), ils permettent de gérer les connexions réseau, répartir les requêtes, bind les ports.
Un seul MPM peut être actif. Il peut être appelé directement en cli :

```shell
--with-mpm=(worker|prefork|event).
```

Le MPM par défaut et traditionnellment utilisé est **prefork**. Plus récement nous avons vu des modules utilisants des threads et donc plusieurs processus avec un overhead reduit, ce sont **worker** et **event**.

Déterminer le MPM utilisé :

```shell
httpd -V
```

* Worker MPM : utilise des processus enfants et peut avoir plusieurs threads affectés à chacun des processus. Chaque thread gère une connection à la fois. C'est un bon choix  pour les serveurs ayant un fort traffic car il possède une empreinte mémoire plus faible. Mais il peut poser des problèmes avec des librairies ou des langages (vieux PHP par exemple) qui ne s'adaptent pas bien avec ce fonctionement par thread. 
* Event MPM : utilise des threads aussi, mais il est fait pour servir plus de requêtes, il fonctionne quasiment de la même façon que Worker mais il n'est accessible qu'à partir d'Apache 2.4.
* Prefork MPM : il utilise plusieurs processus enfant avec un thread pour chacun. Il consomme plus de mémoire. Il est le plus sécurisé car les threads ne sont aps partagés 

### Directives MPM

#### STARTSERVERS (PREFORK, EVENT, WORKER MPM)

Défini le nomre de processus enfants créés au démarrage.

#### MINSPARESERVERS (PREFORK MPM)

Permet de définir le minimum de processus en attente d'une nouvelle requête (IDLE).
Si leur nombre décroit jusqu'à descendre en dessous de cette valeur, Apache va créé de nouveaux enfants (1 par seconde en Apache 2.2, et jusqu'à 32 par seconde en Apache 2.4).
Cela permet d'avoir toujours des ressources disponibles pour recevoir de nouvelles requêtes, cela est plus rapide que créé des processus à la demande.

Exemple de recommandation :

* 1-2GB RAM 10
* 2-4GB RAM 20
* 4+ GB RAM 25

Attention, augmenter cette valeur plus que de raison est dangereux car consommateur de ressources.

#### MAXSPARESERVERS (PREFORK MPM)

Permet de définir le nombre maximum de processus IDLE.
Si leur nombre croit au dessus de cette valeur, ils seront arrêtés.
Si *MaxSpareServers* < *MinSpareServers*, alors Apache va alligner la valeur de *MaxSpareServers* à *MinSpareServers*.

#### SERVERLIMIT (PREFORK, EVENT, WORKER MPM)

 * Prefork : Permet de définir la limte *MaxRequestWorkers* (256 par défaut)
 * Event et Worker : la limite de processus enfants

#### THREADLIMIT (EVENT, WORKER MPM)

Permet de définir le maximum de threads maximum, à lier avec *ThreadsPerChild*. Une valeur trop importante peut rendre votre système instable et rentre votre système inutilisable.

#### THREADSPERCHILD (EVENT, WORKER MPM)

Permet de définir le nombre de threads créés par chaque processus enfant. Pour rappel, un thread peut gérer une seule requête.
La limite haute est controllée par la directive *ThreadLimit*, par défaut 64. Dépasser cette valeur implique aussi une augmentation de cette valeur pour *ThreadsPerChild* implique aussi d'augmenter *ThreadLimit*.

Augmenter cette valeur permet à chque processus enfant de pouvoir gérer plus de requêtes en consommant moins de mémoire, car ils partagent leurs cache. Un processus ne peut peut partager son cache avec un autre processus, mais plusieurs threads d'un même processus oui.

#### MAXREQUESTWORKERS / MAXCLIENTS (PREFORK, EVENT, WORKER MPM)

*MaxRequestWorkers* était *MaxClients* avant Apache version 2.3.

Permet de définir le nombre maximum de requêtes simultanés. Les autres passeront en attente (queued), jusqu'au nombre maxumum défini par la directive *ListenBacklog*. 
Une valeur trop faible va limiter les connexions de vos clients, une valeur trop haute va consommer beaucoup de mémoire vive. 

Pour *prefork*, *MaxRequestWorkers* correspondra au nombre maximal de processus enfants. Par défaut 256, si vous souhaitez l'augmenter, il faudra augmenter aussi la directive *ServerLimit*.

*ServerLimit* doit être génréalement égal à *MaxRequestWorkers*.

Il n'y a pas de formule magique pour calculer ces valeursmais j'ai trouvé ce script pyhton pour lister les processus (apache, php), le nombre de processus enfants et la mémoire consommée. Ainsi vous pourrez déterminer la taille par processus Apache et utiliser la règle ci-dessous :

```
MaxRequestWorkers = (Mémoire totale - OS ou services critiiques) / Taille d'un processus Apache
```

#### MAXCONNECTIONSPERCHILD (PREFORK, EVENT, WORKER MPM)

Permet de définir la limite du nombre de connexion qu'un processus enfant peut gérer.
Ce paramètre permet d'éviter une fuite mémoire, une fois ce seuil atteint, le processus sera arrêté mais si la valeur est à 0 alors il ne le sera jamais. Un processus ne pourra pas donc durer indéfiniment si cette valeur est différente de 0, la mémoire sera donc recyclé dans un nouveau processus.

Une formule existe pour calculer sa valeur, mais demande une métrologie fine :

```
MaxConnectionPerChild - ( nombre de requêtes journalière / nombre de processus journaliers )
```

Il est assez courant de voir la valeur *1000* affectée à cette directive.

#### ExtendedStatus

Ce paramètre permet de récupérer plus de statistiques sur les requêtes et donc réduit les performances. Cette valeur ne doit être positionnée à *On* uniquement pendant le dev ou des benchamrk spécifiques.
