# OpenSSL

## Public Key Cryptography

Aussi connue sous le nom de chiffrement asymétrique, permet de ne pas partager le même secret.
Ainsi chaque entité génère une biclé (publique et privée).

* un secret chiffré avec la clé publique de luke, ne sera déchiffrable que par la clé privée correspondante (celle de luke donc).
* vérifier une signature de secret avec la clé publique ne peut fonctionner que si le secret a été chiffré avec la clé privée correspondante.

Pour que deux entités puissent communiquer ensemble et qu'elles ne se connaissent pas encore, il faut qu'elles puissent s'échanger pa clé publique.
Mais pour être sûr de l'authenticité, il faut qu'un tier certifi que la clé correspond bien à la ressource avec laquelle nous voulons communiquer.

La solution à ce problème est l'utilisation d'une PKI.

## Public Key Infrastructure - PKI

Une PKI permet de lier une clé public à une identité, gâce à une authorité de certification( AC ou CA en anglais).

Une AC est un tier qui centralise les clé publiques.

Ainsi quand Leia veut parler à Luke, Luke lui envoie un message signé par Han (AC) que Leia connait et à qui elle fait confiance.
Ce message signé est ce que l'on appelle un certificat. Leia est capable de vérifier la signature en utilisant le magasin de clé publique de Han et peut parler sereinement à Luke.

Il est courant d'avoir aussi une chaine de confiance. Leia parle à Luke, Han ne le connait pas, mais Han connait Chewbacca qui connait Luke.
Luke montre sa chaine de certificat, une de Chewbacca qui certifie que la clé publique correspond à Luke, et une de Han qui certifie que la clé appartient bien à Leia.
Sans connaitre Chewbacca, Leia  peut vérifier la clé obtenue par Han, être sûr de de la clé de Chewbacca, et si la confiance en Han est transitive alors Leia peut faire confiance à Chewbacca.

Relation Transitive : relation binaire qui, si elle est vérifiée pour les éléments a et b, ainsi que pour b et c, l'est aussi pour a et c.

## X.509

X.509 est un standard pour la PKI. Il défini le format d'une clé publique.

- Version 1 - [RFC1422][]
- Version 2 - [RFC2459][]
- Version 3 - [RFC5280][]

Un certificat X.509 v3 possède cette structure :

- Certificate
    - Version
    - Serial Number
    - Algorithm ID
    - Issuer
    - Validity
        - Not Before
        - Not After
    - Subject
    - Subject public key info
    - Issuer Unique Identifier (optional)
    - Subject Unique Identifier (optional)
    - Extensions (optional)
        - ...
- Certificate Signature Algorithm
- Certificate Signature

### SNI

Partage de plusieurs certificats pour la même adresse IP ou nom DNS.

### Version, Serial, Algorithm ID et Validity

- **Version** - Indique la version X.509
- **Serial** - Nombre unique assigné par l'AC à chaque certificat
- **Algorithm ID** - Doit être le même que le champ "Certificate Signature Algorithm"
- **Validity** - Deux dates (début et fin) qui définissent la période de validité du certificat

### Issuer and Subject

Les deux sont unique par CA et sont des Distinguished Name (DN) uniques

Un DN, [RFC1779], contient sur une seule ligne :

- CN - CommonName
- L - LocalityName
- ST - StateOrProvinceName
- O - OrganizationName
- OU - OrganizationalUnitName
- C - CountryName

Example:  
`C=US, ST=California, L=Mountain View, O=Google LLC, CN=*.google.fr`

Quand on se connecte à un site web en HTTPS, le navigateur va vérifier si la valeur du CN correspond bien au domaine. Dans l'exemple précédent nous avons un wildcard qui est valable pour tous les sous domaines.
Mais des domaines additionnels peuvent être spécifiés dans l'extension Subject Alternative Names.

#### Subject public key info

Contient l'algorithm utilisé pour la clé et ses paramètres.

- algorithm: rsa encryption
- key size: 2048
- exponent: 0x10001
- modulus: 00:ec:82:3f:78:b6...

#### Extensions

Fonctionnalité introduite en version 3. Il y a deux types d'entensions, les critiques et non critiques. Seules les non critques peuvent être ignorées.

- Subject Key Identifier
- Authority Key Identifier
- Subject Alternative Name
- Basic Constraints

##### Authority and Subject Key Identifiers

Utilisé quand une authorité possède plusieurs clés pour signer.
L'indetifiant est un hash de la clé publique, 160 bits SHA-1.

##### Subject Alternative Name

Peut contenir des entrées DNS ou IP additionnelles pour lesquelles le certificat est valide.

### Wildcard certificates

Un certificat wildcard est un certificat qui peut être utilisé pour plusieurs sous domaines.

Un navigateur va accepter un certificat avec le CN `*.vmanchot.lab` pour
`www.vmanchot.lab`, `login.vmanchot.lab` or `smtp.vmanchot.lab`. 
Mais le domaine parent `vmanchot.lab` ne fonctionnera pas.

## OpenSSL

OpenSSL est un outil cryptographiqye contenant plusieurs sous commandes possédant sa propre page de man.
La plupart des commandes foncitonnent avec les paramètres `-in` et `-out`.

 *  `-in <file>`
 *  `-out <file>`
 * `-noout`
 * `-text` pour lire la sortie

### Generate Keys and Certificate Signing Request (CSR)

Générer une clé RSA avec une longeur de clé de 2048 bits:

```
openssl genrsa -out vmanchot.lab.key 2048

Generating RSA private key, 2048 bit long modulus
.........................................+++
```

Le fichier généré `vmanchot.lab.key` contient la clé privée.

Vous pouvez utiliser l'outil `openssl rsa` pour voir la clé.

```
openssl rsa -in vmanchot.lab.key -noout -text
```

La clé publique peut être extraite de la clé privée :

```
openssl rsa -in vmanchot.lab.key -pubout -out vmanchot.lab.pubkey
openssl rsa -in vmanchot.lab.pubkey -pubin -noout -text
```

La clé privé ne doit surtout pas être partagé !

On va pouvoir générer une **Certificate Signing Request** qui contiendra plusieurs informations que nous souhaitons voir incluses dans le certificat.
Pour pouver la propriété de le clé privée, la CSR sera signé avec la clé privée.

```
openssl req -new -key vmanchot.lab.key -out vmanchot.lab.csr

You are about to be asked to enter information that will be incorporated
into your certificate request.
What you are about to enter is what is called a Distinguished Name or a DN.
There are quite a few fields but you can leave some blank
For some fields there will be a default value,
If you enter '.', the field will be left blank.
-----
Country Name (2 letter code) []:FR
State or Province Name (full name) []:IDF
Locality Name (eg, city) []:PARIS
Organization Name (eg, company) []:vmanchot
Organizational Unit Name (eg, section) []:
Common Name (eg, fully qualified host name) []:.vmanchot.lab
Email Address []:admin@vmanchot.lab

Please enter the following 'extra' attributes
to be sent with your certificate request
A challenge password []:
```

Nous pouvons voir le contenu de la CSR :

```
openssl req -in vmanchot.lab.csr -noout -text

Certificate Request:
    Data:
        Version: 0 (0x0)
        Subject: C=FR, ST=IDF, L=PARIS, O=vmanchot, CN=.vmanchot.lab/emailAddress=admin@vmanchot.lab
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:bf:fe:e3:78:99:a0:70:3a:fc:7d:9b:b6:06:c1:
        ...
                Exponent: 65537 (0x10001)
        Attributes:
            a0:00
    Signature Algorithm: sha256WithRSAEncryption
         6f:4a:78:d1:19:47:9c:f9:7b:96:9b:b3:8f:73:1c:e6:19:2e:

         ...
```

Le certificat est envoyé à l'issuer, il approuve la CSR et fourni en retour le certificat.

Il n'y a pas dans cette exemple d'extension, pour cela il vous faudra un fichier de configuration, nous verrons cela plus tard pour remplir ce fichier de configuration.

```
openssl req -new -out vmanchot.lab.csr -keyout vmanchot.lab.key -config san.cfg
```

## Création AC et SAN pour notre lab

### CCréation d'une AC

Premièrement, nous générons une clé privé pour notre AC :

```
openssl genrsa -out ca_vmanchot.lab.key 2048
```

Pour voir la clé

```
openssl rsa -in ca_vmanchot.lab.key -noout -text
```

Extraire la clé publique :

```
openssl rsa -in ca_vmanchot.lab.key -pubout -out ca_vmanchot.lab.pubkey
openssl rsa -in ca_vmanchot.lab.pubkey -pubin -noout -text
```

Générons un certificat auto signé :
                    
```
openssl req -new -x509 -key ca_vmanchot.lab.key -out ca_vmanchot.lab.crt
```

### Signature

La méthode la plus simple pour signer cette CSR :

```
openssl x509 -req -in vmanchot.lab.csr -CA ca_vmanchot.lab.crt -CAkey ca_vmanchot.lab.key -CAcreateserial -out vmanchot.lab.crt

Signature ok
subject=/C=FR/ST=IDF/L=PARIS/O=vmanchot/CN=.vmanchot.lab/emailAddress=admin@vmanchot.lab
Getting CA Private Key
```

Chaque certificat délivré doit contenir une numéro de série unique assigné par l'AC.
OpenSSL stocke les numéros de série dans un fichier portant par défaut le nom du certificat de l'AC. Dans notre exemple :

```
cat ca_vmanchot.srl

8EF747CF0B0D4250
```

## Openssl ca

Vous pouvez aussi signer une CSR avec la commande `ca`. Mai vous devez tout d'abord renseigner un fichier de configuration, exemple `ca.conf`, voir exemple en fin de tuto.

```
$ openssl ca -config ca_vmanchot.cnf -out ca_vmanchot.crt -infiles ca_vmanchot.csr
```

Si nous souhaitons rajouter des extension vous devez créer fichier cnf, un exemple en fin de tuto:

```
$ openssl ca -config ca.cnf -out vmanchot.crt -extfile vmanchot.extensions.cnf -in vmanchot.csr
```


## Cheatsheet

Pour créer un fichier contenant le certificat de l'AC et le certificat de notre service.

```
cat vmanchot.lab.crt ca_vmanchot.lab.crt > example.org.bundle.crt
```

Vérifier la CSR
```
openssl req -text -noout -verify -in vmanchot.lab.csr
```

Vérifier le certificat

```
openssl x509 -noout -text -in vmanchot.lab.crt
```

Vérifier la clé

```
openssl rsa -noout -text -in vmanchot.lab.key
```

Convertir pem en der

```
openssl x509 -outform der -in vmanchot.lab.crt -out vmanchot.lab.der
```

Convertir de en pem

```
openssl x509 -inform der -in vmanchot.lab.der -out vmanchot.lab.pem
```

Convertir un fichier PKCS12 (.p12 ou .pfx contenant le certificat et sa clé privée) en pem

```
openssl pkcs12 -in vmanchot.lab.pfx -out vmanchot.lab.pem -nodes
```

Convertir une certificat pem et sa clé privée en PKCS12

```
openssl pkcs12 -export -out vmanchot.lab.pfx -inkey vmanchot.lab.key -in vmanchot.lab.crt -certfile ca_vmanchot.lab.crt
```

Exporter un p12

```
openssl pkcs12 -export -inkey vmanchot.lab.key -in vmanchot.lab.crt -out vmanchot.lab.p12 -passout pass:
```

Vérifier la chaine de certification

```
openssl verify -verbose -CAfile ca_vmanchot.lab.crt vmanchot.lab.crt
```

Vérifier un p12

```
openssl pkcs12 -in vmanchot.lab.p12 -passin pass: -passout pass: -info
```

Afficher l'empreinte

```
openssl x509 -fingerprint -sha1 -in vmanchot.lab.crt -noout
```

Vérifier que la clé publique dans votre certificat est conforme avec la portion publique de votre clé privée.

```
(openssl x509 -noout -modulus -in vmanchot.lab.crt | openssl sha256 ; openssl rsa -noout -modulus -in vmanchot.lab.key | openssl sha256) | uniq
```

Si plusieurs hash sont affichés alors ils sont différents

Vérifier si la chaine de certificat est correcte avec le certificat de l'AC

```
curl https://www.vmanchot.lab --cacert cacert.pem 
```

```
openssl s_client -connect www.vmanchot.lab -CAfile cacert.pem
```

Récupérer le certificat d'un serveur

Avec SNI

```
openssl s_client -showcerts -servername www.vmanchot.lab -connect www.vmanchot.lab:443 </dev/null
```

Sans SNI

```
openssl s_client -showcerts -connect www.vmanchot.lab:443 </dev/null
```

Pour visualiser les détails d'un certificat (SNI dans l'exemple)
```
echo | openssl s_client -servername www.example.com -connect www.example.com:443 2>/dev/null | openssl x509 -text
```

Supprimé la passphrase d'une clé privée

```
openssl rsa -in privateKey.pem -out newPrivateKey.pem
```

## Génération d'une AC et de certificats SAN

J'ai pris l'exemple ici de la génération de certificats pour une stack elastic de lab.
C'est pour satisfaire les besoins spécifiques à Elastic que dans les extensions vous trouverez :

```ini
keyUsage = keyEncipherment, dataEncipherment, digitalSignature
extendedKeyUsage = serverAuth, clientAuth
```

### Fichier de configuration AC

**ca.cnf**

```ini
# OpenSSL root CA configuration file.

# The [default] section contains global constants that can be referred to from
# the entire configuration file. It may also hold settings pertaining to more
# than one openssl command.

[ default ]
# CA Name
ca = ca_elastic.lab
# Top dir
dir = .

# The next part of the configuration file is used by the openssl req command.
# It defines the CA's key pair, its DN, and the desired extensions for the CA
# certificate.
[ req ]
# RSA key size
default_bits            = 2048
# Protect private key
encrypt_key             = yes
# MD to use
default_md              = sha256
# Input is UTF-8
utf8                    = yes
# Emit UTF-8 strings
string_mask             = utf8only
# Don't prompt for DN
prompt                  = no
# DN section
distinguished_name      = ca_distinguished_name
# Desired extensions
#req_extensions          = custom_req
x509_extensions = custom_req

[ ca_distinguished_name ]
0.domainComponent = "lab"
1.domainComponent = "elastic"
commonName = Elastic lab Root Certificate Authority
stateOrProvinceName = IDF
countryName = FR
emailAddress = support@elastic.lab
organizationName = vmanchot
organizationalUnitName = Development

[ custom_req ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
basicConstraints = critical, CA:true
keyUsage = critical, digitalSignature, cRLSign, keyCertSign

# The remainder of the configuration file is used by the openssl ca command.
# The CA section defines the locations of CA assets, as well as the policies
# applying to the CA.

[ ca ]
# The default CA section
default_ca              = ca_elastic.lab

[ ca_elastic.lab ]
# The CA cert
certificate             = $dir/ca/$ca/$ca.crt
# CA private key
private_key             = $dir/ca/$ca/private/$ca.key
# Certificate archive
new_certs_dir           = $dir/ca/$ca
# Serial number file
serial                  = $dir/ca/$ca/db/$ca.crt.srl
# CRL number file
crlnumber               = $dir/ca/$ca/db/$ca.crl.srl
# Index file
database                = $dir/ca/$ca/db/$ca.db
# Require unique subject
unique_subject          = no
# How long to certify for
default_days            = 3652
# MD to use
default_md              = sha256
# Default naming policy
policy                  = any_pol
# Add email to cert DN
email_in_dn             = no
# Keep passed DN ordering
preserve                = no
# Subject DN display options
name_opt                = ca_default
# Certificate display options
cert_opt                = ca_default
# Copy extensions from CSR
copy_extensions         = copy
# Default cert extensions
x509_extensions         = server_cert
# How long before next CRL
default_crl_days        = 365
# CRL extensions
crl_extensions          = crl_ext

[ match_pol ]
# if the value is "match" then the field value must match the same field in the
# CA certificate. If the value is "supplied" then it must be present.
# Optional means it may be present. Any fields not mentioned are silently
# deleted.
# Must match 'elastic.lab'
domainComponent         = match
# Must match 'vmanchot'
organizationName        = match
# Included if present
organizationalUnitName  = optional
# Must be present
commonName              = supplied

[ any_pol ]
domainComponent         = optional
countryName             = optional
stateOrProvinceName     = optional
localityName            = optional
organizationName        = optional
organizationalUnitName  = optional
commonName              = optional
emailAddress            = optional

# Certificate extensions define what types of certificates the CA is able to
# create.

[ root_ca_ext ]
keyUsage                = critical,keyCertSign,cRLSign
basicConstraints        = critical,CA:true
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid:always

[ signing_ca_ext ]
keyUsage                = critical,keyCertSign,cRLSign
basicConstraints        = critical,CA:true,pathlen:0
subjectKeyIdentifier    = hash
authorityKeyIdentifier  = keyid:always

[ usr_cert ]
# Extensions for client certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
nsCertType = client, email
nsComment = "OpenSSL Generated Client Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer
keyUsage = critical, nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = clientAuth, emailProtection

[ server_cert ]
# Extensions for server certificates (`man x509v3_config`).
basicConstraints = CA:FALSE
#nsCertType = server
#nsComment = "OpenSSL Generated Server Certificate"
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid,issuer:always
keyUsage = critical, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth, clientAuth

# CRL extensions exist solely to point to the CA certificate that has issued
# the CRL.

[ crl_ext ]
authorityKeyIdentifier  = keyid:always
```

### Fichier de configuration des certificats avec SAN

**san.cfg**

```ini
# The main section is named req because the command we are using is req
# (openssl req ...)
[ req ]
# This specifies the default key size in bits. If not specified then 512 is
# used. It is used if the -new option is used. It can be overridden by using
# the -newkey option.
default_bits = 2048

# This is the default filename to write a private key to. If not specified the
# key is written to standard output. This can be overridden by the -keyout
# option.
default_keyfile = private.key

# If this is set to no then if a private key is generated it is not encrypted.
# This is equivalent to the -nodes command line option. For compatibility
# encrypt_rsa_key is an equivalent option.
encrypt_key = no

# This option specifies the digest algorithm to use. Possible values include
# md5 sha1 mdc2. If not present then MD5 is used. This option can be overridden
# on the command line.
default_md = sha256

# if set to the value no this disables prompting of certificate fields and just
# takes values from the config file directly. It also changes the expected
# format of the distinguished_name and attributes sections.
prompt = no

# if set to the value yes then field values to be interpreted as UTF8 strings,
# by default they are interpreted as ASCII. This means that the field values,
# whether prompted from a terminal or obtained from a configuration file, must
# be valid UTF8 strings.
utf8 = yes

# This specifies the section containing the distinguished name fields to
# prompt for when generating a certificate or certificate request.
distinguished_name = custom_req_distinguished_name


# this specifies the configuration file section containing a list of extensions
# to add to the certificate request. It can be overridden by the -reqexts
# command line switch. See the x509v3_config(5) manual page for details of the
# extension section format.
req_extensions = custom_extensions

[ custom_req_distinguished_name ]
countryName = FR
stateOrProvinceName = IDF
localityName = PARIS
organizationName = vmanchot
commonName = *.elastic.lab

[ custom_extensions ]
basicConstraints=CA:FALSE
subjectAltName=@alt_names
subjectKeyIdentifier = hash
keyUsage = keyEncipherment, dataEncipherment, digitalSignature
extendedKeyUsage = serverAuth, clientAuth

[ alt_names ]
DNS.1   = elasticstack.elasticsearch-01
DNS.2   = elasticstack.elasticsearch-02
DNS.3   = elasticstack.elasticsearch-03
DNS.4  = elasticstack.logstash-01
DNS.5  = elasticstack.kibana-01
DNS.6  = elasticstack.apm-01
```

### Script de génération

**generate_certs.sh**

```shell
#!/bin/bash
set -eux

ca='ca_elastic.lab'
domain='elastic.lab'

# create ca directories
mkdir -p ca/$ca/private
mkdir -p ca/$ca/db
# create necessary files
touch ca/$ca/db/$ca.db
touch ca/$ca/db/$ca.crl.srl
touch ca/$ca/db/$ca.crt.srl
echo '01' > ca/$ca/db/$ca.crt.srl

# generate ca key
openssl genrsa -out ca/$ca/private/$ca.key 2048
# generate self-signed certificate for ca
openssl req -new -x509 -key ca/$ca/private/$ca.key -out ca/$ca/$ca.crt -config ca.cnf

# generate private key and csr for server certificate
openssl genrsa -out $domain.key 2048
openssl req -new -key $domain.key -out $domain.csr  -config san.cfg

# genrate ca signed certificate
openssl ca -config ca.cnf -out $domain.crt -infiles $domain.csr 
#openssl ca -config ca.cnf -out $domain.crt -infiles $domain.csr -reqexts server_cert -vv
```
