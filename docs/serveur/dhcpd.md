# DHCPd

## Tester la configuration

```bash
dhcpd -t
dhcpd -t -cf dhcpd.conf
dhcpd -T
dhcpd -T -lf dhcpd.lease.file
```

* -t : vérification de la syntaxe
* -T : tester la base de donnée lease
* -cf : spécifier le fichier de configuration a tester
* -lf : spécifier le fichier de base de donnée a tester
